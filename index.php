<?php

use Framework\{Framework, Mail};

ini_set('session.cookie_lifetime', 60 * 60 * 24 * 365);
ini_set('session.gc_maxlifetime', 60 * 60 * 24 * 365);

session_start();

define('ROOT', __DIR__);

require_once 'config.php';
require_once 'vendor/framework/Framework.php';
require_once ROOT . '/vendor/autoload.php';


$routes = require ROOT . '/application/routes.php';
$framework = new Framework($routes);

Mail::setCredentials(APP_EMAIL, APP_EMAIL_PASSWORD);

require_once ROOT . '/application/registerMiddlewares.php';

$framework->start();

