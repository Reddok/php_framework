'use strict';

const $ = require('jquery'),
    registerMessage = require('./libs/message');

console.log($('.system-message'));

$('.system-message').each(function(i, el) {
    let $el = $(el),
        message = $el.text(),
        type = $el.attr('data-type');

    console.log(message, type);
    registerMessage(message, type);
}).remove();