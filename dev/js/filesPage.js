'use strict';

const $ = require('jquery'),
    $container = $('.filesPage'),
    Selector = require('./libs/selector'),
    modal = require('./libs/modal');

if($container.length) {

    let selector = new Selector({
        container: 'uploadedFiles',
        getUrl: '/media/getUploadedJson',
        postUrl: '/media/createJson'
    });


    selector.refresh();

    selector.registerCallback( function(image) {

        modal.createSubmit(function() {
            window.location = '/files/' + image.id + '/delete';
        });

    } );

}

