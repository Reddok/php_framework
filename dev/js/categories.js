'use strict';

const $ = require('jquery'),
    addingEntities = require('./libs/entitiesAddition'),
    $root = $('.categoriesSection'),
    $selectedList = $root.find('.selectedCategories'),
    $selectElem = $root.find('#categoriesField'),
    $button = $root.find('.addCategory'),
    $newInput = $root.find('[name=newCategory]');

$selectElem.on('change', function() {
    let $selected = $("option:selected", this);

    if(  parseInt($selected.val()) ) {
        addToSelected($selected.val(), $selected.text());
    }

});

$selectedList.on('click', '.delete', function(e) {
    $(this).closest('li').remove();
    e.preventDefault();
});

$button.on('click', function(e) {

    if($newInput.val()) {

        addingEntities.addCategory($newInput.val())
            .then(data => {
                addToList(data.id, data.title);
                addToSelected(data.id, data.title);
                $newInput.val('');
            });
    }

    e.preventDefault();

});

function addToSelected(id, title) {

    if(!$selectedList.find('input[value=' + id +']').length) {

        let html = '<li>' +
            '<span class="category-name">' + title + '</span> ' +
            '<a href="javascript:;" class="delete fa fa-times"></a>' +
            '<input type="hidden" name="post[categories][]" value="' + id +'"/>'    +
            '</li>';

        $selectedList.append(html);
    }

}

function addToList(id, title) {
    $selectElem.append('<option value="' + id + '">' + title + '</option>');
}