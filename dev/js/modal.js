'use strict';

const $ = require('jquery'),
    modal = require('./libs/modal');


$(document).on('click', '.ui-submit', function(e, checkTrigger=false) {

    let $elem = $(this);

    if(!checkTrigger) {

        let message = $elem.attr('data-message'),
            title = $elem.attr('data-title');

        modal.createSubmit(function() {
            $elem.trigger('click', true);
        }, message, title);

        e.preventDefault();

    } else {
        if(this.tagName === 'A') window.location = $elem.attr('href');
    }

});

$(document).on('click', '[data-modal-id]', function() {

    let $modalId = $(this).attr('data-modal-id'),
        $el = $('#' + $modalId);

    if( !$el.dialog( "instance" ) ) {
        modal.createModal($el);
    }

    $el.dialog( $el.dialog('isOpen') ? 'close' : 'open' );

    return false;
});

