'use strict';

const addingEntities = require('./libs/entitiesAddition'),
    getRecommendedTags = require('./libs/autotag.js'),
    $ = require('jquery'),
    $root = $('.tagsSection'),
    $tagCloudList = $root.find('.tagCloudList'),
    $selectedList = $root.find('.selectedTags'),
    $input = $root.find('[name=newTag]'),
    $canvas = $tagCloudList.closest('canvas'),
    $button = $root.find('.addTag'),
    $contentProvider = $('[data-role=autotag-content]'),
    $recommendedTagList = $('[data-role=autotag-container]'),
    $tagContainer = $recommendedTagList.parent();

$tagCloudList.on('click', 'a', function(e) {
    let $el = $(this),
        title = $el.text(),
        id = $el.attr('data-id');

    addToSelected(id, title);

    e.preventDefault();
});

$selectedList.on('click', 'li', function(e) {
    $(this).remove();
    e.preventDefault();
});


$button.on('click', function(e) {
    let content = $input.val().trim();

    if(content) {

        addingEntities.addTag(content)
            .then(data => {
                addToCloud(data.id, data.title, data.postsCount);
                addToSelected(data.id, data.title);
                $input.val('');
            });
    }

    e.preventDefault();

});


$contentProvider.on('blur', function() {

    let content = $(this).text().trim();

    if(content && content !== this.prevContent) {

        $recommendedTagList.html('<div class="loader"><span class="fa fa-refresh fa-spin fa-2x"></span></div>');
        $tagContainer.show();

        getRecommendedTags(content)
            .then(
                tags => {
                    console.log(tags);
                    addToRecommended(tags);
                    tags.length || $tagContainer.hide();
                 },
                 e => $tagContainer.hide()

            );

    }

    this.prevContent = content;

});

$recommendedTagList.on('click', 'li', function() {

    let $el = $(this),
        tag = $el.text(),
        prom = Promise.resolve(),
        anchors = $tagCloudList.find('a'),
        availableTags = anchors.map( (i, elem) => $(elem).text().toLowerCase() ).get(),
        index;

    if( !~(index = availableTags.indexOf(tag)) ) {

        prom =  prom
                .then(addingEntities.addTag.bind(addingEntities, tag))
                .then(data => {
                    addToCloud(data.id, data.title, data.postsCount);
                    return data;
                });


    } else {

        prom = prom.then( () => {
            let $foundEl = $(anchors[index]);
            return {id : $foundEl.attr('data-id'), title: $foundEl.text()};
        });

    }

    prom.then( data => {
        addToSelected(data.id, data.title);
        $el.remove();
        $tagContainer.find('li').length || $tagContainer.hide();
    });



});


function addToSelected(id, title) {

    if(!$selectedList.find('input[value=' + id +']').length) {
        $selectedList.append(
            '<li><a href="javascript:;">' + title + '</a><input type="hidden" name="post[tags][]" value="' + id + '"/></li>'
        );
    }

}

function addToCloud(id, title, weight) {
    $tagCloudList.append(
        '<li> <a href="javascript:;" data-id="' + id + '" data-weight="' + weight + 1 + '">'
        + title + '</a></li>'
    );

    $canvas.tagcanvas('update');
}

function addToRecommended(tags) {
    let listItems = tags.map( tag => '<li>' + tag + '</li>');
    $recommendedTagList.html(listItems);
}