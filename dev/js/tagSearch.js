'use strict';

const $ = require('jquery'),
    $list = $('#tagList').find('li');

$(document).on('input', '#tagSearch', function() {
   let $this = $(this),
       value = $this.val().toLowerCase();

    $list.each(function(i, el) {

      let $el = $(el),
          text = $el.find('a').text().toLowerCase();

      if(text.indexOf(value)) {
          $el.addClass('none');
      } else {
          $el.removeClass('none');
      }

   });
});