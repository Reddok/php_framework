const $ = require('jquery'),
    Selector = require('./libs/selector');

require('jquery-ui/ui/widgets/dialog');

let selectors = new Map();

$(document).on('click', '[data-role=invoker]', function() {

    if( !selectors.has(this) ) {

        let $el = $(this),
            $container = $el.closest('[data-role=container]'),
            $preview = $container.find('[data-role=preview]'),
            $value = $container.find('[data-role=value]'),
            $modalId = $el.attr('data-modal-id'),
            selector = new Selector({
                container: 'uploadedFiles',
                getUrl: '/media/getUploadedJson',
                postUrl: '/media/createJson'
            });

        selector.registerCallback(function(file) {

            $preview.attr('src', file.path);
            $value.val(file.id);

            $modalId && $('#' + $modalId).dialog('close');

        });

        selectors.set(this, selector);

    }

    selectors.get(this).refresh()

});