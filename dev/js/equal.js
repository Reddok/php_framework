'use strict';

const $ = require('jquery');

let $container = $('.other-posts'),
    $posts = $container.find('a'),
    $pruners = $posts.find('.img-pruner'),
    minHeight = Infinity,
    maxHeight = 0;

$pruners.each( function() {
    minHeight = Math.min(this.clientHeight, minHeight);
});

$pruners.css('height', minHeight);

$posts.each( function() {
     maxHeight = Math.max(this.clientHeight, maxHeight);
});


$posts.css('height', maxHeight);