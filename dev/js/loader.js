'use strict';

const $ = require('jquery'),
    $document = $(document),
    $body = $(document.body),
    $window = $(window),
    $container = $('#articles');

    let page = 2,
        nextEvent = 0,
        interval = 1000,
        lastPosition = 0,
        scrollTop;


    if($container.length) {

        $window.on('scroll', function() {

            scrollTop = $document.scrollTop();

            if(
                scrollTop > lastPosition &&
                scrollTop >= $container.offset().top + $container.height() - $window.height() &&
                !$body.hasClass('loading') &&
                Date.now() > nextEvent
            ) {

                nextEvent = Date.now() + interval;

                $.ajax({
                    url: window.location.href,
                    type: 'get',
                    data: {page: page},
                    beforeSend: function() {
                        $body.addClass('loading');
                        $('.ajax-error, .ajax-empty').remove();
                    },
                    complete: function() {
                        $body.removeClass('loading');
                    },
                    success: function(data) {
                        let $main = $(data);

                        if( $main.length ) {
                            $container.append(data);
                            page++;
                        } else {
                            $container.append('<div class="ajax-empty">There are no posts.</div>');
                        }

                    },
                    error: function(xhr) {
                        console.log(xhr);
                        $container.append('<div class="ajax-error">Some error happens during loading.</div>');
                    }
                })

            }

            lastPosition = scrollTop;

        });

    }

