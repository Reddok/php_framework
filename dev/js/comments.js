'use strict';

const $ = require('jquery'),
    dialog = require('jquery-ui/ui/widgets/dialog'),
    root = $('.comments'),
    $dest = $('#commentForm'),
    $userField = $dest.find('.reply-to');

    $('.answers').show();

root.on('click', '.commentButton', function() {
    let $elem = $(this),
        parent = $elem.attr('data-parent') || '',
        user = $elem.attr('data-user');

    $dest.find('input.parentComment').val(parent);

    if(user) {
        $userField.parent().show();
        $userField.text(user);
    } else {
        $userField.parent().hide();
    }

});

root.on('click', '.answers-toggle-wrapper', function() {
    let $elem = $(this),
        $list = $elem.closest('.comment').find('.answers').first(),
        $icon = $elem.find('.answers-toggle').first();

    $list.slideToggle(400);
    $elem.toggleClass('expand').toggleClass('collapse button');
    $icon.toggleClass('fa-plus-square-o').toggleClass('fa-minus-square-o');


});