'use strict';

const $ = require('jquery'),
    initEditor = require('./libs/editor'),
    $selectedCats = $('.categoriesSection').find('.selectedCategories'),
    $selectedTags = $('.tagsSection').find('.selectedTags'),
    $thumbnailPreview = $('.thumbnailPreview'),
    $oldCategories = $selectedCats.html(),
    $oldTags = $selectedTags.html(),
    oldSrc = $thumbnailPreview.attr('src');

initEditor({selector: '.editor'});

$(document).on('click', '[type=reset]', function() {

    $selectedCats.html($oldCategories);
    $selectedTags.html($oldTags);
    $thumbnailPreview.attr('src', oldSrc);

});
