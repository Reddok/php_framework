'use strict';

const $ = require('jquery'),
    $slug = $('#slug'),
    $title = $('#titleField');

function createSlug(input) {
    return (input.replace(/(\s|\.)/ig, '_')).toLowerCase();
}

$title.on('input', function() {
   let val = $(this).val();
   $slug.val( createSlug(val) );
});

