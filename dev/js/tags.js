'use strict';

const $ = require('jquery');
require('./libs/tagCloud');

$('canvas#tags-cloud').tagcanvas({
    outlineThickness : 2,
    maxSpeed : 0.03,
    depth : 0.9,
    frontSelect: true,
    outlineColour: '#a8a8aa',
    zoomMax: 1,
    zoomMin: 1,
    initial: [0.4,-0.3],
    weight: true,
    weightFrom: 'data-weight',
    weightMode: 'both',
    weightSize: 10,
    weightSizeMin: 10,
    weightSizeMax: 10,
    weightGradient: { 0: '#a8a8aa', 1: "#fff" }
});