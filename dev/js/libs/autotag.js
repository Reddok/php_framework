'use strict';

const $ = require('jquery');

function getRecommendedTags(content) {

    return new Promise( (res, rej) => {

        $.ajax('/tags/recommended', {
            method: 'POST',
            data: {content},
            success(data) {
                data = JSON.parse(data);
                res(data);
            },
            error(jqXHR) {
                rej(jqXHR.responseText);
            }
        });

    } );

}

module.exports = getRecommendedTags;