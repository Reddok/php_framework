'use strict';

const $ = require('jquery'),
    tinymce = require('tinymce/tinymce'),
    Selector = require('./selector');

require('tinymce/themes/modern/theme');
require('tinymce/plugins/autolink');
require('tinymce/plugins/lists');
require('tinymce/plugins/link');
require('tinymce/plugins/image');
require('tinymce/plugins/hr');
require('tinymce/plugins/anchor');
require('tinymce/plugins/media');
require('tinymce/plugins/save');
require('tinymce/plugins/paste');
require('tinymce/plugins/textcolor');
require('tinymce/plugins/imagetools');

require.context(
    '!file-loader?name=[path][name].[ext]&context=node_modules/tinymce!tinymce/skins',
    true,
    /.*/
);

let defaults = {
    resize: true,
    branding: false,
    plugins: [
        'autolink lists link image hr anchor',
        'media save',
        'paste textcolor imagetools'
    ],
    toolbar1: 'undo redo | insert image-selector | styleselect | bold italic | ' +
    'alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link media | ' +
    ' forecolor backcolor',
    relative_urls : false,
    remove_script_host : true,
    document_base_url : '',
};

function initEditor(options) {

    let $elements = $(options.selector);


    $elements.each(function(i, el) {

       let $el = $(el),

           params = {
            target: el,
            height: $el.attr('data-width') || 200,
            setup: function(editor) {

                let  $modal, $invoker, $editContainer, selector, $container, $body, handlersFromOrigin, handlers;

                editor.on('init', function (e) {

                    $modal = $('<div>', {id: "editorModal", class: "ui-modal", "data-width": "80%"});
                    $invoker = $('<a>', {class: 'hide', 'data-modal-id': 'editorModal'});
                    $editContainer = $el.parent();
                    $container = $(editor.getContainer());
                    $body = $(editor.dom.doc.body);

                    $editContainer.append($modal);
                    $editContainer.append($invoker);

                    selector = new Selector({
                        container: 'editorModal',
                        getUrl: '/media/getUploadedJson',
                        postUrl: '/media/createJson',
                    });

                    selector.registerCallback( file => {
                        editor.execCommand('mceImage');
                        $invoker.trigger('click');

                        let $inputs = $('.mce-window[aria-label="Insert/edit image"]').find('input'),
                            $source = $($inputs[0]),
                            $desc = $($inputs[1]);

                        $source.val(file.path);
                        $desc.val(file.title);
                    });

                   handlersFromOrigin = $el.data('event-handlers');

                   for(let eventType in handlersFromOrigin) {
                       handlers = handlersFromOrigin[eventType];
                       handlers.forEach( handler => $body.on(eventType, handler));
                   }

                });

                editor.on('remove', function (e) {

                    $modal.remove();
                    $invoker.remove();
                    selector = null;

                });

                editor.addButton('image-selector', {
                    icon: 'image',
                    tooltip: 'Insert/edit image',
                    classes: 'editorImageSelector',
                    onclick: function() {
                        selector.refresh();
                        $invoker.trigger('click');
                    },
                });

            }
        };

        options = Object.assign({}, defaults, options, params);

        tinymce.init(options);

    });


}

module.exports = initEditor;

