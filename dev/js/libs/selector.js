'use strict';

const $ = require('jquery'),
    imagePreview = require('./imagePreview');

require('jquery-ui/ui/widgets/progressbar');



class FileSelector {

    constructor(options) {

        let {container, callback, getUrl, postUrl, preview, fieldName} = options;

        this.callbacks = [];
        this.container = $('#' + container);
        this.list = null;
        this.form = null;
        this.progress = null;
        this.preview = null;
        this.getUrl = getUrl;
        this.postUrl = postUrl;
        this.fieldName = fieldName || 'upload';
        this.defSrc = preview || '/web/images/file-alt.svg';

        callback && this.callbacks.push(callback);

        this.init();

    }

    init() {
        let self = this;
        this.el = $('<div></div>', {class: 'file-selector-container'});
        this.list = this._getList();
        this.form = this._getForm();
        this.progress = this.form.find('.file-selector-progress')
            .progressbar({value: false})
            .closest('.file-selector-progress-layout')
            .addClass('hide');
        this.preview = this.form.find('.file-selector-preview');

        this.el.append(this.list, this.form);
        this.container.append(this.el);

        this.form.on('change', '[type=file]', function() {
            if(this.files && this.files[0])  self.handle(this.files[0], $(this))
        });

        this.list.on('click', 'li.file', function() {
            let file = $(this).data('file');
            self.triggerCallbacks(file);
        });

        imagePreview('#' + this.container.attr('id') + ' [type=file]', this.preview);
    }

    refresh() {

        this.list.html('<div class="file-selector-loader"></div>');

        setTimeout(() => {

            $.ajax(this.getUrl, {
                success: data => {
                    let files = JSON.parse(data);

                    this.list.html('');
                    this.addFiles(files);
                },

                error: () => {
                    this.list.html('An error happens while download assets!');
                }

            });

        }, 1000);

    }

    execute(cb) {

        let func = file => {
            cb && cb(file);
            this.callbacks.splice( this.callbacks.indexOf(func), 1);
        };

        console.log('executed');

        this.registerCallback(func);

        this.refresh();

    }

    addFiles(files) {

        files = Array.isArray(files) ? files : [files];

        let frag = files.reduce( (frag, file) => {
            let $el = $('<li class="file"><figure><img src="' + file.path + '" alt="' + file.title + '"></figure></li>');
            $el.data('file', file);
            frag.append($el);
            return frag;
        }, $(document.createDocumentFragment()));

        this.list.append(frag);

    }

    upload(file, cb) {

        let data = new FormData();

        data.append('upload', file);

        $.ajax({
            url: this.form.attr('action'),
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: cb
        });

    }

    handle(file, $el) {

        this.progress.removeClass('hide');

        this.upload(file, answer => {
            this.addFiles(JSON.parse(answer));
            this.preview.attr('src', this.defSrc);
            this.progress.addClass('hide');
            $el.html('');
        });

    }

    registerCallback(cb) {
        this.callbacks.push(cb);
    }

    triggerCallbacks(file) {

        this.callbacks.forEach(cb => cb(file));
    }

    _getList() {
        return $('<ul class="file-selector-list"></ul>');
    }

    _getForm() {

        let $form = $('<form>')
                .attr('action', this.postUrl)
                .addClass('file-selector-form');

        $form.html(
            '<div class="form-group"> <figure> ' +
                '<img src="' + this.defSrc + '" alt="upload" class="file-selector-preview">' +
                '<div class="file-selector-progress-layout">' +
                    '<div class="file-selector-progress"></div>' +
                    '</div>' +
                '</figure>' +
                '<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />' +
                '<label class="button">' +
                '<input type="file" name="' + this.fieldName + '" style="display:none;" accept="image/*">' +
                '<span class="fa fa-upload" aria-hidden="true"></span>Upload file</label>' +
            '</div>'
        );

        return $form;

    }

}

module.exports = FileSelector;
