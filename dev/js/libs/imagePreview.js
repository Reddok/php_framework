'use strict';

const $ = require('jquery');

function imagePreview(inputSelector, prevSelector, cb) {

    let $preview,
        reader = new FileReader();

    if(!prevSelector) prevSelector = '#' + $(inputSelector).attr('data-preview');
    $preview = typeof prevSelector === 'string'? $(prevSelector) : prevSelector;

    $(document).on('change', inputSelector, function() {

        if(this.files && this.files[0]) {

            reader.onload = (e) => {
                $preview.attr('src', e.target.result);
                cb && cb(this.files[0]);
            };

            reader.readAsDataURL(this.files[0]);

        }

    });

}

module.exports = imagePreview;
