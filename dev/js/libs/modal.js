const $ = require('jquery'),
    $root = $(document.body),
    tinymce = require('tinymce/tinymce'),
    initEditor = require('./editor');

require('jquery-ui/ui/widgets/dialog');

let defaults = {
    resizable: false,
    draggable: false,
    modal: true,
    closeText: '',
    closeOnEscape: true,
    show: 300,
    hide: 300,
    height: "auto",
    width: "auto",
    open: e => {
        $root.addClass('overflow-hidden');

        if(tinymce.activeEditor) {

            let target = e.target,
                id = tinymce.activeEditor.id;

            if( $(target).find('#' + id).length ) {

                tinymce.activeEditor.remove();
                setTimeout(function(){
                    initEditor({selector: '#' + id});
                },300);

            }

        }
    },
    close: () => {
        $root.removeClass('overflow-hidden');
    }
};

function createSubmit(cb, message = 'Are you wanna do this thing', title = 'Submit') {

        let $container = $('<div>');

        $container.text(message).dialog(Object.assign({}, defaults, {
            title: title,
            width: 400,
            classes: {"ui-dialog": "ui-corner-all ui-confirm"},
            buttons: {
                "Submit": function() {
                    $container.dialog( "close" );
                    cb();
                },
                "Cancel": function() {
                    $container.dialog( "close" );
                }
            },
            close: function() {
                defaults.close.call(this);
                $container.dialog('destroy');
            }
        }));

}

function createModal ($el, title = 'Submit') {

    $el.dialog(Object.assign({}, defaults,{
        autoOpen: false,
        title: title || $el.attr('data-title') || "Modal window",
        width: $el.attr('data-width') || 'auto',
        height: $el.attr('data-height') || 'auto'
    }));

}

module.exports = {createSubmit, createModal};