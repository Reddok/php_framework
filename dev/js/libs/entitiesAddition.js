'use strict';

const $ = require('jquery'),
    container = {};

let categoriesUrl = '/categories/createAjax',
    tagsUrl = '/tags/createAjax';

container.addCategory =  function(category) {

    return new Promise( (res, rej) => {

        $.ajax({
            url: categoriesUrl,
            method: 'POST',
            data: {category: {title: category}},
            success: function(data) {
                data = JSON.parse(data);
                res(data);
            }
        })

    } );

};

container.addTag = function(tag) {

    return new Promise( (res, rej) => {

        $.ajax({
            url: tagsUrl,
            method: 'POST',
            data: {tag: {title: tag}},
            success: function (data) {
                try {
                    data = JSON.parse(data);
                } catch($e) {
                    console.log(data);
                }

                res(data);
            }
        })
    })

};

module.exports = container;