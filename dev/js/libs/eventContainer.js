'use strict';

const $ = require('jquery');

let oldOn = $.fn.on;
$.fn.on = function(data, target, handler) {

    let $elem;

    if(typeof target === 'function') {
        handler = target;
        $elem = this;
    } else {
        $elem = $(target);
    }

    let handlers = this.data('event-handlers') || {};
    handlers[data] = handlers[data] || [];
    handlers[data].push(handler);
    $elem.data('event-handlers', handlers);
    oldOn.apply(this, arguments);

};