'use strict';

const $ = require('jquery'),
    $container = $('#popupMessages'),
    interval = 1000;

require('jquery-ui/ui/widgets/dialog');

$container.dialog({

    resizable: false,
    draggable: false,
    closeText: '',
    autoOpen: false,
    position: {at: 'center top'},
    classes: {
        "ui-dialog": "transparent",
        "ui-dialog-titlebar": "none",
    },
    hide: 1000,
    create: function (event) { $(event.target).parent().css('position', 'fixed') }
});

function registerMessage(message, type='success') {

    let $el = $('<div>', {class: 'message message-' + type}).text(message);

    $container.prepend($el);
    $container.dialog('isOpen') || $container.dialog('open');

    setTimeout(() => {
        $el.fadeOut(1000, () => {
            $el.remove();
            if(!$container.find('.message').length) $container.dialog('close');
        });
    }, interval);


}

module.exports = registerMessage;