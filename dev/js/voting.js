'use strict';

const $ = require('jquery'),
    registerMessage = require('./libs/message');

$(document).on('click', '[data-role=voteButton]', function(e) {

    let $this = $(this),
        $container = $this.closest('[data-role=voteContainer]'),
        $countEl = $container.find('[data-role=voteCount]'),
        $buttonsContainer = $container.find('.buttons'),
        $buttons = $container.find('[data-role=voteButton]'),
        url = $this.attr('href');


    $.ajax({
        'url' : url,
        success: function(data) {
            console.log(data);
            $countEl.text( data );
            $buttonsContainer.removeClass('buttons').css({margin: 0});

            $buttons.remove();
            $container.addClass(data >= 0? 'positive' : 'negative');

            registerMessage('Your vote successfully added!');
        },
        error: function(jqXHR) {
            console.log(jqXHR);
            registerMessage('An error happens!', 'error');
        }
    });

    e.preventDefault();

});