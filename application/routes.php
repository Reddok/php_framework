<?php

    return [

        [
            'routes' => [
                'register' => 'user/create',
                'login' => 'user/login',
                'reset' => 'user/reset',
                'error/:code' => 'error/get',
                'changePassword' => 'user/changePassword',
                'logout' => 'user/logout'
            ],

            'permissions' => function() {
                return true;
            }
        ],

        [
            'routes' => [

                'posts' => 'post/index',
                'categories' => 'category/index',
                'tags' => 'tag/index',
                'posts/:id' => 'post/get',
                'statistic' => 'statistic/index',
                '' => 'post/index'
            ],
            'permissions' => function($req) {
                return User::checkPermission($req['user'], ['postReadSelf', 'postReadAnother']);
            }
        ],

        [
            'routes' => [

                'posts/:id/upvote' => 'post/upvote',
                'posts/:id/devote' => 'post/devote',
                'comments/:id/upvote' => 'comment/upvote',
                'comments/:id/devote' => 'comment/devote',
                'media/getUploadedJson' => 'media/getUploadedJson',
                'media/createJson' => 'media/createJson',
                'files' => 'media/index',
                'files/:id/delete' => 'media/delete'
            ],
            'permissions' => function($req) {
                return isset($req['user']['id']) && User::checkPermission($req['user'], ['postReadSelf', 'postReadAnother']);
            }
        ],

        [
            'routes' => [
                'posts/create' => 'post/create',
                'posts/:id/edit' => 'post/edit',
                'categories/createAjax' => 'category/createAjax',
                'tags/createAjax' => 'tag/createAjax',
                'tags/recommended' => 'tag/getRecommendedTags'
            ],
            'permissions' =>  function($req) {

                $postId = $req['params']['id'] ?? null;
                $user = $req['user'];

                if(!$postId || $user['id'] === (new Post())->getById($postId)['authorId'] ) {
                    return User::checkPermission($user, 'postWriteSelf');
                } else {
                    return User::checkPermission($user, 'postWriteAnother');
                }

            }
        ],

        [
            'routes' => [
                'posts/:id/delete' => 'post/delete',
            ],
            'permissions' => function($req) {

                $user = $req['user'];
                $post = (new Post())->getById($req['params']['id']);

                if($user['id'] === $post['authorId']) {
                    return User::checkPermission($user, 'postDeleteSelf');
                } else {
                    return User::checkPermission($user, 'postDeleteAnother');
                }
            }
        ],

        [
            'routes' => [
                'users/:id' => 'user/get',
            ],
            'permissions' => function($req) {
                return User::checkPermission($req['user'], ['userReadSelf', 'userReadAnother']);
            }
        ],

        [
            'routes' => [
                'users' => 'user/index',
                'users/:id/edit' => 'user/edit',
            ],
            'permissions' =>  function($req) {

                $user = $req['user'];

                if($req['params']['id'] === $user['id']) {
                    return User::checkPermission($user, 'userWriteSelf');
                } else {
                    return User::checkPermission($user, 'userWriteAnother');
                }

            }
        ],

        [
            'routes' => [
                'users/:id/delete' => 'user/delete',
            ],
            'permissions' => function($req) {

                $user = $req['user'];
                if($req['params']['id'] === $user['id']) {
                    return User::checkPermission($user, 'userDeleteSelf');
                } else {
                    return User::checkPermission($user, 'userDeleteAnother');
                }

            }
        ],

        [
            'routes' => [
                'comments/create' => 'comment/create',
                'comments/:id/edit' => 'comment/edit',
            ],
            'permissions' => function($req) {

                $user = $req['user'];
                if(!isset($req['params']['id']) || $user['id'] === ( new Comment() )->getById($req['params']['id'])['authorId']) {
                    return User::checkPermission($user, 'commentWriteSelf');
                } else {
                    return User::checkPermission($user, 'commentWriteAnother');
                }

            }
        ],

        [
            'routes' => [
                'comments/:id/delete' => 'comment/delete',
            ],
            'permissions' => function($req) {

                $user = $req['user'];
                if(isset($req['params']['id']) || $user['id'] === ( new Comment() )->getById($req['params']['id'])['authorId']) {
                    return User::checkPermission($user, 'commentDeleteSelf');
                } else {
                    return User::checkPermission($user, 'commentDeleteAnother');
                }

            }
        ],

        [
            'routes' => [
                'pages' => 'page/index'
            ],
            'permissions' => function($req) {
                return User::checkPermission($req['user'], 'pageRead');
            }
        ],

        [
            'routes' => [
                'pages/:id/edit' => 'page/edit',
                'pages/create' => 'page/create'
            ],
            'permissions' => function($req) {
                return User::checkPermission($req['user'], 'pageWrite');
            }
        ],
        [
            'routes' => [
                'pages/:id/delete' => 'page/delete'
            ],
            'permissions' => function($req) {
                return User::checkPermission($req['user'], 'pageDelete');
            }
        ],

        [
            'routes' => [
                '*' => 'page/get'
            ],
            'permissions' => function(&$req) {
                $path = $req['url'];

                if( !(new Page())->checkExist($path) ) return false;

                $req['params']['path'] = $req['url'];
                return true;

            }
        ]



    ];