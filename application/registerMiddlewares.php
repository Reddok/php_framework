<?php

    use Framework\{Controller, HttpException};

    $framework->registerMiddleware(new AuthorizeMiddleware());


    $framework->setErrorHandler(function(HttpException $e) {

        $code = $e->getCode();
        $message =  $e->getMessage();


        $headers = $e->getHeaders();

        if(AjaxHelper::isAjax()) {

            foreach($headers as $header) {
                header($header);
            }

            print json_encode( compact('code', 'message') );
        } else {

            ErrorHelper::setError($message);
            ErrorHelper::setHeaders($headers);

            Controller::redirect('error/' . $code);
        }



    });
