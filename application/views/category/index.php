        <main>
            <article class="categories-page">
                <h1>All categories</h1>
                <ol class="categories-list">

                    <?php foreach($categories as $category): ?>
                        <li><a href="/posts?filterType=category&filterValue=<?= $category['id'] ?>"><?= $category['title'] ?> [<?= $category['postsCount'] ?>]</a></li>
                    <?php endforeach ?>
                </ol>
            </article>
        </main>

