<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?=$title?></title>
    <link rel="shortcut icon" href="/favicon.png" type="image/png"/>
    <link rel="stylesheet" href="/web/css/style.css">
    <!--[if lt IE 9]>
    <script src="/web/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>

<div id="page">
    <header>
        <div class="top-line">
            <div class="logo-container">
                <img src="/web/images/logo.png" alt="logo" class="logo">
                <div class="sitename">Blog.iT</div>
            </div>
            <?php Framework\Menu::getMenu('main') ?>
        </div>
    </header>
    <div id="content">