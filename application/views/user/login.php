<main>
    <article class="login-page">
        <ul class="form-errors">
            <?php foreach($errors as $error):?>
                <li><?=$error?></li>
            <?php endforeach; ?>
        </ul>
        <h2>Login</h2>
        <form action="/login" method="post" id="loginForm" enctype="multipart/form-data">
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" id="email" name="user[email]" placeholder="Enter email" value="<?=$data['email']?>" required>
            </div>
            <div class="form-group">
                <label for="pass">Password:</label>
                <input type="password" id="pass" name="user[password]" placeholder="Enter password" required>
            </div>
            <div class="buttonset">
                <button type="submit">Login</button>
                <button type="reset">Reset</button>
            </div>
        </form>
        <div class="registerLink">
            <a class="addition" href="/register">Registration</a>
            <a class="addition" href="/reset">Forgot password</a>
        </div>
    </article>
</main>