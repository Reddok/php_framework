<main>
    <article>
        <form action="/changePassword?case=<?=$case?>" method="post">
            <ul class="form-errors">
                <?php foreach($errors as $error):?>
                    <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" required>
            </div>
            <div class="form-group">
                <label for="confirm">Confirm password:</label>
                <input type="password" name="confirm" id="confirm" required>
            </div>
            <button type="submit">Reset password</button>
        </form>
    </article>
</main>
