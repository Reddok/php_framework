<main>
    <article class="registration-page">
        <ul class="form-errors">
            <?php foreach($errors as $error):?>
                <li><?=$error?></li>
            <?php endforeach; ?>
        </ul>
        <form action="/reset" method="post">
            <div class="form-group">
                <label for="forgot">Enter your email:</label>
                <input type="email" id="forgot" name="forgot" required placeholder="email...">
            </div>
            <button type="submit">Confirm</button>
        </form>
    </article>
</main>