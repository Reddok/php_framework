<main>
    <article class="registration-page">
        <ul class="form-errors">
            <?php foreach($errors as $error):?>
                <li><?=$error?></li>
            <?php endforeach; ?>
        </ul>
        <h2>Registration</h2>
        <form action="/register" method="post" id="registration-form" enctype="multipart/form-data">
            <div class="columns">
                <div class="form-group">
                    <label for="email">Email*:</label>
                    <input type="email" id="email" name="user[email]" placeholder="Enter email" required>
                </div>
                <div class="form-group">
                    <label for="username">Username*:</label>
                    <input type="text" name="user[username]" id="username" placeholder="Enter username" required>
                </div>
                <div class="form-group">
                    <label for="pass">Password*:</label>
                    <input type="password" id="pass" name="user[password]" placeholder="Enter password" required>
                </div>
                <div class="form-group">
                    <label for="repeatPass">Confirm password*:</label>
                    <input type="password" id="repeatPass" name="user[repeat]" placeholder="Repeat password" required>
                </div>
                <div class="form-group">
                    <label for="firstname">First name:</label>
                    <input type="text" id="firstname" name="user[firstName]" placeholder="Enter first name">
                </div>
                <div class="form-group">
                    <label for="lastname">Last name:</label>
                    <input type="text" id="lastname" name="user[lastName]" placeholder="Enter last name">
                </div>
            </div>
            <div class="form-group avatarGroup">
                <figure>
                    <img src="/web/images/defaultAvatar.png" alt="emptyAvatar" id="uploadPreview">
                </figure>
                <label class="button">
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <input type="file" name="avatar" data-preview="uploadPreview"/>
                    <span class="fa fa-upload" aria-hidden="true"></span>Upload avatar
                </label>
            </div>
            <div class="form-group">
                <label for="bio">Biography:</label>
                <textarea id="bio" name="user[bio]" placeholder="Tell something about yourself"></textarea>
            </div>
            <div class="buttonset">
                <button type="submit">Registration</button>
                <button type="reset">Reset</button>
            </div>
        </form>
    </article>
</main>