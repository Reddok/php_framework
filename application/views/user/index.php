<main>
    <article class="usersPage">
    <h1>Registered users</h1>
        <table class="usersTable">
            <thead>
            <tr>
                <th>#</th>
                <th>Avatar</th>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                <th>Rating</th>
                <th>Banned</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($users as $i => $user): ?>
                <tr>
                    <td><a href="/users/<?=$user['id']?>/edit"><?=$i + 1?></a></td>
                    <td><a href="/users/<?=$user['id']?>/edit"><img width="32px" src="<?=User::getAvatar($user)?>" alt="<?=$user['username']?>"/></a></td>
                    <td><a href="/users/<?=$user['id']?>/edit"><?=$user['username']?></a></td>
                    <td><a href="/users/<?=$user['id']?>/edit"><?=$user['email']?></a></td>
                    <td><a href="/users/<?=$user['id']?>/edit" class="<?=lcfirst(User::getRoleName($user['role']))?>"><?=User::getRoleName($user['role'])?></a></td>
                    <td class="userRating <?=$user['rating'] >= 0? 'positive' : 'negative'?>"><a href="/users/<?=$user['id']?>/edit"><?=$user['rating']?></a></td>
                    <td>
                        <?php if($currentUser['id'] !== $user['id']): ?>
                            <a href="/users/<?=$user['id']?>/delete" class="ui-submit" data-message="Are you really want to ban this user?">
                                Ban <span class="fa fa-gavel"></span>
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>

    </article>
</main>
