
    <form class="user-panel-form" action="/users/<?=$user['id']?>/edit" method="post" enctype="multipart/form-data">

        <main>

            <fieldset class="user-edit">
                <h1>Edit user: <?=$user['username']?></h1>

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" name="user[email]" id="email" placeholder="email..." value="<?=$user['email']?>" required>
                </div>

                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" name="user[username]" id="username" placeholder="username..." required minlength="3" value="<?=$user['username']?>">
                </div>

                <div class="form-group">
                    <label for="firstName">First Name:</label>
                    <input type="text" name="user[firstName]" id="firstName" placeholder="first name..." value="<?=$user['firstName']?>">
                </div>

                <div class="form-group">
                    <label for="lastName">Last Name:</label>
                    <input type="text" name="user[lastName]" id="lastName" placeholder="lastname..." value="<?=$user['lastName']?>">
                </div>

                <div class="form-group">
                    <label for="bio">Biography:</label>
                    <textarea name="user[bio]" id="bio" placeholder="biography..."><?=$user['bio']?></textarea>
                </div>

            </fieldset>

        </main>

        <aside>

            <fieldset class="avatarSection">
                <div class="form-group" data-role="container">
                    <label for="avatar">Avatar:</label>
                    <figure>
                        <img src="<?=User::getAvatar($user)?>" alt="<?=$user['username']?>" class="avatarPreview" data-role="preview">
                    </figure>
                    <input type="hidden" name="user[avatar]" value="<?=$user['avatarId']?>" data-role="value"/>
                    <button class="file-input" data-modal-id="uploadedFiles" id="avatar" data-role="invoker">Select File</button>
                </div>
            </fieldset>

            <?php if( User::checkPermission($currentUser, 'userWriteAnother') ):?>
                <fieldset class="role-section">
                    <div class="form-group" data-role="container">
                        <label for="role">Role:</label>
                        <select name="user[role]" id="role">
                            <?php foreach($roles as $role):?>
                                <option value="<?=$role['id']?>" <?php if($role['id'] === $user['role']['id']): ?> selected <?php endif; ?>><?=$role['title']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </fieldset>
            <?php endif; ?>

            <fieldset class="user-panel-buttons">
                <button type="submit"><span class="fa fa-pencil-square" aria-hidden="true"></span>  Edit</button>
                <a href="/reset" class="button" id="changePassword"><i class="fa fa-unlock" aria-hidden="true"></i> Change password</a>
            </fieldset>

        </aside>

    </form>

<div id="uploadedFiles" class="ui-modal" data-title="Uploaded images" data-width="80%" data-height="500">
</div>
