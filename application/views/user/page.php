
<main>
    <article class="user-page">

        <?php if(!$user['deletedAt']): ?>

        <figure class="user-avatar-wrapper">
            <img class="user-avatar" src="<?=User::getAvatar($user)?>" alt="<?=$user['username']?>">
        </figure>
        <div class="user-data">
            <div class="user-label">Nickname:</div>
            <div class="user-field"><?=$user['username']?></div>
            <div class="user-label">Real name:</div>
            <div class="user-field">
                <?php if( $user['firstName'] || $user['lastName']):?>
                    <?=$user['firstName']?> <?=$user['lastName']?>
                <?php else: ?>
                    --
                <?php endif; ?>
            </div>
            <div class="user-label">Registered:</div>
            <div class="user-field"><?=$user['createdAt']?></div>
            <div class="user-label">Last activity:</div>
            <div class="user-field"><?=(new DateTime($user['lastAttend']))->format('H:i:s | d F Y')?></div>
        </div>
        <div class="user-bio">
            <h3>Short bio:</h3>
            <div>
                <?= empty( $user['bio'] )? '--' : $user['bio'] ?>
            </div>
        </div>

        <footer>
            <?php

            if($currentUser['id'] === $user['id'] && User::checkPermission($currentUser, 'postWriteSelf') || User::checkPermission($currentUser, 'postWriteAnother')): ?>
                <div class="button-set">
                    <a class="button" href="/users/<?=$user['id']?>/edit">Edit</a>
                    <a class="button" href="/reset">Change password</a>
                </div>
            <?php endif; ?>

            <a href="/posts?filterType=author&filterValue=<?=$user['id']?>" class="button publications">Publication [<?=$user['posts']?>]</a>
        </footer>

        <?php else: ?>

        <div class="userDeleted">
            This user has been deleted.
        </div>

        <?php endif; ?>
    </article>
</main>