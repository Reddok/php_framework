<section class="tags-cloud-container">
    <h2>Tags cloud:</h2>
    <div>
        <canvas width="250" height="250" id="tags-cloud">
            <p>Anything in here will be replaced on browsers that support the canvas element</p>
            <ul>
                <?php foreach($tags as $tag): ?>
                    <li>
                        <a href="/posts?filterType=tag&filterValue=<?=$tag['id']?>" data-weight="<?=$tag['postsCount']?>"><?=$tag['title']?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </canvas>
    </div>
    <a class="addition" href="/tags">All tags >></a>
</section>