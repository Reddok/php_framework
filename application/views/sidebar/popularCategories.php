<section class="categories-list-container">
    <h2>Popular categories:</h2>
    <ol class="categories-list">
        <?php foreach($categories as $category):?>
            <li><a href="/posts?filterType=category&filterValue=<?=$category['id']?>"><?=$category['title']?> [<?=$category['postsCount']?>]</a></li>
        <?php endforeach;?>
    </ol>
    <a class="addition" href="/categories">All categories >></a>
</section>