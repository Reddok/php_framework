<section class="statistics-container">
    <h2>Best authors:</h2>
    <table class="stats-list">
        <tbody>
        <?php for($i = 0, $max = count($users); $i < $max; $i++): ?>

            <tr>
                <td><a href="/users/<?=$users[$i]['id']?>"><?=$i + 1?></a></td>
                <td><a href="/users/<?=$users[$i]['id']?>"><img class="author-avatar" src="<?=User::getAvatar($users[$i])?>" alt="<?=$users[$i]['username']?>"></a></td>
                <td><a href="/users/<?=$users[$i]['id']?>"><?=$users[$i]['username']?></a></td>
                <td><a href="/users/<?=$users[$i]['id']?>">[<?=$users[$i]['postsCount']?>]</a></td>
            </tr>
        <?php endfor; ?>
        </tbody>
    </table>
    <a class="addition" href="/statistic">More statistics >></a>
</section>