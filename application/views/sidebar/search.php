<section class="search-container">
    <h2>Пошук:</h2>
    <form action="/" method="get">
        <button class="search-button" type="submit">></button>
        <input type="text" name="search" placeholder="Enter something..." required minlength="3" value="<?=$_GET['search']?>">
    </form>
</section>