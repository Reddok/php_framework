<section class="author-container">
    <h2>Author</h2>
    <a href="/user/get/<?=$post['authorId'] ?>">
        <figure>
            <img class="author-avatar" src="<?=$post['authorAvatar'] ?>" alt="<?php print $post['authorName'] ?>">
            <figcaption><?php print $post['authorName'] ?></figcaption>
        </figure>
    </a>
</section>