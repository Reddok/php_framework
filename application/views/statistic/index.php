<main>
    <article class="statistic-page">
        <h1>Statistics</h1>
        <section class="general">
            <h2>General</h2>
            <dl>
                <?php foreach($counters as $counterKey => $counterValue): ?>
                    <dt>Count of <?=$counterKey?>:</dt>
                    <dd><?=$counterValue?></dd>
                <?php endforeach; ?>
                <dt>Last registered user:</dt>
                <dd><?=$lastRegisteredUser['username']?></dd>
            </dl>
        </section>
        <section>
            <h2>Best authors:</h2>
            <table class="stats-list">
                <thead>
                <tr>
                    <th><a href="#">#</a></th>
                    <th ><a href="#"><span class="fa fa-user" aria-hidden="true" ></span> User</a></th>
                    <th><a href="#"><span class="fa fa-star" aria-hidden="true"></span> Rating</a></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($bestAuthors as $index => $author): ?>
                    <tr>
                        <td><a href="/users/<?=$author['id']?>"><?=$index+1?></a></td>
                        <td><a href="/users/<?=$author['id']?>" class="authorMeta"><img class="author-avatar" src="<?=$author['avatar']?>" alt="<?=$author['username']?>"><span><?=$author['username']?></span></a></td>
                        <td><a href="/users/<?=$author['id']?>"><?=$author['rating']?></a></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </section>
        <section class="best-posts">
            <h2>Best posts:</h2>
            <table class="stats-list">
                <thead>
                <tr>
                    <th><a href="#">#</a></th>
                    <th><a href="#"><span class="fa fa-file-text" aria-hidden="true" ></span> Post</a></th>
                    <th><a href="#"><span class="fa fa-star" aria-hidden="true"></span> Rating</a></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($bestPosts as $index => $post): ?>
                    <tr>
                        <td><a href="/posts/<?=$post['id']?>"><?=$index+1?></a></td>
                        <td><a href="/posts/<?=$post['id']?>"><?=$post['title']?></a></td>
                        <td><a href="/posts/<?=$post['id']?>"><?=$post['rating']?></a></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </section>
        <section>
            <h2>Most active authors:</h2>
            <table class="stats-list">
                <thead>
                <tr>
                    <th><a href="#">#</a></th>
                    <th><a href="#"><span class="fa fa-user" aria-hidden="true" ></span> User</a></th>
                    <th><a href="#"><span class="fa fa-book" aria-hidden="true"></span> Publications</a></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($activeAuthors as $index => $author): ?>
                    <tr>
                        <td><a href="/users/<?=$author['id']?>"><?=$index+1?></a></td>
                        <td><a href="/users/<?=$author['id']?>"  class="authorMeta"><img class="author-avatar" src="<?=$author['avatar']?>" alt="<?=$author['username']?>"><span><?=$author['username']?></span></a></td>
                        <td><a href="/users/<?=$author['id']?>"><?=$author['postsCount']?></a></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </section>
        <section>
            <h2>Hottest posts:</h2>
            <table class="stats-list">
                <thead>
                <tr>
                    <th><a href="#">#</a></th>
                    <th><a href="#"><span class="fa fa-file-text" aria-hidden="true" ></span> Post</a></th>
                    <th><a href="#"><span class="fa fa-commenting" aria-hidden="true"></span> Comments</a></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($hottestPosts as $index => $post): ?>
                    <tr>
                        <td><a href="/posts/<?=$post['id']?>"><?=$index+1?></a></td>
                        <td><a href="/posts/<?=$post['id']?>"><?=$post['title']?></a></td>
                        <td><a href="/posts/<?=$post['id']?>"><?=$post['commentsCount']?></a></td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </section>
    </article>
</main>


