
    <main>
        <article class="pagesPage">
            <h1>Pages</h1>

            <table class="pages">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Parent</th>
                    <th>Show in menu</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>

                <?php if($pages): ?>

                    <?php foreach($pages as $page): ?>
                        <tr>
                            <td><a href="/pages/<?=$page['id']?>/edit"><?=$page['title']?></a></td>
                            <td><a href="/pages/<?=$page['id']?>/edit"><?=$page['parent']? $page['parent'] : '<span class="fa fa-minus" aria-hidden="true"></span>' ?></a></td>
                            <td><a href="/pages/<?=$page['id']?>/edit"><?=$page['showInMenu']? '<span class="fa fa-check" aria-hidden="true"></span>' : '<span class="fa fa-minus" aria-hidden="true"></span>'?></a></td>
                            <td><a href="/pages/<?=$page['id']?>/delete" class="ui-submit" data-message="Are you want delete this page?"><span class="fa fa-times" aria-hidden="true"></span></a></td>
                        </tr>
                    <?php endforeach; ?>

                <?php else: ?>

                    <tr class="empty">
                        <td colspan="4">You did not add custom pages yet.</td>
                    </tr>

                <?php endif; ?>


                </tbody>
            </table>
            <a href="/pages/create" class="addPage"><span class="fa fa-plus"></span> Create page</a>
        </article>
    </main>
