

    <form class="user-panel-form" action="/pages/<?=$page['id']?>/edit" method="post">

        <main>

            <fieldset>

                <div class="form-group">
                    <label for="titleField">Title:</label>
                    <input type="text" id="titleField" name="page[title]" placeholder="New page..." value="<?=$page['title']?>" required>
                </div>
                <div class="entity-content-container">
                    <textarea name="page[content]" id="entityContentField" class="editor"><?=$page['content']?></textarea>
                </div>

            </fieldset>

        </main>

        <aside>

            <?php if($errors): ?>
                <fieldset>
                    <h2>Errors: </h2>
                    <ul class="form-errors">
                        <?php foreach($errors as $error):?>
                            <li><?=$error?></li>
                        <?php endforeach; ?>
                    </ul>
                </fieldset>
            <?php endif; ?>

            <fieldset class="user-panel-buttons">
                <button type="submit"><span class="fa fa-sticky-note" aria-hidden="true"></span>Update</button>
                <button type="reset"><span class="fa fa-reply" aria-hidden="true"></span>Reset</button>
                <a href="/pages/<?=$page['id']?>/delete" class="button delete-button ui-submit" data-message="Are you want delete this page?"><span class="fa fa-times-circle" aria-hidden="true"></span>Delete</a>
            </fieldset>

            <fieldset>
                <div class="form-group">
                    <label for="parentPage">Parent:</label>
                    <select name="page[parent]" id="parentPage">
                        <option value="">--</option>

                        <?php foreach($otherPages as $otherPage): ?>
                            <option value="<?=$otherPage['id']?>" <?php if($otherPage['id'] === $page['parent']): ?> selected <?php endif; ?>><?=$otherPage['title']?></option>
                        <?php endforeach ?>

                    </select>
                </div>
            </fieldset>

            <fieldset>
                <div class="form-group inline">
                    <label for="showOnMenu">Show on menu:</label>
                    <input type="checkbox" name="page[show]" id="showOnMenu" value="show" <?php if($page['showInMenu']):?> checked <?php endif; ?>>
                </div>
            </fieldset>

        </aside>

    </form>
