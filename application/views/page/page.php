<main>
    <article>
        <h1><?=$page['title'] ?></h1>
        <div class="post-content">
            <?=$page['content'] ?>
        </div>
        <div class="post-footer">
            <div class="date-posted"><?=$page['createdAt']?></div>
            <a href="/" class="button back"><span class="fa fa-undo" aria-hidden="true"></span> Back to main</a>
        </div>
    </article>
</main>