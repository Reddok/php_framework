
    <form class="user-panel-form" action="/pages/create" method="post">

        <main>

            <fieldset>

                <div class="form-group">
                    <label for="titleField">Title:</label>
                    <input type="text" id="titleField" name="page[title]" placeholder="New page..." required>
                </div>
                <div class="entity-content-container">
                    <textarea name="page[content]" id="entityContentField" class="editor"></textarea>
                </div>

            </fieldset>

        </main>

        <aside>

            <?php if($errors): ?>
                <fieldset>
                    <h2>Errors: </h2>
                    <ul class="form-errors">
                        <?php foreach($errors as $error):?>
                            <li><?=$error?></li>
                        <?php endforeach; ?>
                    </ul>
                </fieldset>
            <?php endif; ?>

            <fieldset class="user-panel-buttons">
                <button type="submit"><span class="fa fa-sticky-note" aria-hidden="true"></span>Create</button>
                <button type="reset"><span class="fa fa-reply" aria-hidden="true"></span>Reset</button>
            </fieldset>

            <fieldset>
                <div class="form-group">
                    <label for="parentPage">Parent:</label>
                    <select name="page[parent]" id="parentPage">
                        <option value="">--</option>

                        <?php foreach($pages as $page): ?>
                            <option value="<?=$page['id']?>"><?=$page['title']?></option>
                        <?php endforeach ?>

                    </select>
                </div>
            </fieldset>

            <fieldset>
                <div class="form-group inline">
                    <label for="showOnMenu">Show on menu:</label>
                    <input type="checkbox" name="page[show]" id="showOnMenu" value="show">
                </div>
            </fieldset>

            <fieldset>
                <div class="form-group">
                    <label for="slug">Url:</label>
                    <input type="text" id="slug" name="page[slug]" placeholder="page1...">
                </div>
            </fieldset>

        </aside>

    </form>