</div>
<footer>
    <section class="site-information">
        <div>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium dolore ducimus eius facere, ipsum laboriosam pariatur placeat quasi qui sed, tempore.
        </div>
        <div class="copyright">&copy; Blog.IT</div>
    </section>
    <section class="socials">
        <ul>
            <li><a href="#"><img src="/web/images/fb.png" alt="facebook"></a></li>
            <li><a href="#"><img src="/web/images/vk.png" alt="vkontakte"></a></li>
            <li><a href="#"><img src="/web/images/twitter.png" alt="twitter"></a></li>
        </ul>
    </section>
</footer>


<?php
$messages = Framework\Message::getMessages();

if($messages):?>
    <?php foreach($messages as $message): ?>
        <div class="system-message none" data-type="<?=$message['type']?>"><?=$message['message']?></div>
    <?php endforeach; ?>
<?php endif; ?>
<div id="popupMessages"></div>

<script src="/web/js/bundle.js"></script>
</body>
</html>

