
        <main>
            <article class="tags-page">
                <h1>Search for tags:</h1>
                    <div class="form-group">
                        <input name="tagName" type="text" id="tagSearch" placeholder="enter some tagname...">
                    </div>

                <div class="tags">
                    <ul id="tagList">
                        <?php foreach($tags as $tag): ?>
                            <li><a href="/posts?filterType=tag&filterValue=<?=$tag['id']?>"><?=$tag['title']?></a><span><?=$tag['postCount']?></span></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </article>
        </main>

