<?php

foreach($posts as $post): ;?>

    <article class="preview">
        <h2><a href="/posts/<?=$post['id']?>"> <?=$post['title']?> </a></h2>
        <div class="preview-container">
            <figure class="thumbnail-container">
                <img src="<?=$post['thumbnail'] ?>" alt="<?= $post['title'] ?>">
            </figure>
            <div class="preview-place">
                <div class="meta-data">
                    <div class="category">
                        Categories:

                        <?php
                        $output = array_map( function($category) {
                            return '<a href="/posts?filterType=category&filterValue=' . $category['id'] . '">' . $category['title'] . '</a>';
                        }, $post['categories']);

                        print join(', ', $output);
                        ?>

                    </div>
                    <div class="preview-author">
                        <div>
                            <div class="author-name">
                                <a href="/users/<?=$post['authorId'] ?>"><?=$post['authorName'] ?></a>
                            </div>
                            <div class="date-posted"><?=$post['createdAt']?></div>
                        </div>
                        <a href="/user/get/<?=$post['authorId'] ?>"><img class="author-avatar" src="<?=$post['authorAvatar'] ?>" alt="<?=$post['authorName'] ?>"></a>
                    </div>
                </div>
                <div class="preview-content">
                    <?=$post['preview'] ?>
                </div>
            </div>
        </div>

        <div class="preview-footer">
            <div class="tags">
                <span>Related to:</span>
                <ul>
                    <?php foreach($post['tags'] as $tag):?>
                        <li><a href="/posts?filterType=tag&filterValue=<?=$tag['id'] ?>"><?=$tag['title'] ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="post-buttons">
                <a href="/posts/<?=$post['id']?>" class="button">Details >></a>
                <a href="/posts/<?=$post['id']?>" class="button">Comments [<?=$post['comments'] ?>]</a>
            </div>
        </div>
        <div class="postRating <?=$post['rating'] >= 0? 'positive' : 'negative'?>">
            <span class="fa fa-star"></span> <?=$post['rating']?>
        </div>
    </article>


<?php endforeach; ?>