<main class="ajaxLoading">
    <?php if($posts): ?>
        <?php if($filter):?>
            <h1><?=$filter?></h1>
        <?php endif; ?>
        <div id="articles">
            <?php require ROOT . '/application/views/post/indexAjax.php' ?>
        </div>
    <div id="loader"></div>

    <?php else: ?>

    <section class="noPosts">
        <div>We don't have posts to show you...</div>
    </section>

    <?php endif; ?>

</main>
