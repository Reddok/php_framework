
    <form class="user-panel-form" action="/posts/create" method="post" enctype="multipart/form-data">

        <main>

            <fieldset>

                <div class="form-group">
                    <label for="titleField">Title:</label>
                    <input type="text" id="titleField" name="post[title]" placeholder="New post..." required>
                </div>

                <div class="entity-content-container">
                    <textarea name="post[content]" id="entityContentField" class="editor" data-role="autotag-content" data-width="400"></textarea>
                </div>

            </fieldset>

            <fieldset>
                <div class="form-group">
                    <label for="previewField">Preview:</label>
                    <textarea name="post[preview]" id="previewField" placeholder="Short preview"></textarea>
                </div>
            </fieldset>

        </main>

        <aside>

            <?php if($errors): ?>
                <fieldset>
                    <h2>Errors: </h2>
                    <ul class="form-errors">
                        <?php foreach($errors as $error):?>
                            <li><?=$error?></li>
                        <?php endforeach; ?>
                    </ul>
                </fieldset>
            <?php endif; ?>

            <fieldset class="user-panel-buttons">
                <button type="submit"><span class="fa fa-check-square-o" aria-hidden="true"></span>Create</button>
                <button type="reset"><span class="fa fa-reply" aria-hidden="true"></span>Reset</button>
            </fieldset>

            <fieldset class="categoriesSection">
                <div class="form-group">
                    <label for="categoriesField">Categories:</label>
                    <select name="" id="categoriesField">
                        <option value="0">none</option>
                        <?php foreach($categories as $category): ?>
                            <option value="<?=$category['id']?>"><?=$category['title']?></option>
                        <?php endforeach; ?>
                    </select>
                    <ul class="selectedCategories">
                    </ul>


                </div>
                <div class="form-group categoryAddContainer">
                    <input type="text" name="newCategory" placeholder="create new category...">
                </div>
                <button class="addCategory">Add category</button>
            </fieldset>

            <fieldset class="tagsSection">
                <div class="form-group">
                    <label for="tagsField">Tags:</label>
                    <canvas width="250" height="250" id="tags-cloud">
                        <p>Anything in here will be replaced on browsers that support the canvas element</p>
                        <ul class="tagCloudList">
                            <?php foreach($tags as $tag):?>
                                <li>
                                    <a href="javascript:;" data-id="<?=$tag['id']?>" data-weight="<?=$tag['postCount'] + 1?>"><?=$tag['title'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </canvas>
                </div>
                Selected tags:
                <ul class="selectedTags">
                    ...
                </ul>
                <div class="recommendedTags">
                    We have recommended tags for you:
                    <ul data-role="autotag-container">
                    </ul>
                </div>
                <div class="form-group tagAddContainer">
                    <input type="text" name="newTag" placeholder="create new tag...">

                </div>
                <button class="addTag">Add tag</button>
            </fieldset>

            <fieldset class="thumbnailSection" data-role="container">
                <label for="thumbnailField">Thumbnail:</label>
                <figure>
                    <img src="/web/images/defThumb.jpg" alt="nothing" class="thumbnailPreview" data-role="preview">
                </figure>
                <div class="form-group">
                    <input type="hidden" name="post[thumbnail]" data-role="value"/>
                    <button class="file-input" data-modal-id="uploadedFiles" data-role="invoker">Select File</button>
                </div>
            </fieldset>
        </aside>

    </form>

<div id="uploadedFiles" class="ui-modal" data-title="Uploaded images" data-width="80%" data-height="500">
</div>