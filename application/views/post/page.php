<main>
    <article class="postPage">
        <h1><?=$post['title'] ?></h1>
        <div class="post-content">
            <?=$post['content'] ?>
        </div>
        <div class="post-footer">
            <div class="row">
                <div class="tags">
                    <span>Related to:</span>
                    <ul>
                        <?php foreach($post['tags'] as $tag):?>
                            <li><a href="/?tag=<?=$tag['id'] ?>"><?= $tag['title'] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                    <?php if($voted || !(isset($currentUser['id']) && User::checkPermission($currentUser, ['postReadSelf', 'postReadAnother'])) ) : ?>
                        <div class="postRating <?=$post['rating'] >= 0? 'positive' : 'negative'?>" data-role="voteContainer">
                            <span class="fa fa-thumbs-up"></span>
                            <span data-role="voteCount"><?=$post['rating']?></span>
                        </div>
                    <?php else: ?>
                        <div class="postRating" data-role="voteContainer">
                            <span class="buttons">
                                <a href="/posts/<?=$post['id']?>/upvote" data-role="voteButton" class="upvotePost"><span class="fa fa-caret-up"></span></a>
                                <a href="/posts/<?=$post['id']?>/devote" data-role="voteButton" class="devotePost"> <span class="fa fa-caret-down"></span></a>
                                <span class="fa fa-thumbs-up"></span>
                            </span>
                            <span data-role="voteCount"><?=$post['rating']?></span>
                        </div>
                    <?php endif; ?>


            </div>
            <div class="date-posted"><?=$post['createdAt']?></div>
            <a href="/" class="button back"><span class="fa fa-undo" aria-hidden="true"></span> Back to main</a>
        </div>
        <ul class="other-posts">
            <?php foreach($otherPosts as $otherPost): ?>
                <li>
                    <a href="/posts/<?=$otherPost['id']?>">
                        <figure>
                            <div class="img-pruner"><img src="<?=$otherPost['thumbnail']?>" alt="<?=$otherPost['title']?>"></div>
                            <figcaption><?=$otherPost['title'] ?></figcaption>
                        </figure>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </article>
    <section class="comments">
        <div class="write-comment">
            <a href="javascript:;" data-modal-id="commentForm" class="commentButton button"><span class="fa fa-comments-o" aria-hidden="true"></span>Write comment</a>
        </div>

        <?php if($comments):?>

            <ul class="comments-list">
                <?php CommentHelper::printComments($comments, $post, $currentUser) ?>
            </ul>

        <?php else: ?>
            <p class="empty">This post has no comments yet. You can be the first.</p>
        <?php endif; ?>
    </section>
</main>

<form action="/comments/create" id="commentForm" class="ui-modal" data-title="Add comment" method="POST">
    <div class="form-group">
        <textarea id="commentField"  name="comment[content]" class="editor"></textarea>
    </div>
    <div class="form-group">
        <input type="hidden" value="<?=$post['id']?>" name="comment[post]">
        <input type="hidden" value="" name="comment[parent]" class="parentComment">
        <div class="reply-to-container">Reply to: <span class="reply-to"></span></div>
    </div>
    <div class="buttonset">
        <button type="submit">Add comment</button>
        <button type="reset">Clear</button>
    </div>
</form>

