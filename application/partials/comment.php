<li class="comment">
    <div class="comment-meta">
        <div class="comment-user">
            <img class="author-avatar" src="<?=$comment['authorAvatar']?>" alt="<?=$comment['authorName']?>">
            <div class="comment-user-name">
                <a href="/users/<?=$comment['authorId']?>"><?=$comment['authorName']?></a>
            </div>
            <div class="comment-user-role">user</div>
        </div>
        <div class="comment-data">
            <div><?=$comment['createdAt']?></div>
            <div class="commentRating <?=$comment['rating'] >= 0? 'positive' : 'negative'?>" data-role="voteContainer">
                <?php if(!$comment['voted'] && isset($currentUser['id']) && User::checkPermission($currentUser, ['postReadSelf', 'postReadAnother'])):?> <a href="/comments/<?=$comment['id']?>/upvote" data-role="voteButton"><span class="fa fa-plus"></span></a> <?php endif; ?>
                <span class="ratingCount" data-role="voteCount"><?=$comment['rating']?></span>
                <?php if(!$comment['voted'] && isset($currentUser['id']) && User::checkPermission($currentUser, ['postReadSelf', 'postReadAnother'])):?> <a href="/comments/<?=$comment['id']?>/devote" data-role="voteButton"><span class="fa fa-minus"></span></a> <?php endif; ?>
            </div>

            </div>
        </div>
    <div class="comment-content">
        <?= ($comment['deletedAt']? '<p><span class="deleted-comment">This comment has been deleted</span></p>' : $comment['content'])?>
    </div>
    <div class="clearfix">
        <?php if(!$comment['deletedAt']): ?>

            <div class="buttonset">
                <a href="javascript:;" data-modal-id="commentForm" data-user="<?=$comment['authorName']?>" data-parent="<?=$comment['id']?>" class="button commentButton">Answer</a>
                <a href="/comments/<?=$comment['id']?>/delete" class="button ui-submit comment-delete" data-message="Are you want to delete the comment?">Delete</a>
            </div>

        <?php endif; ?>

        <?php if($comment['children']) : ?>

            <div class="answers-toggle-wrapper expand">
                <span class="answers-toggle fa fa-minus-square-o"></span>
                <span class="answers-info">Answers:<?=count($comment['children'])?></span>
            </div>

        <?php endif; ?>
    </div>

    <?php if($comment['children']) : ?>

    <ul class="answers">
        <?php self::printComments($comment['children'], $post, $currentUser) ?>
    </ul>

    <?php endif; ?>

</li>