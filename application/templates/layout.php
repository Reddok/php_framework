<?php
    require_once ROOT . '/application/views/header.php';
    require_once ROOT . '/application/views/' . $content_page;
    if(!empty($sidebar)) require_once ROOT . '/application/views/sidebar.php';

    require_once ROOT . '/application/views/footer.php';
