<?php require_once ROOT . '/application/views/header.php' ?>

    <div class="user-panel">
        <?php Framework\Menu::getMenu('user', [$currentUser]) ?>

        <?php require_once ROOT . '/application/views/' . $content_page ?>
    </div>


<?php require_once ROOT . '/application/views/footer.php' ?>