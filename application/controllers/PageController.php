<?php

use Framework\{Controller, Validator, Message};

/**
 * class ErrorController
 *
 *  methods:
 *
 *  * indexAction
 *  * getAction
 *  * editAction
 *  * createAction
 *  * deleteAction
 *
 */

class PageController extends Controller {

    /**
     * Constructor
     *
     *  initialises page model
     *
     */

    public function __construct() {

        parent::__construct();
        $this->model = new Page();

    }

    /**
     * Collects all pages. Render appropriate view
     *
     * @param array       $req      request array
     *
     */

    public function indexAction($req) {

        $pages = $this->model->getAll();

        $this->view->render('cabinet.php', 'page/index.php', [
            'pages' => $pages,
        ], null, 'Created pages');

    }


    /**
     * Take one page by path. Render appropriate view
     *
     * @param array       $req      request array
     *
     */

    public function getAction($req) {

        $path = $req['params']['path'];
        $page = $this->model->getByPath($path);

        $this->view->render('layout.php', 'page/page.php', [
            'page' => $page,
        ], null, $page['title']);

    }


    /**
     * edits the page if the data is validated.
     *
     * @param array       $req      request array
     *
     */

    public function editAction($req) {

        $id = $req['params']['id'];
        $errors = [];
        $page = $this->model->getById($id);
        $otherPages = $this->model->getAll($page['id']);

        if($_SERVER['REQUEST_METHOD'] === 'POST') {

            $data = $req['content']['page'];
            $validator = new Validator();

            $validator->addField('title', $data['title'])
                ->addConstraint('required')->addConstraint('minLength', [6]);

            $validator->addField('content', $data['content'])->addConstraint('required');

            if($data['parent']) $validator->addField('parent', $data['parent'])->addConstraint('isNumber');

            $errors = $validator->validate(true);

            if(!$errors) {

                $data['show'] = isset( $data['show'] )? 1 : 0 ;

                $data = array_merge($data, $validator->getValues());
                $this->model->update($id, $data);

                Message::registerMessage('Page successfully edited!');

                $this->redirect('/');

            }

        }

        $this->view->render('cabinet.php', 'page/edit.php', [
            'page' => $page,
            'errors' => $errors,
            'otherPages' => $otherPages
        ], null, $page['title']);

    }

    /**
     * extracts the page data from request, validates it and creates new page
     *
     * @param array       $req      request array
     *
     */

    public function createAction($req) {

        $errors = [];

        $pages = $this->model->getAll();

        if($_SERVER['REQUEST_METHOD'] === 'POST') {

            $data = $req['content']['page'];
            $validator = new Validator();

            $validator->addField('title', $data['title'])
                ->addConstraint('required')->addConstraint('minLength', [6]);

            $validator->addField('content', $data['content'])
                ->addConstraint('required');

            if($data['parent']) $validator->addField('parent', $data['parent'])
                ->addConstraint('isNumber');

            $errors = $validator->validate(true);

            if(!$errors) {

                if( empty($data['slug']) ) $data['slug'] = $data['title'];
                $data['slug'] = strtolower( str_replace(['.', ' '], '_', $data['slug']) );

                $data['show'] = isset( $data['show'] )? 1 : 0 ;

                $data = array_merge($data, $validator->getValues());
                $this->model->create($data);

                Message::registerMessage('Page successfully created!');

                $this->redirect('/');

            }

        }

        $this->view->render('cabinet.php', 'page/create.php', [
            'errors' => $errors,
            'pages' => $pages
        ],  null, 'Create new page');

    }


    /**
     * deletes page and register appropriate message.
     *
     * @param array       $req      request array
     *
     */

    public function deleteAction($req) {

        $id = $req['params']['id'];
        $this->model->delete($id);

        Message::registerMessage('Page has been deleted!');
        $this->redirect('pages');

    }

}