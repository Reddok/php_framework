<?php

use Framework\Controller;

/**
 * class TagController
 *
 *  methods:
 *
 *  * indexAction
 *  * createAjaxAction
 *  * getRecommendedTagsAction
 *
 */

class TagController extends Controller {

    /**
     * Constructor
     *
     *  initialises model
     *
     */

    public function __construct() {

        parent::__construct();
        $this->model = new Tag();

    }

    /**
     * Collect all tags. Generates sidebar. Render in view.
     *
     * @param array       $req      request array
     *
     */

    public function indexAction($req) {

        $tags = $this->model->getAll();

        $sidebar = SidebarHelper::generate([
            'actions' => [],
            'bestAuthors' => []
        ], $req['user']);

        $this->view->render('layout.php', 'tag/index.php', [
            'tags' => $tags
        ], $sidebar, 'Tags');

    }


    /**
     * Create new tag from request data and return it
     *
     * @param array       $req      request array
     *
     */

    public function createAjaxAction($req) {

        $tag = $req['content']['tag'];
        $tag['title'] = strtolower($tag['title']);
        $id = $this->model->create($tag);

        print json_encode($this->model->getById($id));

    }

    /**
     * get recommended tags for content
     *
     * @param array       $req      request array
     *
     */

    public function getRecommendedTagsAction($req) {

        $content = strip_tags( $req['content']['content'] );

        $autotag = new AutotagHelper(AUTOTAG_TOKEN);

        $tags = $autotag
            ->addContent($content)
            ->findTags(5)
            ->getTags();

        print json_encode($tags);

    }

}