<?php

/**
 * class ErrorController
 *
 *  methods:
 *
 *  * getAction
 *
 */

use Framework\Controller;

class ErrorController extends Controller{

    /**
     * extract data about current error and render it
     *
     * @param array       $req      request array
     *
     */

    function getAction($req) {

        $code = $req['params']['code'];
        $message = ErrorHelper::getError();
        $headers = ErrorHelper::getHeaders();

        ErrorHelper::clearError();

        if($headers) {
            foreach($headers as $header) {
                header($header);
            }
        }


        $this->view->render('layout.php', 'error/page.php', [
            'code' => $code,
            'message' => $message
        ], null, 'Error ' . $code);

    }

}