<?php



use Framework\{Controller, Message, Database, HttpException};

/**
 * class PostController
 *
 *  methods:
 *
 *  * indexAction
 *  * getAction
 *  * editAction
 *  * createAction
 *  * deleteAction
 *
 */


    class PostController extends Controller{

        use VoteTrait;

        /**
         * class PostController
         *
         *  methods:
         *
         *  * indexAction
         *  * getAction
         *  * editAction
         *  * createAction
         *  * deleteAction
         *
         */

        /**
         *
         * helper models
         *
         */

        private $mediaModel;
        private $categoryModel;
        private $tagModel;
        private $commentModel;
        private $userModel;
        private $commentRatingHelper;


        /**
         * Constructor
         *
         *  initialises models and rating helpers
         *
         */

        public function __construct() {
            parent::__construct();

            $this->model = new Post();
            $this->mediaModel = new Media();
            $this->categoryModel = new Category();
            $this->tagModel = new Tag();
            $this->commentModel = new Comment();
            $this->userModel = new User();

            $this->commentRatingHelper = new RatingHelper(Database::getInstance(), 'comment');
            $this->ratingHelper = new RatingHelper(Database::getInstance(),'post');
        }


        /**
         * Collects all posts. Apply filters to them if needed. Build sidebar. Render appropriate view
         *
         * @param array       $req      request array
         *
         */

        public function indexAction($req) {

            $limit = 5;
            $offset = isset($req['query']['page']) ? $limit * ($req['query']['page'] - 1) : 0;
            $search = $req['query']['search'] ?? null;
            $filter = null;
            $params = [];

            if(isset($req['query']['filterType']) && isset($req['query']['filterValue'])) {

                $modelName = $req['query']['filterType'] === 'author' ? 'User' : ucfirst($req['query']['filterType']);
                $value = $req['query']['filterValue'];

                $name = (new $modelName())->getName($value);

                $filter = $modelName . ': ' . $name;
                $params[$req['query']['filterType']] = $req['query']['filterValue'];
            }

            $posts = $this->model->getAll($params,  $limit, $offset, 'createdAt', 'DESC', $search);

            foreach($posts as &$post) {
                $post['preview'] = $post['preview'] ?? $this->model->makePreview($post['content']);
                $post['categories'] = $this->categoryModel->getByPost($post['id']);
                $post['tags'] = $this->tagModel->getByPost($post['id']);
                $post['createdAt'] = (new DateTime($post['createdAt']))->format('d F Y');
                $post['authorAvatar'] = $post['authorAvatar'] ?? DEFAULT_AVATAR;
            }

            $sidebar = SidebarHelper::generate([
                'search' => [],
                'actions' => [],
                'popularCategories' => [],
                'popularTags' => [],
                'bestAuthors' => []
            ], $req['user']);

            if(AjaxHelper::isAjax()) {
                $this->view->render(null, 'post/indexAjax.php', [
                    'posts' => $posts,
                ]);
            } else {
                $this->view->render('layout.php', 'post/index.php', [
                    'posts' => $posts,
                    'filter' => $filter
                ], $sidebar);
            }

        }


        /**
         * Takes one post by id and collects all comments by this post. Collects all tags by this post. Collects some other posts.
         * Builds tree like structure for comments and renders all of this shit.
         *
         * @param array       $req      request array
         *
         * @throws HttpException   throw exception if posts with this id deleted or not exists.
         *
         */

        public function getAction($req) {

            $id = $req['params']['id'];
            $post = $this->model->getById($id);
            $user = $req['user'];

            if(!$post) throw new HttpException(404);

            $post['tags'] = $this->tagModel->getByPost($post['id']);
            $comments = $this->commentModel->getAll($post['id'], $user['id'] ?? null);

            foreach($comments as &$comment) {
                $comment['voted'] = (bool) $comment['voted'];
                $comment['authorAvatar'] = $comment['authorAvatar'] ?? DEFAULT_AVATAR;
            }

            $comments = TreeHelper::structurize($comments);
            $otherPosts = $this->model->getOtherPosts($post);

            $post['authorAvatar'] = $post['authorAvatar'] ?? DEFAULT_AVATAR;
            $sidebar = SidebarHelper::generate([
                'postAuthor' => [$post],
                'actions' => [$post],
            ], $req['user']);

            $voted = $this->ratingHelper->isVoted($id, $user['id']);

            $this->view->render('layout.php', 'post/page.php', [
                'post' => $post,
                'otherPosts' => $otherPosts,
                'comments' => $comments,
                'voted' => $voted
            ], $sidebar, $post['title']);

        }

        /**
         *
         * Extract changed the data from the query. Checks it.
         * If this is correct, the publication is updated and takes care of changes to categories and tags,
         * linking new objects and removing old links.
         *
         * @param array       $req      request array
         *
         * @throws HttpException   throw exception if posts with this id deleted or not exists.
         *
         */

        public function editAction($req) {

            $id = $req['params']['id'];
            $post = $this->model->getById($id);
            $errors = null;
            $editedPost = null;
            $categoriesIds = null;
            $tagsIds = null;
            $newCats = null;
            $oldCats = null;
            $newTags = null;
            $oldTags = null;

            if(!$post) throw new HttpException(404);

            $post['categories'] = $this->categoryModel->getByPost($post['id']);
            $post['tags'] = $this->tagModel->getByPost($post['id']);

            $categories = $this->categoryModel->getAll();
            $tags = $this->tagModel->getAll();


            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $editedPost = $req['content']['post'];

                $editedPost['title'] = trim($editedPost['title']);
                $editedPost['content'] = trim($editedPost['content']);

                $this->validator->addField('title', $editedPost['title'])
                    ->addConstraint('required')
                    ->addConstraint('min', [6]);

                $this->validator->addField('content', $editedPost['content'])
                    ->addConstraint('required');

                $this->validator->addField('thumbnail', $editedPost['thumbnail'])
                    ->addConstraint('required');

                $this->validator->addField('categories', $editedPost['categories'])
                    ->addConstraint('required');

                $this->validator->addField('tags', $editedPost['tags'])
                    ->addConstraint('required');

                if( !($errors = $this->validator->validate(true)) ) {

                    $categoriesIds = array_column( $post['categories'], 'id');
                    $tagsIds = array_column( $post['tags'], 'id');

                    $newCats = array_values( array_diff($editedPost['categories'], $categoriesIds) );
                    $oldCats = array_values( array_diff($categoriesIds, $editedPost['categories']) );
                    $newTags = array_values( array_diff($editedPost['tags'], $tagsIds) );
                    $oldTags = array_values( array_diff($tagsIds, $editedPost['tags']) );

                    $this->categoryModel->boundToPost($id, $newCats);
                    $this->tagModel->boundToPost($id, $newTags);

                    $this->categoryModel->unboundFromPost($id, $oldCats);
                    $this->tagModel->unboundFromPost($id, $oldTags);

                    $editedPost['preview'] = empty($editedPost['preview'])?  null : trim($editedPost['preview']);
                    $this->model->update($id, $editedPost);
                    Message::registerMessage('Post successfully edited!');
                    $this->redirect('/posts');
                }

            }

            $this->view->render('cabinet.php', 'post/edit.php', [
                'post' => $post,
                'categories' => $categories,
                'tags' => $tags,
                'errors' => $errors
            ], null, 'Edit: ' . $post['title']);

        }


        /**
         * Extracts...Validates...Links...Creates...And again...
         *
         * @param array       $req      request array
         *
         */

        public function createAction($req) {

            $categories = $this->categoryModel->getAll();
            $tags = $this->tagModel->getAll();
            $post = null;
            $id = null;

            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $post = $req['content']['post'];

                $post['title'] = trim($post['title']);
                $post['content'] = trim($post['content']);
                $post['preview'] = empty($post['preview'])?  null : trim($post['preview']);
                $post['author'] = $req['user']['id'];

                $this->validator->addField('title', $post['title'])
                    ->addConstraint('required')
                    ->addConstraint('min', [6]);

                $this->validator->addField('content', $post['content'])
                    ->addConstraint('required');

                $this->validator->addField('thumbnail', $post['thumbnail'])
                    ->addConstraint('required');

                $this->validator->addField('categories', $post['categories'])
                    ->addConstraint('required');

                $this->validator->addField('tags', $post['tags'])
                    ->addConstraint('required');

                if( !($errors = $this->validator->validate(true)) ) {

                    $id = $this->model->create($post);

                    $this->categoryModel->boundToPost($id, $post['categories']);
                    $this->tagModel->boundToPost($id, $post['tags']);

                    Message::registerMessage('Post successfully added!');
                    $this->redirect('/posts');
                }

            }

            $this->view->render('cabinet.php', 'post/create.php', [
                'categories' => $categories,
                'tags' => $tags,
                'errors' => $errors
            ], null, 'Create post');

        }


        /**
         * deletes post and register appropriate message.
         *
         * @param array       $req      request array
         *
         */

        public function deleteAction($req) {

            $id = $req['params']['id'];

            $this->model->delete($id);
            Message::registerMessage('Post has been deleted!');
            $this->redirect('posts');

        }


    }