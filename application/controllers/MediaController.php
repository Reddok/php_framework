<?php

use Framework\{Controller, Message};

/**
 * class MediaController
 *
 *  methods:
 *
 *  * getUploadedJsonAction
 *  * createJsonAction
 *  * deleteAction
 *
 */


class MediaController extends Controller {

    /**
     * Constructor.
     *
     * initializes model
     */

    public function __construct() {

        parent::__construct();
        $this->model = new Media();

    }


    /**
     * Collect all files by the current user and render them
     *
     * @param array       $req      request array
     *
     */


    public function indexAction($req) {

        $user = $req['user']['id'];
        $files = $this->model->getByUser($user);

        $this->view->render('cabinet.php', 'media/index.php', compact('files'), false, 'Uploads');

    }


    /**
     * Collect all files by the current user and send them as json data
     *
     * @param array       $req      request array
     *
     */

    public function getUploadedJsonAction($req) {

        $user = $req['user']['id'];
        $files = $this->model->getByUser($user);
        print json_encode($files);

    }


    /**
     * extracts the file data from request, creates new file and send new data as json data
     *
     * @param array       $req      request array
     *
     */

    public function createJsonAction($req) {

        $file = $req['content']['upload'];
        $file['uploadedBy'] = $req['user']['id'];

        $id = $this->model->create($file);
        print json_encode($this->model->getById($id));

    }


    /**
     * deletes file and register appropriate message.
     *
     * @param array       $req      request array
     *
     */

    public function deleteAction($req) {

        $fileId = $req['params']['id'];
        $this->model->delete($fileId);

        Message::registerMessage('File successfully deleted');
        $this->redirect('/files');

    }

}