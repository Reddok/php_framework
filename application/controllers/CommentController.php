<?php

use Framework\{Controller, Message, Database};

/**
 * class CommentController
 *
 *  methods:
 *
 *  * createAction
 *  * editAction
 *  * deleteAction
 *
 */


    class CommentController extends Controller {

        use VoteTrait;

        /**
         * Constructor
         *
         * sets the model and initialies rating helper
         *
         */

        public function __construct() {

            parent::__construct();
            $this->model = new Comment();
            $this->ratingHelper = new RatingHelper(Database::getInstance(), 'comment');

        }


        /**
         * extracts the comment data from request, validate it and creates new comment
         *
         * @param array       $req      request array
         *
         */


        public function createAction($req) {

                $comment = $req['content']['comment'];

                $comment['commentator'] = $req['user']['id'];
                $comment['content'] = trim($comment['content']);
                $comment['parent'] = empty($comment['parent'])? null : trim($comment['parent']);

                $this->validator->addField('content', $comment['content'])
                    ->addConstraint('required');

                if(($errors =$this->validator->validate(true)) ) {
                    Message::registerMessage($errors[0], 'error');
                } else {
                    $this->model->create($comment);
                    Message::registerMessage('Comment successfully added!');
                }

                static::returnBack();

        }


        /**
         * deletes comment and register appropriate message.
         *
         * @param array       $req      request array
         *
         */

        public function deleteAction($req) {

            $id = $req['params']['id'];

            $this->model->delete($id);

            Message::registerMessage('Comment successfully deleted!');

            static::returnBack();

        }


        /**
         * edits a comment if the data is validated.
         *
         * @param array       $req      request array
         *
         */

        public function editAction($req) {

            $id = $req['params']['id'];
            $post = $req['query']['post'];
            $comment = $this->model->getById($id);
            $errors = null;


            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $editedComment = $req['content']['comment'];
                $editedComment['content'] = trim($editedComment['content']);

                if(!($errors =$this->validator->validate(true)) ) {
                    $this->model->update($id, $editedComment);
                    Message::registerMessage('Comment successfully updated!');
                    $this->redirect('posts/' . $post);
                }

            }

            $this->view->render('layout.php', 'comment/edit.php', [
                'comment' => $comment,
                'errors' => $errors
            ]);

        }


    }