<?php


    use Framework\Controller;

/**
 * class StatisticController
 *
 *  methods:
 *
 *  * indexAction
 *
 */

    class StatisticController extends Controller {

        /**
         *
         * helper models
         *
         */

        private $categoryModel;
        private $tagModel;
        private $commentModel;
        private $userModel;
        private $postModel;

        /**
         * Constructor
         *
         *  initialises models
         *
         */

        public function __construct() {

            parent::__construct();

            $this->categoryModel = new Category();
            $this->tagModel = new Tag();
            $this->commentModel = new Comment();
            $this->userModel = new User();
            $this->postModel = new Post();

        }

        /**
         * Count general statistic. Collects some sort of information like best authors, best post, hottest ladies and other trash.
         *
         * @param array       $req      request array
         *
         */

        public function indexAction($req) {

            $counters = [
                'posts' => $this->postModel->countRows(),
                'users' => $this->userModel->countRows(),
                'categories' => $this->categoryModel->countRows(),
                'tags' => $this->tagModel->countRows()
            ];

            $lastRegisteredUser = $this->userModel->getLastRegisteredUser();

            $bestAuthors = $this->userModel->getAll([], 10);
            $bestPosts = $this->postModel->getAll([], 10, 0, 'rating', 'DESC');
            $activeAuthors = $this->userModel->getMostActive(10);
            $hottestPosts = $this->postModel->getHottest(10);

            $this->view->render(
                'layout.php',
                'statistic/index.php',
                compact('counters', 'lastRegisteredUser', 'bestAuthors', 'bestPosts', 'activeAuthors', 'hottestPosts'),
                null,
                'Statistic'
            );

        }

    }