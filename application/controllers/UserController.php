<?php


    use Framework\{Controller, Message, Oauth, Validator, Mail, HttpException};

/**
 * class UserController
 *
 *  methods:
 *
 *  * indexAction
 *  * getAction
 *  * deleteAction
 *  * editAction
 *  * loginAction
 *  * createAction
 *  * logoutAction
 *  * resetAction
 *  * changePasswordAction
 *
 */


    class UserController extends Controller
    {

        /**
         * Model, which operates with files
         *
         * @var object
         */

        public $mediaModel;


        /**
         * Constructor
         *
         * sets the models
         *
         */

        public function __construct() {
            parent::__construct();
            $this->model = new User();
            $this->mediaModel = new Media();
        }


        /**
         * Collects all users. Render appropriate view
         *
         * @param array       $req      request array
         *
         */

        public function indexAction($req) {
            $users = $this->model->getAll();

            $this->view->render('cabinet.php', 'user/index.php', ['users' => $users], null, 'Users');
        }


        /**
         * Collects user by id. Render appropriate view
         *
         * @param array       $req      request array
         *
         * @throws HttpException        throws error if user doesn't exist
         */

        public function getAction($req) {

            $id = $req['params']['id'];
            $user = $this->model->getById($id);

            if(!$user) throw new HttpException('404');

            $this->view->render(
                'layout.php',
                'user/page.php',
                ['user' => $user],
                null,
                $user['username']
            );

        }


        /**
         * Delete user by id and logout current user if needed.
         *
         * @param array       $req      request array
         *
         */

        public function deleteAction($req) {

            $id = $req['params']['id'];

            $this->model->delete($id);

            Message::registerMessage('User has been banned!');
            if(Oauth::getCurrentUser()['id'] === $id) $this->logoutAction();
            $this->redirect('/users');

        }



        /**
         * Edit user by id
         *
         * @param array       $req      request array
         *
         *
         * @throws HttpException        throws error if user doesn't exist
         */


        public function editAction($req) {

            $id = $req['params']['id'];
            $user = $this->model->getById($id);
            $roles = $this->model->getRoles();

            if(!$user) throw new HttpException('404');

            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $user = $req['content']['user'];

                $this->model->update($id, $user);

                Message::registerMessage('Profile successfully edited!');
                $this->redirect('users/' . $id);

            }

            $this->view->render(
                'cabinet.php',
                'user/edit.php',
                compact('user', 'roles'),
                null,
                'Edit: ' . $user['username']
            );

        }

        /**
         * Authenticates user if he provides the correct data
         *
         * @param array       $req      request array
         *
         */

        public function loginAction($req) {

            if($req['user']['id']) $this->redirect('/');

            $data = [];
            $errors = [];

            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $data = $req['content']['user'];

                $validator = new Validator();

                $validator->addField('email', $data['email'])
                    ->addConstraint('isEmail');

                $user = $this->model->getByEmail($data['email']);

                if (!$user) $errors[] = 'User with this email doesn\'t exists';

                $validator->addField('password', $data['password'])
                    ->addConstraint('hashEqual', [$user['password'], 'registered password']);

                $validator->validate();
                $errors = array_merge( $errors, $validator->getErrors());

                if (!$errors) {
                    Oauth::authenticate($user['id']);
                    $this->redirect('');
                }

            }

            $this->view->render('layout.php', 'user/login.php', [
                'data' => $data,
                'errors' => $errors
            ], null, 'Login');

        }

        /**
         * Register and authenticates new user
         *
         * @param array       $req      request array
         *
         */


        public function createAction($req) {

            if($req['user']['id']) $this->redirect('/');

            $user = [];
            $errors = [];

            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $user = $req['content']['user'];
                $validator = new Validator();

                $validator->addField('email', $user['email'])
                    ->addConstraint('required')
                    ->addConstraint('isEmail')
                    ->addConstraint('isUnique', ['email', $this->model->connect, 'user']);

                $validator->addField('username', $user['username'])
                    ->addConstraint('required')
                    ->addConstraint('between', [4,16]);

                $validator->addField('password', $user['password'])
                    ->addConstraint('equal', [$user['repeat'], 'confirmation'])
                    ->addConstraint('between', [6,16]);

                $validator->validate();
                $errors = $validator->getErrors();

                if(!$errors) {

                    $user['password'] = Oauth::generatePasswordHash($user['password']);

                    $id = $this->model->create($user);

                    if( !empty($req['content']['avatar']) ) {
                        $file = $req['content']['avatar'];
                        $file['uploadedBy'] = $id;
                        $user['avatar'] = $this->mediaModel->create($file);
                    }

                    $this->model->update($id, $user);

                    Oauth::authenticate($id);
                    $this->redirect('posts');

                }

            }

            $this->view->render('layout.php', 'user/create.php', [
                'data' => $user,
                'errors' => $errors
            ], null, 'Registration');


        }


        /**
         * Perform logout for user
         *
         */

        public function logoutAction() {
            Oauth::clear();
            self::redirect('login');
        }


        /**
         * Reset password for user. Provides form for email. Check email. If correct, create message with reset link and sends
         * to this email
         *
         * @param array       $req      request array
         */

        public function resetAction($req) {

            $errors = [];
            $email = [];

            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $email = $req['content']['forgot'];
                $validator = new Validator();

                $validator->addField('email', $email)
                    ->addConstraint('required')
                    ->addConstraint('isEmail')
                    ->addConstraint('isExists', ['email', $this->model->connect, 'user']);

                $errors = $validator->validate(true);

                if(!$errors) {

                    $user = $this->model->getByEmail($email);
                    $secret = random_bytes(16);
                    $hash = Oauth::generatePasswordHash($secret);

                    $url = 'http://framework/changePassword?case=' . $hash;

                    $mailbody = "Dear user,\n\nIf this e-mail does not apply to you please ignore it. It appears that you have requested a password reset at our website www.yoursitehere.com\n\nTo reset your password, please click the link below. If you cannot click it, please paste it into your web browser's address bar.\n\n" . $url . "\n\nThanks,\nThe Administration";

                    Mail::getInstance()->sendMail($email, 'Admin', "framework - Password Reset", $mailbody);

                    $req['session']['secret'] = $secret;
                    $req['session']['expire'] = time() + 10 * 60;
                    $req['session']['user_id'] = $user['id'];

                    $this->view->render('layout.php', 'user/resetConfirmed.php', [], null, 'Reset');
                }

            }

            $this->view->render('layout.php', 'user/reset.php', [
                'email' => $email,
                'errors' => $errors
            ], null, 'Reset');

        }

        /**
         * If link is correct, provides form to change password and change it.
         *
         * @param array       $req      request array
         *
         * @throws HttpException    throws exception if link doesn't valid
         */


        public function changePasswordAction($req) {

            $hash = $req['query']['case'] ?? null;
            $secret = $req['session']['secret'] ?? null;
            $expires = $req['session']['expire'] ?? null;
            $errors = [];

            $validator = new Validator();

            $validator->addField('case', $hash)->addConstraint('required');
            $validator->addField('expires', $expires)
                ->addConstraint('required')->addConstraint('beforeTime');
            $validator->addField('secret', $secret)
                ->addConstraint('required')->addConstraint('hashEqual', [$hash, 'reset key']);

            if( $errors = $validator->validate(true) ) {
                unset($_SESSION);
                throw new HttpException(303);
            };


            if($_SERVER['REQUEST_METHOD'] === 'POST') {

                $validator->clear();

                $validator->addField('password', $req['content']['password'])
                    ->addConstraint('between', [6, 16])
                    ->addConstraint('equal', [$req['content']['confirm'], 'confirmation']);

                $errors = $validator->validate(true);

                if(!$errors) {

                    $password = $validator->getValues()['password'];
                    $password = OAuth::generatePasswordHash($password);
                    $id = $req['session']['user_id'];

                    $this->model->changePassword($id, $password);

                    unset($_SESSION);
                    Message::registerMessage('Password successfully changed. Please login.!');

                    self::redirect('login');

                }

            }

            $this->view->render('layout.php', 'user/changePassword.php', [
                'errors' => $errors,
                'case' => $hash
            ], null, 'Change password');

        }

    }