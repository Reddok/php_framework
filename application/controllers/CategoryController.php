<?php

    use Framework\Controller;

    /**
     * class CategoryController
     *
     *  methods:
     *
     *  * indexAction
     *  * createAjaxAction
     *
     */

    class CategoryController extends Controller {

        /**
         * Constructor
         *
         * sets the model
         *
         */

        public function __construct() {

            parent::__construct();
            $this->model = new Category();

        }

        /**
         * Collects all categories. Create sidebar. Render appropriate view
         *
         * @param array       $req      request array
         *
         */

        public function indexAction($req) {

            $categories = $this->model->getAll();

            $sidebar = SidebarHelper::generate([
                'actions' => [],
                'bestAuthors' => []
            ], $req['user']);

            $this->view->render('layout.php', 'category/index.php', [
                'categories' => $categories
            ], $sidebar, 'Categories');

        }

        /**
         * extracts the category data from request, creates new category and send created data as answer
         *
         * @param array       $req      request array
         *
         */


        public function createAjaxAction($req) {

            $category = $req['content']['category'];
            $id = $this->model->create($category);

            print json_encode($this->model->getById($id));

        }

    }