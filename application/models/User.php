<?php
use Framework\Model;

/**
 * class User represents user in blog
 *
 *  methods:
 *
 *  * getAll
 *  * getById
 *  * getByEmail
 *  * getMostActive
 *  * getLastRegisteredUser
 *  * getName
 *  * getRoles
 *  * create
 *  * update
 *  * changePassword
 *  * loadRole
 *  * setAttending
 *  * baseRequest
 *  * checkPermission
 *  * getAvatar
 *  * getRoleName
 *
 */

class User extends Model {

    /**
     * name of the table
     *
     * @var string
     */

    protected $tableName = 'user';

    /**
     *
     * So many tables. They all represent entities from blog
     *
     * @var string
     */

    protected $avatarTable = 'media';
    protected $postTable = 'post';
    protected $commentTable = 'comment';

    protected $roleTable = 'role';
    protected $permissionTable = 'permission';
    protected $rolePermissionTable = 'role_permission';
    protected $postRatingTable = 'post_rating';
    protected $commentRatingTable = 'comment_rating';


    /**
     *
     * User rating is sum of post rating and comment rating that belongs to this user. They have different coefficients
     *
     * @const int
     */

    const COMMENT_COEFFICIENT = 0.4;
    const POST_COEFFICIENT = 1;


    /**
     * returns users by filter. Can sort users and, use for pagination
     *
     * @param array         $filter     array with filters
     * @param int           $limit      indicates how many posts should be returned
     * @param int           $offset     indicates how many posts should be skipped
     * @param string        $order      determines column for sorting
     * @param string        $direction  determines order for sorting
     *
     * @return array
     */

    public function getAll($filter=[], $limit = null, $offset = null,  $order='rating', $direction = 'desc')
    {

        $q = sprintf(
            $this->baseRequest()
            . ' FROM %1$s LEFT JOIN %2$s'
            . ' ON %1$s.avatar = %2$s.id'
            . ' WHERE %1$s.deletedAt is NULL'
            . ' ORDER BY %7$s %8$s',
            $this->tableName, $this->avatarTable, $this->postTable, $this->commentRatingTable, $this->postRatingTable, $this->commentTable, $order, $direction, static::COMMENT_COEFFICIENT, static::POST_COEFFICIENT);

        if($limit) $q .= ' LIMIT ' . $limit;

        return $this->connect->execute($q);

    }

    /**
     * returns user by id
     *
     * @param int           $id      The id of the user
     *
     * @return array
     */

    public function getById($id)
    {

        $q = sprintf(
            $this->baseRequest() . ','
            . ' %1$s.avatar as avatarId,'
            . ' %1$s.bio as bio,'
            . ' %1$s.createdAt as createdAt,'
            . ' %1$s.deletedAt as deletedAt,'
            . ' %1$s.lastAttend as lastAttend,'
            . ' COUNT(%3$s.id) as posts'
            . ' FROM %1$s LEFT JOIN %2$s'
            . ' ON %1$s.avatar = %2$s.id'
            . ' LEFT JOIN %3$s'
            . ' ON %1$s.id = %3$s.author'
            . ' WHERE %1$s.id=? AND %3$s.deletedAt is NULL;',
            $this->tableName, $this->avatarTable, $this->postTable);

        $user = $this->connect->execute($q, [$id], false);
        $user['role'] = $this->loadRole($user['role']);

        return $user;

    }


    /**
     * returns user by email
     *
     * @param string           $email      The email of the user
     *
     * @return array
     */

    public function getByEmail($email)
    {

        $q = sprintf(
            $this->baseRequest() . ','
            . ' %1$s.avatar as avatarId,'
            . ' %1$s.password as password,'
            . ' %1$s.bio as bio,'
            . ' %1$s.createdAt as createdAt,'
            . ' %1$s.deletedAt as deletedAt,'
            . ' COUNT(%3$s.id) as posts'
            . ' FROM %1$s LEFT JOIN %2$s'
            . ' ON %1$s.avatar = %2$s.id'
            . ' LEFT JOIN %3$s'
            . ' ON %1$s.id = %3$s.author'
            . ' WHERE %1$s.email=?;',
            $this->tableName, $this->avatarTable, $this->postTable);

        return $this->connect->execute($q, [$email], false);

    }

    /**
     * returns most active users in blog
     *
     * @param int         $count    indicates how many users should be returned
     *
     * @return array
     */


    public function getMostActive($count=5)
    {

        $q = sprintf(
            $this->baseRequest() . ','
            . ' %1$s.avatar as avatarId,'
            . ' COUNT(%3$s.id) as postsCount'
            . ' FROM %1$s INNER JOIN %3$s'
            . ' ON %1$s.id = %3$s.author'
            . ' INNER JOIN %2$s'
            . ' ON %1$s.avatar = %2$s.id'
            . ' WHERE %3$s.deletedAt is NULL'
            . ' GROUP BY %1$s.id'
            . ' ORDER BY postsCount DESC'
            . ' LIMIT %4$d;',
            $this->tableName, $this->avatarTable, $this->postTable, $count
        );

        return $this->connect->execute($q);
    }


    /**
     * returns last registered user
     *
     * @return array
     */

    public function getLastRegisteredUser()
    {

        $q = 'SELECT username FROM ' . $this->tableName . ' ORDER BY createdAt DESC LIMIT 1';

        return $this->connect->execute($q, [], false);

    }

    /**
     * returns name of particular user
     *
     * @param int           $id      The id of the user
     *
     * @return string
     */


    public function getName($id)
    {

        $q = 'SELECT username from ' . $this->tableName . ' WHERE id = ?;';

        $entry = $this->connect->execute($q, [$id], false);
        return $entry['username'];

    }

    /**
     * returns all user roles
     *
     *
     * @return array
     */

    public function getRoles()
    {
        $q = 'SELECT * FROM ' . $this->roleTable . ';';

        return $this->connect->execute($q);
    }


    /**
     *  creates new user
     *
     * @param array           $data The data for user
     *
     * @return int
     */

    public function create($data)
    {

        $q = 'INSERT INTO ' . $this->tableName . '(username, password, email, firstName, lastName, avatar, bio)'
            . ' VALUES(?, ?, ?, ?, ?, ?, ?);';

        return $this->connect->execute($q, [
            $data['username'],
            $data['password'],
            $data['email'],
            $data['firstName'],
            $data['lastName'],
            $data['avatar'],
            $data['bio']
        ]);

    }

    /**
     *  updates the user
     *
     * @param int           $id   The user's id
     * @param array         $data The data for user
     *
     * @return bool
     */

    public function update($id, $data)
    {

        $q = 'UPDATE ' . $this->tableName . ' SET'
            . ' firstName=?,'
            . ' lastName=?,'
            . ' username=?,'
            . ' email=?,'
            . ' avatar=?,'
            . ' bio=?';

        $params = [$data['firstName'], $data['lastName'], $data['username'], $data['email'], $data['avatar'], $data['bio']];

        if( isset($data['role']) ) {
            $q .= ', role=?';
            $params[] = $data['role'];
        }

        $q .= ' WHERE id=?;';
        $params[] = $id;

        return $this->connect->execute($q, $params);

    }

    /**
     *  change password for user
     *
     * @param int           $id   The user's id
     * @param string        $password The password for user
     *
     * @return bool
     */


    public function changePassword($id, $password) {

        $q = 'UPDATE ' . $this->tableName . ' SET password = ? WHERE id = ?';

        return $this->connect->execute($q, [$password, $id]);

    }


    /**
     *  return role with associated permissions
     *
     * @param int           $roleId   The role's id
     *
     * @return array
     */

    public function loadRole($roleId) {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.title as title,'
            . ' GROUP_CONCAT(%2$s.title) as permissions'
            . ' FROM %1$s LEFT JOIN %3$s'
            . ' ON %1$s.id = %3$s.role'
            . ' INNER JOIN %2$s'
            . ' ON %3$s.permission = %2$s.id'
            . ' WHERE %1$s.id = ?;',
           $this->roleTable, $this->permissionTable, $this->rolePermissionTable);

        $result = $this->connect->execute($q, [$roleId], false);
        $result['permissions'] = explode( ',', $result['permissions'] );

        return $result;
    }


    /**
     *  determines the user on the site at this time
     *
     * @param int           $userId   The user's id
     *
     * @return bool
     */

    public function setAttending($userId) {

        $q = 'UPDATE user SET lastAttend = NOW() WHERE id = ?;';

        return $this->connect->execute($q, [$userId]);

    }


    /**
     * Returns base request
     *
     * @return string
     *
     */

    private function baseRequest() {

        return sprintf('SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.username as username,'
            . ' %1$s.email as email,'
            . ' %1$s.role as role,'
            . ' %2$s.path as avatar,'
            . ' %1$s.firstName as firstName,'
            . ' %1$s.lastName as lastName,'
            . ' ROUND(coalesce((SELECT sum(%4$s.value) from %4$s inner join %6$s on %4$s.comment = %6$s.id where %6$s.commentator = %1$s.id), 0) * %7$s + coalesce((SELECT sum(%5$s.value) from %5$s inner join %3$s on %5$s.post = %3$s.id where %3$s.author = %1$s.id), 0) * %8$s) as rating',
            $this->tableName, $this->avatarTable, $this->postTable, $this->commentRatingTable, $this->postRatingTable, $this->commentTable, static::COMMENT_COEFFICIENT, static::POST_COEFFICIENT
        );

    }

    /**
     * checks if user has permission to do something
     *
     * @param array         $user           user's data
     * @param mixed         $permissions    permissions that a user must have
     *
     * @return bool
     *
     */

    static public function checkPermission($user, $permissions) {

        $permissions = is_array($permissions)? $permissions : [$permissions];

        foreach($permissions as $permission) {
            if( !in_array( $permission, $user['role']['permissions'] ) ) return false;
        }

        return true;
    }

    /**
     * return user's avatar or default
     *
     * @param array         $user           user's data
     *
     * @return bool
     *
     */

    static public function getAvatar($user) {
        return $user['avatar'] ?? DEFAULT_AVATAR;
    }


    /**
     * return user's avatar or default
     *
     * @param array         $roleId          The role's id
     *
     * @return mixed
     *
     */

    static public function getRoleName($roleId) {

        $names = [
            1 => 'User',
            2 => 'Author',
            3 => 'Admin'
        ];

        return $names[$roleId] ?? null;
    }
}