<?php

/**
 * class Media represents media files
 *
 *  methods:
 *
 *  * getAll
 *  * getById
 *  * getByUser
 *  * create
 *  * update
 *
 */

use Framework\Model;

class Media extends Model
{
    /**
     * name of the table
     *
     * @var string
     */

    protected $tableName = 'media';

    /**
     * returns all media files
     *
     * @return array
     */

    public function getAll()
    {
        return $this->connect->execute('SELECT * FROM ' . $this->tableName . ' WHERE deletedAt is NULL;');
    }

    /**
     * returns file by id
     *
     * @param int           $id      id of the file
     *
     * @return array
     */

    public function getById($id)
    {

        $q = 'SELECT * FROM ' . $this->tableName . ' WHERE id=? and deletedAt is NULL;;';
        return $this->connect->execute($q, [$id], false);

    }

    /**
     * returns files of particular user
     *
     * @param int           $id      id of the user
     *
     * @return array
     */

    public function getByUser($id)
    {

        $q = 'SELECT * FROM ' . $this->tableName . ' WHERE uploadedBy=? AND deletedAt is NULL;';
        return $this->connect->execute($q, [$id]);

    }


    /**
     *  creates new media file
     *
     * @param array           $file The data for file
     *
     * @return int
     */


    public function create($file)
    {

        $path = FileHelper::upload($file);
        $type = 2;
        $q = 'INSERT INTO ' . $this->tableName . '(path, title, uploadedBy, type) VALUES(?, ?, ?, ?);';

        return $this->connect->execute($q, [$path, $file['name'], $file['uploadedBy'], $type]);


    }

    /**
     *  updates the file
     *
     * @param int           $id   The file's id
     * @param array         $data The data for file
     *
     * @return bool
     */

    public function update($id, $data)
    {

        $q = 'UPDATE ' . $this->tableName . ' SET title=?;';
        return $this->connect->execute($q, [$data['title']]);

    }


}

