<?php

/**
 * class Category represents category in blog
 *
 *  methods:
 *
 *  * getAll
 *  * getById
 *  * getByPost
 *  * getName
 *  * create
 *  * update
 *  * delete
 *  * boundToPost
 *  * unboundToPost
 *
 */

use Framework\Model;

class Category extends Model {

    use BoundTrait;

    /**
     * name of the table
     *
     * @var string
     */

    protected $tableName = 'category';

    /**
     * name of the table, where there are links between posts and categories
     *
     * @var string
     */

    protected $boundTable = 'post_category';

    /**
     * name of the table, where there are posts
     *
     * @var string
     */

    protected $postTable = 'post';

    /**
     * returns all categories
     *
     * @param int           $count      indicates how many items should be returned
     *
     * @return array
     */

    public function getAll($count=0)
    {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.title as title,'
            . ' COUNT(%2$s.post) as postsCount'
            . ' FROM %1$s LEFT JOIN %2$s'
            . ' ON %1$s.id = %2$s.category'
            . ' LEFT JOIN %3$s'
            . ' ON %2$s.post = %3$s.id'
            . ' WHERE %3$s.deletedAt is NULL'
            . ' GROUP BY %1$s.id'
            . ' ORDER BY postsCount DESC',
            $this->tableName, $this->boundTable, $this->postTable
        );

        if($count) $q .= ' LIMIT ' . $count;
        $q .= ';';

        return $this->connect->execute($q);

    }

    /**
     * returns category by id
     *
     * @param int           $id      id of the category
     *
     * @return array
     */

    public function getById($id)
    {
        $q = 'SELECT * FROM ' . $this->tableName . ' WHERE id=?;';
        return $this->connect->execute($q, [$id], false);
    }

    /**
     * returns categories by post
     *
     * @param int           $postId The post id
     *
     * @return array
     */


    public function getByPost($postId)
    {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.title as title'
            . ' FROM %1$s INNER JOIN %2$s'
            . ' ON %1$s.id = %2$s.category'
            . ' WHERE %2$s.post = ?',
            $this->tableName, $this->boundTable
        );

        return $this->connect->execute($q, [$postId]);

    }

    /**
     * returns name of the category by id
     *
     * @param int           $id The category's id
     *
     * @return string
     */

    public function getName($id)
    {

        $q = 'SELECT title from ' . $this->tableName . ' WHERE id = ?;';

        $entry = $this->connect->execute($q, [$id], false);
        return $entry['title'];

    }

    /**
     *  creates new category
     *
     * @param array           $data The data for category
     *
     * @return int
     */

    public function create($data)
    {

        $q = 'INSERT INTO ' . $this->tableName . '(title) VALUES(?);';
        return $this->connect->execute($q, [$data['title']]);

    }

    /**
     *  updates the category
     *
     * @param int           $id   The category's id
     * @param array           $data The data for category
     *
     * @return bool
     */

    public function update($id, $data)
    {

        $q = 'UPDATE ' . $this->tableName . ' SET title=?;';
        return $this->connect->execute($q, [$data['title']]);

    }

    /**
     *  deletes the category
     *
     * @param int           $id   The category's id
     *
     * @return bool
     */

    public function delete($id)
    {

        $q = 'DELETE FROM ' . $this->tableName . ' WHERE id=?;';
        return $this->connect->execute($q, [$id]);

    }

}