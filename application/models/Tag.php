<?php

use Framework\Model;

/**
 * class Tag represents tag in blog
 *
 *  methods:
 *
 *  * getAll
 *  * getById
 *  * getByPost
 *  * getName
 *  * getMostPopular
 *  * create
 *  * update
 *  * delete
 *  * boundToPost
 *  * unboundToPost
 *
 */


class Tag extends Model {

    use BoundTrait;

    /**
     * name of the table
     *
     * @var string
     */

    protected $tableName = 'tag';


    /**
     * name of the table, where there are links between posts and tags
     *
     * @var string
     */

    protected $boundTable = 'post_tag';

    /**
     * name of the table, where there are posts
     *
     * @var string
     */

    protected $postTable = 'post';


    /**
     * returns all tags
     *
     * @return array
     */

    public function getAll() {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id'
            . ', %1$s.title as title'
            . ', ' . 'COUNT(%2$s.post) as postCount'
            . ' FROM %1$s  LEFT JOIN %2$s'
            . ' ON %1$s.id = %2$s.tag'
            . ' LEFT JOIN %3$s'
            . ' ON %2$s.post = %3$s.id'
            . ' WHERE %3$s.deletedAt is NULL'
            . ' GROUP BY %1$s.id',
            $this->tableName, $this->boundTable, $this->postTable
        );

        return $this->connect->execute($q);

    }

    /**
     * returns tag by id
     *
     * @param int           $id      id of the tag
     *
     * @return array
     */

    public function getById($id) {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id'
            . ', %1$s.title as title'
            . ', ' . 'COUNT(%2$s.post) as postCount'
            . ' FROM %1$s  LEFT JOIN %2$s'
            . ' ON %1$s.id = %2$s.tag'
            . ' WHERE %1$s.id = ?;',
            $this->tableName, $this->boundTable
        );


        return $this->connect->execute($q, [$id], false);
    }


    /**
     * returns tags by post
     *
     * @param int           $postId The post id
     *
     * @return array
     */

    public function getByPost($postId) {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.title as title'
            . ' FROM %1$s INNER JOIN %2$s'
            . ' ON %1$s.id = %2$s.tag'
            . ' WHERE %2$s.post = ?',
            $this->tableName, $this->boundTable
        );

        return $this->connect->execute($q, [$postId]);

    }

    /**
     * returns most popular tags in blog
     *
     * @param int         $count    indicates how many tags should be returned
     *
     * @return array
     */

    public function getMostPopular($count=20) {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.title as title,'
            . ' COUNT(%2$s.post) as postsCount'
            . ' FROM %1$s INNER JOIN %2$s'
            . ' ON %1$s.id = %2$s.tag'
            . ' INNER JOIN %3$s'
            . ' ON %2$s.post = %3$s.id'
            . ' WHERE %3$s.deletedAt is NULL'
            . ' GROUP BY %1$s.id'
            . ' ORDER BY postsCount DESC'
            . ' LIMIT %4$d;',
            $this->tableName, $this->boundTable, $this->postTable,$count
        );

        return $this->connect->execute($q);

    }

    /**
     * returns name of the tag by id
     *
     * @param int           $id The tag's id
     *
     * @return string
     */

    public function getName($id) {

        $q = 'SELECT title from ' . $this->tableName . ' WHERE id = ?;';

        $entry = $this->connect->execute($q, [$id], false);
        return $entry['title'];

    }

    /**
     *  creates new tag
     *
     * @param array           $data The data for tag
     *
     * @return int
     */

    public function create($data) {

        $q = 'INSERT INTO ' . $this->tableName . '(title) VALUES(?);';
        return $this->connect->execute($q, [$data['title']]);

    }

    /**
     *  updates the tag
     *
     * @param int             $id   The tag's id
     * @param array           $data The data for tag
     *
     * @return int
     */

    public function update($id, $data) {

        $q = 'UPDATE ' . $this->tableName . ' SET title=?;';
        return $this->connect->execute($q, [$data['title']]);

    }

    /**
     *  deletes the tag
     *
     * @param int           $id   The tag's id
     *
     * @return bool
     */

    public function delete($id) {

        $q = 'DELETE FROM ' . $this->tableName . ' WHERE id=?;';
        return $this->connect->execute($q, [$id]);

    }


}