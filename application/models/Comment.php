<?php

use Framework\Model;

/**
 * class Comment represents comment in blog
 *
 *  methods:
 *
 *  * getAll
 *  * getById
 *  * countByPost
 *  * getName
 *  * create
 *  * update
 *
 */

class Comment extends Model {

    /**
     * name of the table
     *
     * @var string
     */

    protected $tableName = 'comment';

    /**
     * name of the users table
     *
     * @var string
     */

    protected $userTable = 'user';

    /**
     * name of the user thumbnails table
     *
     * @var string
     */

    protected $mediaTable = 'media';

    /**
     * name of the table where are rating for comments
     *
     * @var string
     */

    protected $ratingTable = 'comment_rating';


    /**
     * returns all comments
     *
     * @param int           $postId      If set, return entries only by post
     * @param int           $userId      If set, indicate whether user already voted fro this post or not
     *
     * #return array
     */

    public function getAll($postId=null, $userId = null) {

        $q = 'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.content as content,'
            . ' %1$s.createdAt as createdAt,'
            . ' %2$s.username as authorName,'
            . ' %2$s.id as authorId,'
            . ' %3$s.path as authorAvatar,'
            . ' %1$s.parent as parent,';

        $values = [];

        if(!empty( $userId ) ) {
            $q .= ' (SELECT COALESCE(%4$s.value, 0) FROM %4$s WHERE %4$s.comment = %1$s.id AND %4$s.user = ?) as voted,';
            $values[] = $userId;
        }

        $q .= ' %1$s.deletedAt as deletedAt'
            . ' FROM %1$s LEFT JOIN %2$s'
            . ' ON %1$s.commentator = %2$s.id'
            . ' LEFT JOIN %3$s'
            . ' ON %2$s.avatar = %3$s.id';

        if($postId) {
            $q .= ' WHERE post = ?';
            $values[] = $postId;
        }

        $q = sprintf($q, $this->tableName, $this->userTable, $this->mediaTable, $this->ratingTable);

        return $this->connect->execute($q, $values);

    }

    /**
     * returns comment by id
     *
     * @param int           $id      The comment's id
     *
     * #return array
     */

    public function getById($id) {

        $q = sprintf(
            'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.content as content,'
            . ' %1$s.createdAt as createdAt,'
            . ' %2$s.username as authorName,'
            . ' %2$s.id as authorId,'
            . ' %3$s.path as authorAvatar,'
            . ' %1$s.parent as parent,'
            . ' %1$s.deletedAt as deletedAt,'
            . ' (SELECT COALESCE(SUM(%4$s.value),0) FROM %4$s WHERE %4$s.comment = %1$s.id) as rating'
            . ' FROM %1$s INNER JOIN %2$s'
            . ' ON %1$s.commentator = %2$s.id'
            . ' INNER JOIN %3$s'
            . ' ON %2$s.avatar = %3$s.id'
            . ' WHERE %1$s.id = ?',
            $this->tableName, $this->userTable, $this->mediaTable, $this->ratingTable
        );

        return $this->connect->execute($q, [$id], false);

    }

    /**
     * counts all comments for particular post
     *
     * @param int           $postId      The post's id
     *
     * #return int
     */

    public function countByPost($postId) {

        $q = 'SELECT COUNT(id) as comments FROM ' . $this->tableName . ' WHERE post = :id';

        return $this->connect->execute($q, [$postId], false)['comments'];

    }

    /**
     *  creates new comment
     *
     * @param array           $data The data for comment
     *
     * #return int
     */

    public function create($data) {

        $q = 'INSERT INTO ' . $this->tableName . '(content, commentator, post, parent) VALUES(?, ?, ?, ?);';

       return $this->connect->execute($q, [$data['content'], $data['commentator'], $data['post'], $data['parent']]);

    }

    /**
     *  updates the comment
     *
     * @param int             $id   The comment's id
     * @param array           $data The data for comment
     *
     * @return bool
     */

    public function update($id, $data) {

        $q = 'UPDATE ' . $this->tableName . ' SET content=? WHERE id=?;';
        return $this->connect->execute($q, [$data['content'], $id]);

    }

}
