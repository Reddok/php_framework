<?php

    use Framework\Model;

/**
 * class Post represents post in blog
 *
 *  methods:
 *
 *  * getAll
 *  * getById
 *  * getOtherPosts
 *  * getHottest
 *  * create
 *  * update
 *  * makePreview
 *
 */

    class Post extends Model {

        /**
         * name of the table
         *
         * @var string
         */

        protected $tableName = 'post';

        /**
         * name of the table with files
         *
         * @var string
         */

        protected $mediaTable = 'media';

        /**
         * name of the comments table
         *
         * @var string
         */

        protected $commentTable = 'comment';


        /**
         * name of the users table
         *
         * @var string
         */

        protected $userTable = 'user';

        /**
         * name of the table, where there are links between posts and categories
         *
         * @var string
         */

        protected $categoriesTable = 'post_category';

        /**
         * name of the table, where there are links between posts and tags
         *
         * @var string
         */

        protected $tagsTable = 'post_tag';


        /**
         * name of the table where are rating for comments
         *
         * @var string
         */

        protected $ratingTable = 'post_rating';


        /**
         * returns posts by filter. Can sort posts and, use for pagination and search
         *
         * @param array         $filter     array with filters
         * @param int           $limit      indicates how many posts should be returned
         * @param int           $offset     indicates how many posts should be skipped
         * @param string        $order      determines column for sorting
         * @param string        $direction  determines order for sorting
         * @param string        $search     if set, returns only those posts that have the desired value
         *
         * @return array
         */

        public function getAll($filter=[], $limit=5, $offset=0, $order = 'createdAt', $direction = 'DESC', $search=null) {

            $q = sprintf(
                'SELECT'
                . array_reduce(['id', 'content', 'title', 'author', 'preview', 'createdAt'], function($buff, $name) {
                    return $buff . ' %1$s. ' . $name . ' as ' . $name . ',';
                })
                . ' %1$s.thumbnail as thumbnailId,'
                . ' %2$s.id as authorId,'
                . ' %2$s.username as authorName,'
                . ' (SELECT %3$s.path from %3$s WHERE %3$s.id = %2$s.avatar) as authorAvatar,'
                . ' %3$s.path as thumbnail,'
                . ' COUNT(DISTINCT %4$s.id) as comments,'
                . ' (SELECT COALESCE(SUM(%7$s.value),0) FROM %7$s WHERE %7$s.post = %1$s.id) as rating'
                . ' FROM %1$s INNER JOIN %2$s'
                . ' ON %1$s.author = %2$s.id'
                . ' INNER JOIN %3$s'
                . ' ON %1$s.thumbnail = %3$s.id'
                . ' LEFT JOIN %4$s'
                . ' ON %1$s.id = %4$s.post'
                . ' INNER JOIN %5$s'
                . ' ON %1$s.id = %5$s.post'
                . ' INNER JOIN %6$s'
                . ' ON %1$s.id = %6$s.post'
                . ' WHERE %1$s.deletedAt is NULL',
                $this->tableName, $this->userTable, $this->mediaTable, $this->commentTable, $this->categoriesTable, $this->tagsTable, $this->ratingTable
            );

            $bindingParams = [];

            if($search) {
                $q .= sprintf(' AND (%1$s.title LIKE ? OR %1$s.content LIKE ? OR %1$s.preview LIKE ?)',
                    $this->tableName);
                array_push($bindingParams, "%" . $search . "%", "%" . $search . "%", "%" . $search . "%");
            }

            foreach($filter as $key => $value) {
                $q .= ' AND ' . $key . ' = ?';
                $bindingParams[] = $value;
            }


            $q .= ' GROUP BY ' . $this->tableName . '.id';
            $q .= ' ORDER BY ' . $order . ' ' . $direction;

            if($limit) $q .= ' LIMIT ' . $limit;
            if($offset) $q .= ' OFFSET ' . $offset;

            $q .= ';';

            return $this->connect->execute($q, $bindingParams);

        }

        /**
         * returns post by filter. Can sort post and, use for pagination and search
         *
         * @param int         $post     The post, which should be omitted
         * @param int         $count    indicates how many posts should be returned
         *
         * @return array
         */

        public function getOtherPosts($post, $count = 3) {

            $q = sprintf(
                'SELECT'
                . ' %1$s.id as id,'
                . ' %1$s.title as title,'
                . ' %2$s.path as thumbnail'
                . ' FROM %1$s INNER JOIN %2$s'
                . ' ON %1$s.thumbnail = %2$s.id'
                . ' WHERE %1$s.deletedAt is NULL AND %1$s.createdAt <= ? AND %1$s.id NOT IN (?)'
                . ' ORDER BY %1$s.createdAt DESC LIMIT %3$d;',
            $this->tableName, $this->mediaTable, $count);

            return $this->connect->execute($q, [$post['createdAt'], $post['id']]);

        }

        /**
         * returns posts with the most comments
         *
         * @param int         $count    indicates how many posts should be returned
         *
         * @return array
         */

        public function getHottest($count) {

            $q = sprintf(
                'SELECT'
                . ' %1$s.id as id,'
                . ' %1$s.title as title,'
                . ' COUNT(%2$s.id) as commentsCount'
                . ' FROM %1$s LEFT JOIN %2$s'
                . ' ON %1$s.id = %2$s.post'
                . ' WHERE %1$s.deletedAt is NULL'
                . ' GROUP BY %1$s.id'
                . ' ORDER BY commentsCount DESC LIMIT %3$d;',
                $this->tableName, $this->commentTable, $count);

            return $this->connect->execute($q, []);

        }

        /**
         * returns post by id
         *
         * @param int           $id      id of the post
         *
         * @return array
         */

        public function getById($id) {

            $q = sprintf('SELECT'
                . ' %1$s.id as id,'
                . ' %1$s.content as content,'
                . ' %1$s.title as title,'
                . ' %1$s.preview as preview,'
                . ' %2$s.id as authorId,'
                . ' %2$s.username as authorName,'
                . ' %1$s.createdAt as createdAt,'
                . ' (SELECT COALESCE(SUM(%4$s.value),0) FROM %4$s WHERE %4$s.post = %1$s.id) as rating,'
                . ' %3$s.path as thumbnail,'
                . ' %1$s.thumbnail as thumbnailId,'
                . ' (SELECT %3$s.path from %3$s WHERE %3$s.id = %2$s.avatar) as authorAvatar'
                . ' FROM %1$s INNER JOIN %2$s'
                . ' ON %1$s.author = %2$s.id'
                . ' INNER JOIN %3$s'
                . ' ON %1$s.thumbnail = %3$s.id'
                . ' WHERE %1$s.deletedAt is NULL AND %1$s.id=?;',
                $this->tableName, $this->userTable, $this->mediaTable, $this->ratingTable);

            return $this->connect->execute($q, [$id], false);

        }

        /**
         *  creates new post
         *
         * @param array           $data The data for post
         *
         * @return int
         */

        public function create($data) {

            $q = 'INSERT INTO ' . $this->tableName
                . '(title, content, thumbnail, preview, author)'
                . ' VALUES(?, ?, ?, ?, ?);';

            return $this->connect->execute($q, [
                $data['title'],
                $data['content'],
                $data['thumbnail'],
                $data['preview'],
                $data['author']
            ]);

        }

        /**
         *  updates the post
         *
         * @param int           $id   The post's id
         * @param array           $data The data for post
         *
         * @return bool
         */
        
        public function update($id, $data) {

            $q = 'UPDATE ' . $this->tableName . ' SET'
                . ' title=?,'
                . ' content=?,'
                . ' thumbnail=?,'
                . ' preview=?'
                . ' WHERE id=?;';

            return $this->connect->execute($q, [
                $data['title'],
                $data['content'],
                $data['thumbnail'],
                $data['preview'],
                $id
            ]);

        }

        /**
         *  make preview for the post
         *
         * @param string          $content   The post's content
         * @param int             $length    The data for post
         *
         * @return string
         */
        
        public function makePreview($content, $length = 100) {

            $text = strip_tags($content);
            preg_match_all('/\S+/', $text, $matches);
            $matches = array_slice($matches[0], 0, $length);
            return join(' ', $matches);

        }

    }