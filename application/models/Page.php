<?php

use Framework\Model;

/**
 * class Page represents page in blog
 *
 *  methods:
 *
 *  * getAll
 *  * getForMenu
 *  * getById
 *  * getByPath
 *  * create
 *  * update
 *  * delete
 *  * checkExist
 *  * baseRequest
 *
 */

class Page extends Model {

    /**
     * name of the table
     *
     * @var string
     */

    protected $tableName = 'page';

    /**
     * returns all pages
     *
     * @param int           $except      If set, omit this page
     *
     * @return array
     */

    public function getAll($except=null) {

        $params = [];
        $q = sprintf(
            $this->baseRequest() . ','
            . ' %1$s.parent as parent,'
            . ' %1$s.showInMenu as showInMenu'
            . ' FROM %1$s',
            $this->tableName
        );

        if($except) {
            $q .= ' WHERE ' . $this->tableName . '.id NOT IN (?)';
            $params[] = $except;
        }

        $q .= ' GROUP BY ' . $this->tableName . '.id;';

        return $this->connect->execute($q, $params);

    }

    /**
     * returns short version of page for menu
     *
     * @return array
     */

    public function getForMenu() {

        $q = 'SELECT id, title, slug, parent FROM ' . $this->tableName . ' WHERE showInMenu > 0;';
        $pages = $this->connect->execute($q);

        array_unshift($pages, ['title' => 'Main', 'slug' => '', 'parent' => null]);
        return $pages;

    }

    /**
     * returns page by id
     *
     * @param int           $id      The page's id
     *
     * @return array
     */

    public function getById($id) {

        $q = sprintf(
            $this->baseRequest() . ','
            . ' %1$s.parent as parent,'
            . ' %1$s.showInMenu as showInMenu'
            . ' FROM %1$s'
            . ' WHERE %1$s.id = ?;',
            $this->tableName
        );

        return $this->connect->execute($q, [$id], false);

    }

    /**
     * returns page by path
     *
     * @param string           $path      The page's path
     *
     * @return array
     */


    public function getByPath($path) {

        $q = sprintf(
            $this->baseRequest()
            . ' FROM %1$s LEFT'
            . ' WHERE %1$s.slug = ?;',
            $this->tableName
        );

        return $this->connect->execute($q, [$path], false);

    }

    /**
     *  creates new page
     *
     * @param array           $data The data for page
     *
     * @return int
     */

    public function create($data) {

        $q = 'INSERT INTO ' .  $this->tableName .'(title, slug, content, parent, showInMenu) VALUES(?,?,?,?,?);';

        return $this->connect->execute($q, [
            $data['title'],
            $data['slug'],
            $data['content'],
            $data['parent'],
            $data['show']
        ]);

    }

    /**
     *  updates new page
     *
     * @param int             $id   The page's id
     * @param array           $data The data for page
     *
     * @return bool
     */

    public function update($id, $data) {

        $q = 'UPDATE ' . $this->tableName . ' SET'
            . ' title=?,'
            . ' content=?,'
            . ' parent=?,'
            . ' showInMenu=?'
            . ' WHERE id=?;';

        return $this->connect->execute($q, [
            $data['title'],
            $data['content'],
            $data['parent'],
            $data['show'],
            $id
        ]);

    }


    /**
     * Deletes page
     *
     * @param int           $id     The id of the page
     *
     * @return bool
     *
     */

    public function delete($id) {

        $q = 'DELETE FROM ' . $this->tableName . ' WHERE id=?';

        return $this->connect->execute($q, [$id]);

    }

    /**
     * Checks if the page exists
     *
     * @param string           $path    The path of the page
     *
     * @return bool
     *
     */

    public function checkExist($path) {

        $q = 'SELECT id FROM ' . $this->tableName . ' WHERE slug = ? LIMIT 1;';
        return (bool) $this->connect->execute($q, [$path]);

    }

    /**
     * Returns base request
     *
     * @return string
     *
     */

    private function baseRequest() {

        return sprintf(
            'SELECT'
            . ' %1$s.id as id,'
            . ' %1$s.title as title,'
            . ' %1$s.slug as slug,'
            . ' %1$s.content as content,'
            . ' %1$s.createdAt as createdAt',
            $this->tableName
        );

    }

}