<?php

/**
 * trait VoteTrait. The trait add voting functionality.
 *
 *  methods:
 *
 *  * upvoteAction
 *  * devoteAction
 *  * vote
 *
 */

trait BoundTrait {

    /**
     *  create link between post and categories
     *
     * @param int           $postId   The post's id
     * @param array         $entities     The array of entities ids
     *
     * @return bool
     */

    public function boundToPost($postId, $entities) {

        if($entities) {

            $args = array_reduce($entities, function($buff, $tag) use($postId) {
                array_push($buff, $postId, $tag);
                return $buff;
            }, []);

            $q = 'INSERT INTO ' . $this->boundTable . '(post, ' . lcfirst(get_called_class()) . ') VALUES ';
            $q .= rtrim( str_repeat('(?, ?),', count($entities)), ',') . ';';

            return $this->connect->execute($q, $args);
        }

        return false;

    }

    /**
     *  destroys link between post and entities
     *
     * @param int           $postId   The post's id
     * @param array         $entities     The array of entity ids
     *
     * @return bool
     */

    public function unboundFromPost($postId, $entities)
    {

        if ($entities) {

            $q = 'DELETE FROM ' . $this->boundTable
                . ' WHERE post=? AND ' . lcfirst(get_called_class()) . ' IN (' . join(',', array_fill(0, count($entities), '?')) . ');';

            array_unshift($entities, $postId);
            return $this->connect->execute($q, $entities);

        }

        return false;

    }

}