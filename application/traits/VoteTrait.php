<?php

/**
 * trait VoteTrait. The trait add voting functionality.
 *
 *  methods:
 *
 *  * upvoteAction
 *  * devoteAction
 *  * vote
 *
 */

trait VoteTrait {

    /**
     * Rating helper that must be defined in controller
     *
     * @var object
     */

    private $ratingHelper;

    /**
     * Increase rating of entity by 1
     *
     * @param array            $req        request array
     *
     */

    public function upvoteAction($req) {
        $entity = $req['params']['id'];
        $this->vote($entity, 1, $req['user']);
    }

    /**
     * Decrease rating of entity by 1
     *
     * @param array            $req        request array
     *
     */

    public function devoteAction($req) {
        $entity = $req['params']['id'];
        $this->vote($entity, -1, $req['user']);
    }

    /**
     * vote for particular entity
     *
     * @param array            $entity        request array
     * @param int              $value         value of vote
     * @param  array            $user          voting user
     *
     */

    public function vote($entity, $value, $user) {

        if(! $this->ratingHelper->isVoted($entity, $user['id']) ) {

            $this->ratingHelper->vote($entity, $user['id'], $value);
            print $this->ratingHelper->getSum($entity);

        }

    }

}