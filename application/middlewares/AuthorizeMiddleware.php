<?php

    use Framework\{Interfaces\MiddlewareInterface, View, Oauth};

/**
 *  class AuthorizeMiddleware. Authorize user and grant some permissions
 *
 *  methods:
 *
 *  process
 *
 */

    class AuthorizeMiddleware implements MiddlewareInterface {

        /**
         * Carries out the authorization
         *
         * @param array             $req   request array
         */

        public function &process(&$req) {

            if( isset($req['session']['user']) ) {

                $model = new User();
                $req['user'] = $model->getById($req['session']['user']);
                $model->setAttending($req['user']['id']);

            } else {

                $req['user'] = [
                    'username' => 'Anonym',
                    'role' => [
                        'title' => 'Anonym',
                        'permissions' => ['postReadSelf', 'postReadAnother', 'userReadSelf', 'userReadAnother', 'pageRead']
                    ]
                ];

            }

            View::registerGlobals('currentUser', $req['user']);
            Oauth::setCurrentUser($req['user']);

        }

    }