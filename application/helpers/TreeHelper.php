<?php

/**
 * class Controller, helps receive tree structure array from a simple array
 *
 *  methods:
 *
 *  * structurize
 *
 */

class TreeHelper {

    /**
     * helps receive tree structure array from a simple array
     *
     * @param array             $items  array of items
     *
     * @return array
     */

    static public function structurize($items) {

        usort($items, function($a, $b) {
            return $a['id'] > $b['id'];
        });

        $tree = [];
        $references = [];

        foreach($items as &$el) {

            $references[$el['id']] = &$el;
            $el['children'] = [];

            if( !$el['parent'] ) {
                $tree[] = &$el;
            } else {
                $references[$el['parent']]['children'][] = &$el;
            }

        }

        return $tree;

    }

}