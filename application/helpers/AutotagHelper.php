<?php

/**
 * class AutotagHelper. The class tries to automatically select the tags for the given content.
 * This is done using the OpenCalais service, which already has a sufficient database of words.
 * In any case, it will do better than something written by me. Unfortunately, so far it only supports English and Spanish.
 * Internet access is required.
 *
 *  methods:
 *
 *  * addContent
 *  * clear
 *  * findTags
 *  * getTags
 *  * addHeader
 *  * extractMostUsefulValues
 *
 */

class AutotagHelper
{

    const API_URL = 'https://api.thomsonreuters.com/permid/calais';

    /**
     * Content
     *
     * @var string
     */

    private $_text = '';

    /**
     * tags from given content
     *
     * @var array
     */

    private $_tags = [];

    /**
     * OpenCalais requires special token for authenticate user.
     *
     * @var string
     */

    private $_token = null;

    /**
     * Headers which specified what type of data service receive and return tags
     *
     * @var array
     */

    private $_headers = [
        'Content-type: text/html',
        'outputformat: application/json'
    ];

    /**
     * Constructor
     *
     * @param string            $accessToken    opencalais token
     *
     */

    public function __construct($accessToken)
    {
        $this->_token = $accessToken;
    }

    /**
     * Adds content to internal buffer.
     *
     * @param string            $text    content to add
     *
     * @return object           reference for current instance
     *
     */


    public function addContent($text)
    {
        $this->_text .= $text;
        return $this;
    }

    /**
     * Clear internal buffer and tags. For reusing current instance.
     *
     * @return object           reference for current instance
     *
     */

    public function clear()
    {
        $this->_text = '';
        $this->_tags = [];
        return $this;
    }

    /**
     * Does actual work. Sends request to service and receive tags
     *
     * @param int           $length     number of tags, that should be returned. Completely optional.
     *
     * @return object           reference for current instance
     *
     * @throws  Exception       throws error if service is unavailable
     *
     */


    public function findTags($length = null)
    {

        $request = curl_init();

        $headers = $this->_headers;
        $headers[] = 'X-AG-Access-Token: ' . $this->_token;

        curl_setopt($request, CURLOPT_URL, self::API_URL);
        curl_setopt($request, CURLOPT_HTTPHEADER,$headers);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_POST, 1);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($request, CURLOPT_POSTFIELDS, $this->_text);


        try {

            $result = curl_exec($request);

            $result = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $result);
            $result = json_decode($result);

            $this->_tags = $this->extractMostUsefulValues($result, $length);

            return $this;

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Get found tags
     *
     * @return array          reference for current instance
     *
     */

    public function getTags()
    {
        return $this->_tags;
    }

    /**
     * Add custom header, which will be sent to service
     *
     * @param string            $key    header's name
     * @param string            $value  header's value
     *
     */

    public function addHeader($key, $value) {
        $this->_headers[$key] = $value;
    }


    /**
     * Take all received tags and try to extract most useful. But it still returns a lot of garbage...
     *
     * @param array            $result    received tags
     * @param int              $length    determines how many tags to return
     *
     * @return array
     *
     */

    private function extractMostUsefulValues($result, $length) {

        $buffer = [];
        $typesOfTag = ['socialTag', 'topics'];

        foreach ($result as $item) {
            if (in_array($item->_typeGroup, $typesOfTag)  && !empty($item->name)) {
                $buffer[] = strtolower(trim($item->name));
            }
        }

        $buffer = array_count_values($buffer);
        arsort($buffer);
        $keys = array_keys($buffer);

        if($length) $keys = array_slice( $keys, 0, $length );

        return $keys;

    }

}

