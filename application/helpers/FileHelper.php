<?php

/**
 * class FileHelper. Provide method for uploading files
 *
 *  methods:
 *
 *  * upload
 *
 */

class FileHelper {

    /**
     *
     * Upload file to specified directory
     *
     * @param array         $file   file for upload
     *
     * @return string
     *
     */

    static function upload($file) {

        $filename = preg_replace('/(\.\w+)$/', '_' . microtime(true) * 10000 . '$1', $file['name']);
        $filePath = '/uploads/' . $filename;
        move_uploaded_file($file['tmp_name'], ROOT . $filePath);

        return $filePath;
    }

}