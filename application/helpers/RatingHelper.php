<?php

/**
 * class RatingHelper. This should be a model, but it is too different from the definite interface of the model.
 *
 *  methods:
 *
 *  * getSum
 *  * vote
 *  * isVoted
 *  * isVotedArray
 *
 */


class RatingHelper {

    /**
     * column of entity
     *
     * @var string
     */

    private $column;

    /**
     * name of table
     *
     * @var string
     */

    private $tableName;

    /**
     * database connection
     *
     * @var string
     */

    private $connect;

    /**
     * Constructor.
     *
     * @param \Framework\Database        $db     database connection
     * @param string                        $type   set
     *
     * sets connection, define table name and column
     */

    public function __construct(\Framework\Database $db, $type) {

        $this->column = $type;
        $this->tableName = $type . '_rating';
        $this->connect = $db;

    }

    /**
     * Return rating
     *
     * @param string          $entityId   entity id
     *
     *
     */


    public function getSum($entityId) {

    $q = sprintf('SELECT COALESCE(SUM(%1$s.value),0) as rating FROM %1$s WHERE %1$s.%2$s = ?;',
        $this->tableName, $this->column
    );

    $rating = $this->connect->execute($q, [$entityId], false);

    return $rating['rating'];

}

    /**
     * vote for specified entity
     *
     * @param string           $entityId   entity id
     * @param int              $userId     id of user
     * @param int              $value      vote value
     *
     * @return int;
     */

    public function vote($entityId, $userId, $value=1) {

        $q = 'INSERT INTO ' . $this->tableName . '(' . $this->column . ', user, value) VALUES(?,?,?);';

        return $this->connect->execute($q, [$entityId, $userId, $value]);

    }

    /**
     * check if particular user voted for this entity
     *
     * @param string           $entityId   entity id
     * @param int              $userId     id of user
     *
     * @return bool;
     */

    public function isVoted($entityId, $userId) {

        if( is_array($entityId) ) return $this->isVotedArray($entityId, $userId);

        $q = 'SELECT user FROM ' . $this->tableName . ' WHERE ' . $this->column . '=? AND user=?;';

        return (bool) ($this->connect->execute($q, [$entityId, $userId], false));

    }

    /**
     * check if particular user voted for this entities
     *
     * @param array           $entities   entities ids
     * @param int             $userId     id of user
     *
     * @return array;
     */

    private function isVotedArray($entities, $userId) {

        $placeholders = array_fill(0, count($entities), '?');
        $q = 'SELECT ' . $this->column . ' FROM ' . $this->tableName . ' WHERE ' . $this->column . ' IN (' . join(',', $placeholders) . ') AND user=?;';

        $params = $entities;
        $params[] = $userId;

        $result = $this->connect->execute($q, $params);
        $arr = [];

        foreach($entities as $entity) {

            foreach($result as $entry) {
                if($entry[$this->column] == $entity) {
                    $arr[$entity] = true;
                    break;
                }
            }

            if(!isset($arr[$entity])) $arr[$entity] = false;

        }

        return $arr;

    }


}