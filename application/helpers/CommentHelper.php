<?php

/**
 * class CommentHelper. Allows to display comments that have a tree structure
 *
 *  methods:
 *
 *  * printComments
 *
 */

class CommentHelper {

    /**
     * Print comment. Just use predefined template
     *
     * @param array     $comments       array of comments
     * @param array     $post           post data
     * @param array     $currentUser    reference to current user
     *
     */

    static function printComments($comments, $post, $currentUser) {

        foreach($comments as $comment) {
            require ROOT . '/application/partials/comment.php';
        }
    }

}