<?php

/**
 * The class SidebarHelper implements a dynamic sidebar or something similar
 *
 *  methods:
 *
 *  * a lot of methods, that just identify different possible sections of sidebar and have similar logic.
 * It must be one method with dynamic argument, and I refactor it soon
 *
 *  * generate
 *
 */

class SidebarHelper {

    /**
     * Path to directory, where are all sections of sidebar
     *
     * @var string
     */

    public $viewsPath = ROOT . '/application/views/sidebar/';


    /**
     * Different models, which retrieve data, when it needed
     *
     * @var object
     */

    public $categoryModel = null;
    public $tagModel = null;
    public $commentModel = null;
    public $userModel = null;
    public $mediaModel = null;

    /**
     * Constructor.
     *
     * Initializes different models
     */

    public function __construct() {

        $this->mediaModel = new Media();
        $this->categoryModel = new Category();
        $this->tagModel = new Tag();
        $this->commentModel = new Comment();
        $this->userModel = new User();

    }



    public function search($currentUser) {
        require $this->viewsPath . 'search.php';
    }

    public function login($currentUser) {
        require $this->viewsPath . 'login.php';
    }

    public function popularCategories($currentUser) {

        $categories = $this->categoryModel->getAll(10);
        require $this->viewsPath . 'popularCategories.php';

    }

    public function popularTags($currentUser) {

        $tags = $this->tagModel->getMostPopular();
        require $this->viewsPath . 'popularTags.php';

    }

    public function bestAuthors($currentUser) {

        $users = $this->userModel->getMostActive();
        require $this->viewsPath . 'bestAuthors.php';

    }

    public function actions($currentUser, $post=null) {
        require $this->viewsPath . 'actions.php';
    }

    public function postAuthor($currentUser, $post) {
        require $this->viewsPath . 'postAuthor.php';
    }

    /**
     *  Generate dynamic sidebar from options that it receive
     *
     * @param array         $options        array of section names
     * @param array         $user           array of user data
     *
     * @return string       buffer of html code that represent sidebar
     *
     */

    static public function generate($options=[], $user) {

        $sidebar = new SidebarHelper();

        $buffer = '';

        foreach($options as $method => $arguments) {

            array_unshift($arguments, $user);

            if(method_exists($sidebar, $method)) {
                ob_start();
                call_user_func_array([$sidebar, $method], $arguments);
                $buffer .= ob_get_contents();
                ob_end_clean();
            }

        }

        return $buffer;

    }

}