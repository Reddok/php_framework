<?php

/**
 * class ErrorHelper. Provide interface for saving error information.
 * All methods are private and call only from magic __callStatic.
 * This allows to check and initialize the existence of a session variable at each call.
 *
 *  methods:
 *
 *  * getError
 *  * setError
 *  * clearError
 *  * setHeaders
 *  * getHeaders
 *  * check
 *
 */

class ErrorHelper {

    /**
     * Get error from session
     *
     * @return string
     */

    static private function getError(){
        return $_SESSION['error']['message'];
    }

    /**
     * Set error from session
     *
     * @param string            $error      error message
     *
     */

    static private function setError($error){
        $_SESSION['error']['message'] = $error;
    }

    /**
     * Clear error session
     *
     */

    static private function clearError(){
        unset($_SESSION['error']);
    }

    /**
     * set custom headers
     *
     */

    static private function setHeaders($headers) {
        $headers = is_array($headers) ? $headers : [$headers];
        $_SESSION['error']['headers'] = $_SESSION['error']['headers'] ?? [];

        array_push($_SESSION['error']['headers'], ...$headers);
    }

    /**
     * get custom headers
     *
     */

    static private function getHeaders() {
        return $_SESSION['error']['headers'];
    }

    /**
     * checks the session and initializes it if needed
     *
     */

    static private function check() {
        $_SESSION['error'] = $_SESSION['error'] ?? [];
    }

    static public function __callStatic($methodName, $args) {
        if( method_exists( __CLASS__, $methodName ) ) {
            static::check();
            return static::$methodName(...$args);
        } else {
            trigger_error('Call to undefined method ' . __CLASS__ . '::' . $methodName . '()', E_USER_ERROR);
        }
    }

}