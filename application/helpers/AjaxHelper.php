<?php

/**
 *  A class that has several methods for working with ajax (it should have, but I have only made one)
 *
 *  methods:
 *
 *  * isAjax
 *
 */

class AjaxHelper {

    /**
     * checks if it ajax call or not
     *
     * @return bool
     */

    static function isAjax() {

        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

    }

}