<?php

use Framework\Menu;

/**
 *  class UserMenu represent menu in user cabinet
 *
 *  methods:
 *
 *  * getItems
 *  * print
 *
 */

class UserMenu extends Menu {

    /**
     *
     * variable that consist post data, if it present
     *
     * @var mixed
     *
     */

    private $user;

    /**
     * Constructor
     *
     * sets user and post data
     *
     * @param array             $user       user array
     *
     */

    public function __construct($user) {
        $this->user = $user;
    }

    /**
     *
     * Select items for current user
     *
     */

    protected function getItems() {

        $this->items = [];

        if( User::checkPermission($this->user, [
            'pageRead',
            'pageWrite',
            'pageDelete'
        ]) ) {
            $this->items[] = [
                'title' => 'Pages list',
                'slug' => 'pages',
                'icon' => 'print'
            ];
        }

        if( User::checkPermission($this->user, [
            'userReadSelf',
            'userWriteSelf',
            'userDeleteSelf',
            'userReadAnother',
            'userWriteAnother',
            'userDeleteAnother'
        ])) {

            $this->items[] = [
                'title' => 'Users list',
                'slug' => 'users',
                'icon' => 'user-circle'
            ];

        }

        $this->items[] = [
                'title' => 'Uploads',
                'slug' => 'files',
                'icon' => 'upload'
        ];

        $this->items[] = [
            'title' => 'Profile',
            'slug' => 'users/' . $this->user['id'] . '/edit',
            'icon' => 'cog'
        ];

        if( User::checkPermission($this->user, 'postWriteSelf')) {
            $this->items[] = [
                'title' => 'Create post',
                'slug' => 'posts/create',
                'icon' => 'pencil-square'
            ];
        }


    }

    /**
     *
     * print whole menu.
     *
     */


    protected function print() {

        print '<nav class="user-menu"><ul>';

        print array_reduce($this->items, function($buffer, $item) {

            $buffer .= '<li><a href="/' . $item['slug'] .'"';

            if($item['selected']) $buffer .= ' class="active" ';

            $buffer .= '> <span class="fa fa-' . $item['icon'] . '" aria-hidden="true"></span>';
            $buffer .= $item['title'];
            $buffer .= '</a></li>';

           return $buffer;

        }, '');

        print '</ul></nav>';

    }

}