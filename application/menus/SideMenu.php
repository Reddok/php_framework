<?php

use Framework\Menu;

/**
 *  class SideMenu represent side menu in blog
 *
 *  methods:
 *
 *  * getItems
 *  * print
 *
 */

class SideMenu extends Menu {

    /**
     *
     * variable that consist current user data
     *
     * @var array
     *
     */

    private $user;

    /**
     *
     * variable that consist post data, if it present
     *
     * @var mixed
     *
     */

    private $post;

    /**
     * Constructor
     *
     * sets user and post data
     *
     * @param array             $user       user array
     * @param array             $post       post array
     *
     */

    public function __construct($user, $post = null) {
        $this->user = $user;
        $this->post = $post;
    }

    /**
     *
     * Select items for current user
     *
     */

    protected function getItems() {

        $this->items = [];

        if(!$this->user['id']) {

            array_push($this->items,
                ['title' => 'Login', 'slug' => 'login', 'icon' => 'sign-in'],
                ['title' => 'Register', 'slug' => 'register', 'icon' => 'user-plus']
            );

        } else {

            if($this->post) {

                if(User::checkPermission($this->user, 'postWriteAnother')
                || ( $this->user['id'] === $this->post['authorId'] && User::checkPermission($this->user, 'postWriteSelf'))) {

                    $this->items[] = [
                        'title' => 'Edit this post',
                        'slug' => 'posts/' . $this->post['id'] . '/edit',
                        'icon' => 'pencil'
                    ];

                }


                if(User::checkPermission($this->user, 'postDeleteAnother')
                || ( $this->user['id'] === $this->post['authorId'] && User::checkPermission($this->user, 'postDeleteSelf'))) {

                    $this->items[] = [
                        'title' => 'Delete this post',
                        'slug' => 'posts/' . $this->post['id'] . '/delete',
                        'icon' => 'times-circle'
                    ];

                }

            }

            if( User::checkPermission($this->user, 'postWriteSelf') ) {
                $this->items[] = [
                    'title' => 'Create post',
                    'slug' => 'posts/create',
                    'icon' => 'plus-square'
                ];
            }

            if( User::checkPermission($this->user, ['pageRead','pageWrite','pageDelete']) ) {
                $this->items[] = [
                    'title' => 'Create page',
                    'slug' => 'pages/create',
                    'icon' => 'file-text'
                ];

                $this->items[] = [
                    'title' => 'See all pages',
                    'slug' => 'pages',
                    'icon' => 'print'
                ];
            }

            if( User::checkPermission($this->user, ['userWriteAnother','userDeleteAnother']) ) {
                $this->items[] = [
                    'title' => 'See all users',
                    'slug' => 'users',
                    'icon' => 'user-circle'
                ];
            }

            if( User::checkPermission($this->user, 'userWriteSelf') ) {
                $this->items[] = [
                    'title' => 'Edit your profile',
                    'slug' => 'users/' . $this->user['id'] . '/edit',
                    'icon' => 'cog'
                ];
            }

            $this->items[] = [
                'title' => 'Logout',
                'slug' => 'logout',
                'icon' => 'sign-out'
            ];
        }


    }

    /**
     *
     * print whole menu.
     *
     */

    protected function print() {

        print '<ul>';

        print array_reduce($this->items, function($buffer, $item) {

            $buffer .= '<li><a href="/' . $item['slug'] .'">';

            $buffer .= '<span class="fa fa-' . $item['icon'] . '" aria-hidden="true"></span>';
            $buffer .= $item['title'] . ' >';
            $buffer .= '</a></li>';

            return $buffer;

        }, '');

        print '</ul>';

    }

}