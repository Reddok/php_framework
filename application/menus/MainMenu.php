<?php

use Framework\Menu;

/**
 *  class MainMenu represent main menu in blog
 *
 *  methods:
 *
 *  * getItems
 *  * selectActive
 *  * print
 *
 */

class MainMenu extends Menu {

    /**
     * model from which data will be taken
     */

    private $model;

    /**
     * Constructor
     * Initializes model
     *
     */

    public function __construct() {
        $this->model = new Page();
    }

    /**
     *
     * return items from model in internal buffer
     *
     */

    protected function getItems() {

        $this->items = $this->model->getForMenu();

    }

    /**
     *
     * add property selected to selected items and all parents
     *
     */

    protected function selectActive() {
        $selected = parent::selectActive();

        while($selected['parent']) {

            $select = false;

            foreach($this->items as &$item) {
                if($item['id'] === $selected['parent']) {
                    $select = true;
                    $selected = &$item;
                    break;
                }
            }

            if(!$select) break;

            $selected['selected'] = true;

        }
    }

    /**
     *
     * print whole menu. For this purpose used method printLevel
     *
     */

    protected function print() {

        $items = TreeHelper::structurize($this->items);

        print '<nav><ul class="main-menu">';

        $this->printLevel($items);

        print '</ul></nav>';

    }

    /**
     *
     * print specified level of menu items
     *
     * @param array          $items      menu items
     *
     */

    private function printLevel($items) {

        foreach ($items as $item) {

            print '<li><a href="/' . $item['slug'] . '"';

            if($item['selected']) print 'class="active"';

            print '>' . $item['title'];

            if($item['children']) print ' <span class="fa fa-angle-right"></span>';

            print '</a>';

            if($item['children']) {
                print '<ul>';
                $this->printLevel($item['children']);
                print '</ul>';
            }

            print '</li>';

        }
        
    }
}