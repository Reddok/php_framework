-- MySQL dump 10.13  Distrib 5.7.16, for Win64 (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (24,'Artificial Inteligence'),(26,'Evolution'),(21,'Hardware'),(27,'History'),(25,'OS'),(23,'Programming'),(22,'Security'),(20,'Software');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `commentator` int(10) unsigned NOT NULL,
  `post` int(10) unsigned NOT NULL,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `parent` int(10) unsigned DEFAULT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_postId` (`post`),
  KEY `index_userId` (`commentator`),
  KEY `foreign_parent_idx` (`parent`),
  CONSTRAINT `foreign_parentId` FOREIGN KEY (`parent`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreign_postId` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreign_userId` FOREIGN KEY (`commentator`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (42,'<p>rwtwre</p>','2017-07-24 21:53:32',32,33,'2017-07-24 21:54:01',NULL,'2017-07-24 21:54:01'),(43,'<p>some comment</p>','2017-07-24 21:53:48',32,33,'2017-07-24 21:59:34',NULL,'2017-07-24 21:59:34'),(44,'<p>new comment</p>','2017-07-24 21:58:42',32,33,'2017-07-24 21:58:42',43,NULL),(45,'<p>wertrewtw</p>','2017-07-24 22:11:03',32,33,'2017-07-24 22:11:03',NULL,NULL),(46,'<p><img src=\"/uploads/Cassidy-Fallout-2-Companion_15009885514532.jpg\" alt=\"Cassidy-Fallout-2-Companion.jpg\" width=\"500\" height=\"500\" /></p>\r\n<p>comment with image</p>','2017-07-25 13:26:17',33,33,'2017-07-25 13:26:17',44,NULL),(47,'<p>here you are</p>','2017-07-25 13:33:33',34,33,'2017-07-25 13:33:33',44,NULL);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_rating`
--

DROP TABLE IF EXISTS `comment_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_rating` (
  `comment` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`comment`,`user`),
  KEY `foreign_comment_rating_user_idx` (`user`),
  CONSTRAINT `foreign_comment_rating_comment` FOREIGN KEY (`comment`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreign_comment_rating_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_rating`
--

LOCK TABLES `comment_rating` WRITE;
/*!40000 ALTER TABLE `comment_rating` DISABLE KEYS */;
INSERT INTO `comment_rating` VALUES (42,29,1),(42,32,1),(42,33,1),(42,34,1),(43,29,-1),(43,33,1),(43,34,1),(45,29,1),(45,33,1),(45,34,1);
/*!40000 ALTER TABLE `comment_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filetype`
--

DROP TABLE IF EXISTS `filetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filetype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filetype`
--

LOCK TABLES `filetype` WRITE;
/*!40000 ALTER TABLE `filetype` DISABLE KEYS */;
INSERT INTO `filetype` VALUES (3,'document'),(1,'file\r'),(2,'image\r');
/*!40000 ALTER TABLE `filetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `uploadedBy` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path_UNIQUE` (`path`),
  KEY `foreign_type_idx` (`type`),
  KEY `index_uploaded_by` (`uploadedBy`,`deletedAt`),
  CONSTRAINT `foreign_type` FOREIGN KEY (`type`) REFERENCES `filetype` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (118,'/uploads/Thenamelessone_15009183408789.jpg','Thenamelessone.jpg',29,2,NULL),(119,'/uploads/team_adobestock_96318044_800-100691250-large_15009187982241.jpg','team_adobestock_96318044_800-100691250-large.jpg',29,2,NULL),(120,'/uploads/teams_page-100691254-large_15009188545063.jpg','teams_page-100691254-large.jpg',29,2,NULL),(121,'/uploads/teams_tabs-100691253-large_15009189304437.jpg','teams_tabs-100691253-large.jpg',29,2,NULL),(122,'/uploads/ransomware-100710984-large_15009192348721.jpg','ransomware-100710984-large.jpg',29,2,NULL),(123,'/uploads/Morte_Planescape_15009196411773.jpg','Morte_Planescape.jpg',30,2,NULL),(124,'/uploads/thinkstockphotos-466086694-100609677-large_15009198258719.jpg','thinkstockphotos-466086694-100609677-large.jpg',30,2,NULL),(125,'/uploads/machine-learning-ai-artificial-intelligence-100678121-large_15009208981222.jpg','machine-learning-ai-artificial-intelligence-100678121-large.jpg',30,2,NULL),(126,'/uploads/artificial-intelligence-elon-musk-hawking-100697449-large_15009211265173.jpeg','artificial-intelligence-elon-musk-hawking-100697449-large.jpeg',30,2,NULL),(127,'/uploads/virgil_15009215312304.png','virgil.png',31,2,NULL),(128,'/uploads/browser-773216-100599823-large_15009216749306.jpg','browser-773216-100599823-large.jpg',31,2,NULL),(129,'/uploads/broken_computer_waste_garbage_by_dokumol_cc0_via_pixabay-100729755-large_15009219257960.jpg','broken_computer_waste_garbage_by_dokumol_cc0_via_pixabay-100729755-large.jpg',31,2,NULL),(130,'/uploads/surface-book-surface-pro-4-resize-100721163-large_15009221701760.jpg','surface-book-surface-pro-4-resize-100721163-large.jpg',31,2,NULL),(131,'/uploads/windows-10-updates-1-100670400-primary.idge_15009224061605.jpg','windows-10-updates-1-100670400-primary.idge.jpg',31,2,NULL),(132,'/uploads/images_15009227326821.jpg','images.jpg',32,2,NULL),(133,'/uploads/the-evolution-of-the-apple-desktop-computer-0_15009231370093.jpg','the-evolution-of-the-apple-desktop-computer-0.jpg',32,2,NULL),(134,'/uploads/Cassidy-Fallout-2-Companion_15009885514532.jpg','Cassidy-Fallout-2-Companion.jpg',33,2,NULL),(135,'/uploads/Fo2_Myron_Ending_15009895259000.png','Fo2_Myron_Ending.png',34,2,NULL),(136,'/uploads/int-0998_blog_ssd-100583150-primary.idge_15009924079328.jpg','int-0998_blog_ssd-100583150-primary.idge.jpg',29,2,NULL);
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `parent` int(10) unsigned DEFAULT NULL,
  `showInMenu` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `urlPath_UNIQUE` (`slug`),
  KEY `foreign_parent_idx` (`parent`),
  KEY `show_index` (`showInMenu`),
  CONSTRAINT `foreign_page_parent` FOREIGN KEY (`parent`) REFERENCES `page` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'Contacts','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid amet asperiores cum cumque dicta eligendi error eveniet itaque laboriosam laudantium maxime nam natus nostrum odio perspiciatis praesentium quae quidem ratione repudiandae similique soluta, unde voluptates. Aperiam autem enim fugit iusto laboriosam quo veritatis. Architecto cumque dolore doloribus eligendi est eum fugit harum illo incidunt nemo optio perferendis possimus praesentium quaerat quas quasi quod recusandae rem, saepe vitae. Accusantium at consectetur cupiditate debitis dolore eligendi ex expedita facilis harum illum, labore laudantium maxime minima natus nemo, nisi non nulla numquam officiis placeat porro provident quibusdam recusandae repudiandae similique tempore voluptates? Adipisci alias aliquid architecto aspernatur assumenda atque, autem consequatur consequuntur dicta dolor eaque earum eius eligendi fugit in ipsum itaque modi officia omnis provident quaerat quas quia quidem quis quo reprehenderit rerum sapiente sed sint totam ullam veniam voluptatem voluptatibus. Ab architecto atque cumque cupiditate dignissimos dolor eligendi et eum, hic illum laboriosam, maxime modi nostrum quas sapiente soluta temporibus totam ut vel voluptates! Ab accusamus aperiam, aut consectetur ducimus ea eos error, eum excepturi ipsam itaque iure iusto molestias nulla numquam, optio sequi sunt unde veniam voluptatem? At eveniet facere id incidunt obcaecati placeat sint tempora voluptas. Aperiam, omnis.</p>','contacts','2017-07-25 07:05:16','2017-07-25 07:05:16',NULL,1),(2,'Test page','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid amet asperiores cum cumque dicta eligendi error eveniet itaque laboriosam laudantium maxime nam natus nostrum odio perspiciatis praesentium quae quidem ratione repudiandae similique soluta, unde voluptates. Aperiam autem enim fugit iusto laboriosam quo veritatis. Architecto cumque dolore doloribus eligendi est eum fugit harum illo incidunt nemo optio perferendis possimus praesentium quaerat quas quasi quod recusandae rem, saepe vitae. Accusantium at consectetur cupiditate debitis dolore eligendi ex expedita facilis harum illum, labore laudantium maxime minima natus nemo, nisi non nulla numquam officiis placeat porro provident quibusdam recusandae repudiandae similique tempore voluptates? Adipisci alias aliquid architecto aspernatur assumenda atque, autem consequatur consequuntur dicta dolor eaque earum eius eligendi fugit in ipsum itaque modi officia omnis provident quaerat quas quia quidem quis quo reprehenderit rerum sapiente sed sint totam ullam veniam voluptatem voluptatibus. Ab architecto atque cumque cupiditate dignissimos dolor eligendi et eum, hic illum laboriosam, maxime modi nostrum quas sapiente soluta temporibus totam ut vel voluptates! Ab accusamus aperiam, aut consectetur ducimus ea eos error, eum excepturi ipsam itaque iure iusto molestias nulla numquam, optio sequi sunt unde veniam voluptatem? At eveniet facere id incidunt obcaecati placeat sint tempora voluptas. Aperiam, omnis.</p>','test_page','2017-07-25 07:05:54','2017-07-25 07:05:54',1,1),(3,'Personell','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid amet asperiores cum cumque dicta eligendi error eveniet itaque laboriosam laudantium maxime nam natus nostrum odio perspiciatis praesentium quae quidem ratione repudiandae similique soluta, unde voluptates. Aperiam autem enim fugit iusto laboriosam quo veritatis. Architecto cumque dolore doloribus eligendi est eum fugit harum illo incidunt nemo optio perferendis possimus praesentium quaerat quas quasi quod recusandae rem, saepe vitae. Accusantium at consectetur cupiditate debitis dolore eligendi ex expedita facilis harum illum, labore laudantium maxime minima natus nemo, nisi non nulla numquam officiis placeat porro provident quibusdam recusandae repudiandae similique tempore voluptates? Adipisci alias aliquid architecto aspernatur assumenda atque, autem consequatur consequuntur dicta dolor eaque earum eius eligendi fugit in ipsum itaque modi officia omnis provident quaerat quas quia quidem quis quo reprehenderit rerum sapiente sed sint totam ullam veniam voluptatem voluptatibus. Ab architecto atque cumque cupiditate dignissimos dolor eligendi et eum, hic illum laboriosam, maxime modi nostrum quas sapiente soluta temporibus totam ut vel voluptates! Ab accusamus aperiam, aut consectetur ducimus ea eos error, eum excepturi ipsam itaque iure iusto molestias nulla numquam, optio sequi sunt unde veniam voluptatem? At eveniet facere id incidunt obcaecati placeat sint tempora voluptas. Aperiam, omnis.</p>','personell','2017-07-25 07:06:34','2017-07-25 07:06:34',2,1);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_permission`
--

DROP TABLE IF EXISTS `page_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_permission` (
  `page` int(10) unsigned NOT NULL,
  `permission` int(10) unsigned NOT NULL,
  PRIMARY KEY (`page`,`permission`),
  KEY `foreign_permission_idx` (`permission`),
  CONSTRAINT `foreign_page_permission` FOREIGN KEY (`page`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreign_permission_page` FOREIGN KEY (`permission`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_permission`
--

LOCK TABLES `page_permission` WRITE;
/*!40000 ALTER TABLE `page_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (24,'commentDeleteAnother'),(23,'commentDeleteSelf'),(20,'commentReadAnother'),(19,'commentReadSelf'),(22,'commentWriteAnother'),(21,'commentWriteSelf'),(33,'pageDelete'),(31,'pageRead'),(32,'pageWrite'),(18,'postDeleteAnother'),(17,'postDeleteSelf'),(14,'postReadAnother'),(13,'postReadSelf'),(16,'postWriteAnother'),(15,'postWriteSelf'),(30,'userDeleteAnother'),(29,'userDeleteSelf'),(26,'userReadAnother'),(25,'userReadSelf'),(28,'userWriteAnother'),(27,'userWriteSelf');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `author` int(10) unsigned NOT NULL,
  `thumbnail` int(10) unsigned NOT NULL,
  `preview` text,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author_index` (`author`),
  KEY `foreign_thumbnail_idx` (`thumbnail`),
  CONSTRAINT `foreign_author` FOREIGN KEY (`author`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `foreign_thumbnail` FOREIGN KEY (`thumbnail`) REFERENCES `media` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (24,'Go Teams! – Microsoft introduces new way to connect people and conversations','<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/team_adobestock_96318044_800-100691250-large_15009187982241.jpg\" alt=\"team_adobestock_96318044_800-100691250-large.jpg\" width=\"700\" height=\"467\" /></p>\r\n<p>October was a big month for Microsoft with lots of exciting&nbsp;<a href=\"https://blogs.office.com/2016/10/26/office-365-october-news-exciting-new-value-coming-to-windows-10/\" target=\"_blank\" rel=\"noopener\">updates to software and hardware</a>, but today brings the official announcement of&nbsp;<a href=\"https://products.office.com/en-US/microsoft-teams/group-chat-software\" target=\"_blank\" rel=\"noopener\">Microsoft Teams</a>&nbsp;&ndash; the much anticipated group chat workspace for Office 365 users. Microsoft Teams is a new experience that brings together people, conversations, and content in Office 365. Microsoft Teams is (are?) Microsoft&rsquo;s answer to Slack. The cool thing about Microsoft Teams, however, is that while Microsoft may be a little late to the group chat party, it&rsquo;s got all the elements needed to deliver a best-in-class solution &ndash; and these tools are the ones we already use every day.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/teams_page-100691254-large_15009188545063.jpg\" alt=\"teams_page-100691254-large.jpg\" width=\"700\" height=\"524\" /></p>\r\n<p>Microsoft Teams (http://teams.microsoft.com) adds another dimension to the way teams can collaborate. As of November 2, 2016, Teams can be enabled for your Office 365 tenant on the following plans: Business Essentials, Business Premium, and Enterprise E1, E3, and E5. It is available in preview in 181 countries and in 18 languages and is expected to be generally available by the end of the year. The Admin setting options for Teams are shown in the screen shot below. Most of the settings are off to start so each organization can make a decision about which features to enable.</p>\r\n<p>As Satya Nadella mentioned in the announcement today, &ldquo;Every individual is different and so is every team. Teamwork and collaboration is an art.&rdquo; Nadella made the analogy between work teams and other teams &ndash; like an orchestra or a jazz ensemble. Both an orchestra and jazz ensemble have similar outcomes - creating music &ndash; but they do it in different ways. An orchestra typically has a more formal arrangement to follow but for a jazz ensemble, the score may be more of a suggestion. Work teams can definitely be like this as well &ndash; which is probably why so many IT teams have gravitated to Slack. Microsoft Teams is the jazz ensemble to SharePoint&rsquo;s orchestra. But, what makes Teams so exciting for organizations is that Teams really bring together all the existing tools that people need and are already using &ndash; but Teams delivers them in an integrated experience combined with chat. For example, Word, Excel, PowerPoint, SharePoint, OneNote, Planner, Power BI, and Delve are all built into Microsoft Teams so people have all the information and tools they need. SharePoint provides the Files service for Teams, which makes me very, very happy from a governance perspective. This means that the documents that the cool jazz ensemble shares live in an organizationally controlled place &ndash; SharePoint &ndash; not outside the organization or without the controls needed to keep the security and compliance folks happy. Each Team gets a SharePoint document library and the documents for each channel live in a Folder within the document library. Each team shares a OneNote notebook as well &ndash; and each Channel gets a Section of the OneNote notebook. (This is a really intuitive design, by the way, and should be very easy to explain to users because it just makes sense!)</p>\r\n<p>Conversations in Teams are automatically threaded, which is really important for context. (Slack conversations are not threaded and can get really hard to follow if your team shares a lot of content.) You can like or flag a specific post for easy access later. You can also edit posts after they go up, though as with Facebook and Slack, these posts will have an Edit icon to let people know that it&rsquo;s been changed. Teams have standard text editing like bolding and bullets but they also let you attach fun stuff like stickers, GIFs, and, of course, emojis.</p>\r\n<aside class=\"nativo-promo smartphone\"></aside>\r\n<p>Teams have another context &ndash; Tabs. Tabs provide a dedicated place where different services are surfaced for the Team. Third-parties can create custom Tabs to provide a web experience inside the Team, allowing users to access the service in the right context and collaborate around its content. There are lots of opportunities for additional services in Teams &ndash; including Bots. (It won&rsquo;t take long to win buzzword bingo if you watch the&nbsp;<a href=\"http://news.microsoft.com/november-2016-event/\" target=\"_blank\" rel=\"noopener\">video announcing Teams</a>. Listen for digital transformation, Bots, GIFs, meme, and emojis.)</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/teams_tabs-100691253-large_15009189304437.jpg\" alt=\"teams_tabs-100691253-large.jpg\" width=\"700\" height=\"498\" /></p>\r\n<p>Microsoft Teams is a welcome addition to the Office 365 family and provides a way for teams to connect in a fun and engaging way. What is important, however, is that the Teams service is still part of the family &ndash; and leverages the services provided by the other family members to deliver an engaging user experience &ndash; membership from Groups, files from SharePoint, tasks from Planner, meeting minutes in OneNote, and video meetings provided by Skype.</p>',29,119,NULL,'2017-07-24 17:57:25','2017-07-24 17:57:25',NULL),(25,'Winning the war on ransomware','<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/ransomware-100710984-large_15009192348721.jpg\" alt=\"ransomware-100710984-large.jpg\" width=\"700\" height=\"467\" /></p>\r\n<p>Back in the &lsquo;70s, the United States suffered a severe oil shortage.</p>\r\n<p>Lines at the gas station filled city blocks. Thieves siphoned gasoline not to save money but time. In response, the federal government created a measurement, miles per gallon. Since then, MPG has become a factor in many car purchase decisions. Today, fuel efficiency has improved threefold, and we have hybrid and electric cars.</p>\r\n<p>We are facing another crisis that threatens our way of life &mdash;&nbsp;<a href=\"http://www.computerworld.com/article/3196292/windows-pcs/microsoft-issues-first-windows-xp-patch-in-3-years-to-stymie-wannacrypt.html\">ransomware</a>. Cybercriminals hold hostage individual, public sector and enterprise data with remarkable ease and frequency. Although paying ransoms may solve a short-term problem, it almost guarantees that attacks will continue creating a larger threat to our digital society.</p>\r\n<p>Ransomware commonly infects via a disguised email attachment. Unwitting users click, and at that point antivirus software should detect and block the ransomware. That&rsquo;s the security hole &mdash; email &mdash; and that&rsquo;s the measurement &mdash; detection rates.</p>\r\n<aside class=\"nativo-promo smartphone\"></aside>\r\n<p>Ransomware can be defeated if we consider detection rates when purchasing security software. As detection rates rise, ransomware payments will decline, and cybercriminal income will fall. As their income falls, they will have less to invest in more sophisticated attacks. As detection rates continue to rise, their income will fall to the point where alternative and legal vocations will become more attractive.</p>\r\n<p>Sadly, many antivirus (AV) products do not participate in public detection rate testing. These companies complain that the tests are expensive and not realistic. This position hurts the public&rsquo;s ability to view a comprehensive head-to-head AV detection rate test.</p>\r\n<p>With that in mind, my company partnered with AV Comparatives from Innsbruck, Austria, to create an involuntary test, where no product would be aware when and how the test would be executed. The sample size of the test exceeded 5,000 viruses and with two separate ransomware sets. The test was comprehensive including 28 different AV products.</p>\r\n<aside class=\"fakesidebar\"><strong>[ To comment on this story, visit&nbsp;<a href=\"https://www.facebook.com/Computerworld/posts/10155336951259680\" target=\"_blank\" rel=\"noopener\">Computerworld\'s Facebook page</a>. ]</strong></aside>\r\n<p>The highlight of the report is Microsoft Windows Defender&rsquo;s surprisingly high detection rates. Microsoft has historically had a curious love-hate relationship with the antivirus community. They positioned Windows Defender and Security Essentials as a last-resort AV inferior to any and all other security. In the last three years, the gloves have come off, and Windows Defender is superior to many pay alternatives.</p>\r\n<aside class=\"nativo-promo tablet desktop\"></aside>\r\n<p>Microsoft is raising the bar on detection rates and that is good for the battle against ransomware and the entire Windows ecosystem. The message is clear. When purchasing AV, select products that have better detection rates than Windows Defender.</p>\r\n<p>Microsoft&rsquo;s sudden rise in detection rates demonstrates that the world of antivirus is not static. The cybercriminals are rapidly increasing in sophistication, and antivirus products are frankly struggling to adapt. Today&rsquo;s studs can be tomorrow&rsquo;s duds. A great antivirus must constantly and continuously evolve in order to stay current and remain great.</p>\r\n<p>Detection rates bring much-needed objectivity into the purchase process of security software. Sadly, the purchase of security software is analogous to how consumers buy beer and toothpaste. A brand is chosen based on largely subjective attributes, and seldom is that choice reevaluated. In the battle against ransomware, one&rsquo;s security solution should be constantly reevaluated in search of higher detection rates against the latest threats.</p>\r\n<p>Unlike beer and toothpaste, automobiles are replaced every three to four years. This drives a cycle of innovation where cars improve in security features, technology and MPG. A similar cycle can begin with antivirus software and lead us to a world without ransomware. It all begins by considering detection rates when purchasing security software.</p>',29,122,NULL,'2017-07-24 18:02:40','2017-07-24 18:02:40',NULL),(26,'Don’t make poets become programmers','<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/thinkstockphotos-466086694-100609677-large_15009198258719.jpg\" alt=\"thinkstockphotos-466086694-100609677-large.jpg\" width=\"580\" height=\"373\" /></p>\r\n<p>Computers are now part of everything we do. Resultantly, there is a growing assumption that everyone is going to have to master programming in addition to their other skills. Just as there was a push towards &ldquo;data literacy,&rdquo; we are seeing a similar push towards &ldquo;code literacy.&rdquo;</p>\r\n<p>The industry&rsquo;s current approach to hiring reflects this shift and it is becoming somewhat problematic. In particular, we are seeing more and more companies taking the stance that, no matter your core job, being able to program is always going to be a plus.</p>\r\n<p>This is a really bad idea.</p>\r\n<p>To understand my position, let&rsquo;s go back to 2009. My research partner,&nbsp;<a href=\"http://www.mccormick.northwestern.edu/research-faculty/directory/profiles/birnbaum-larry.html\">Larry Birnbaum</a>, and I started teaching a class at&nbsp;<a href=\"http://www.medill.northwestern.edu/\" target=\"_blank\" rel=\"noopener\">Medill</a>, Northwestern University&rsquo;s School of Journalism, aimed at bringing computer science and journalism students together in cross-functional teams. &nbsp;</p>\r\n<aside class=\"nativo-promo smartphone\"></aside>\r\n<p>2009 was also the year in which it was becoming clear that journalism was being massively disrupted along every imaginable dimension because of a combination of factors; Google (disintermediation), Craigslist (undercutting the monetization of classifieds) and the rise of Web 2.0 (everyone was a journalist). In response to this disruption, journalism students thought they needed to learn programming</p>\r\n<p>Larry and I envisioned a different path forward. Rather than learning to program, they needed to develop the ability to work with engineers. In many ways, journalism was expanding beyond writing -- stories were becoming multi-faceted products and journalists needed to become media product designers. They needed to be able to communicate functional needs to engineers who could execute against them and provide ongoing feedback during the process. This is a far more powerful skill than knowing javascript.</p>\r\n<p>Of course, since 2009, computation and connectivity have disrupted nearly everything. And as a result, we are hearing the same siren song drawing everyone to the nirvana of programming training. The message is simple: Computers are the future and if you don&rsquo;t know how to program, you will be left behind. Period.</p>\r\n<p>If we step back for a moment, it is clear that we shouldn&rsquo;t blindly follow the programming path: &nbsp;</p>\r\n<aside class=\"nativo-promo tablet desktop\"></aside>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p>Yes, computers are the future. There is very little in the developed world that is untouched by computation and their impact will continue to grow.</p>\r\n</li>\r\n</ul>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p>Yes, we need more developers at all levels and all areas of work. If the number of people with computational skills does not increase, we will fall behind.</p>\r\n</li>\r\n</ul>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p>And yes, there are powerful problem-solving skills that are also aligned with learning how to program, and these skills are powerful tools in a wide variety of areas.</p>\r\n</li>\r\n</ul>\r\n<p>We can all agree on these points. But it does not follow that in order to function in business and the rest of world, everyone needs to be able to write (or even understand) code. &nbsp;</p>\r\n<p>I do argue that everyone should learn how to decompose large problems into smaller ones, abstract solutions in a way that makes them reusable and think about the complexity of those solutions. These are skills that flow from training in &ldquo;computational thinking&rdquo; and make people into better problem solvers. But there is a huge difference between computational thinking and programming.</p>\r\n<p>And these skills are only one component of a larger suite of reasoning capabilities. Everyone should learn how to avoid cognitive pitfalls such as&nbsp;<a href=\"https://www.verywell.com/what-is-functional-fixedness-2795484\">functional fixedness</a>&nbsp;and&nbsp;<a href=\"http://www.lifehack.org/articles/communication/how-the-sunk-cost-fallacy-makes-you-act-stupid.html\">sunk cost fallacy</a>&nbsp;and be able to think about innovation as defined by needs and goals and craft communication for impact&rsquo;s sake.</p>\r\n<p>These are essential skills that are rarely part of a software engineer&rsquo;s training.<strong>&nbsp;</strong></p>\r\n<p>The future workforce is going to require more than the ability to code -- we also need people who are able to craft the next round of transformational products and services.&nbsp;</p>\r\n<p>For example, Uber&rsquo;s success stems from effective use of technologies aimed at a product that is the poster child for disruption. It connected underutilized resources (drivers and cars) with users who were impatient with a locked down and highly regulated market. The Uber stack is essential, but the innovation that drives it is less the code base and more the product. When we hear people suggesting things like, &ldquo;Uber for dry cleaners,&rdquo; we understand that they&rsquo;re suggesting a direct and flexible relationship between customer and server; they are not talking about code.</p>\r\n<p>An even more timely example is&nbsp;<a href=\"http://www.computerworld.com/article/3050928/cloud-computing/why-microsoft-wants-to-help-developers-build-bots.html\" target=\"_blank\" rel=\"noopener\">Microsoft&rsquo;s efforts to build out its chatbot platform</a>. It is a reflection of the balance between engineering and product. The company&rsquo;s chatbot team has developers but it also has&nbsp;<a href=\"https://www.washingtonpost.com/news/the-switch/wp/2016/04/07/why-poets-are-flocking-to-silicon-valley/\">improvisors and poets</a>&nbsp;who shape the chatbots&rsquo; conversational interactions. The result is a powerful cross-functional team made up of experts able to leverage each other&rsquo;s technical and linguistic strengths.</p>\r\n<p>We all have finite cognitive resources. Imagine if everyone had to not only understand and reason about the problems associated with their own jobs (e.g. how are we going to get millennials to buy life insurance?) while also thinking about the data layer (e.g. how do I determine the probability of risk aversion associated with a given age?) and implementation (e.g. should I use pandas or statsmodels to do my regression analysis?). If everyone has to think about everything all the time, nothing would ever get done.</p>\r\n<p>Yes, you probably need a boatload of new developers and you should hire them. But do not assume that all new employees should know how to program. There are skills that complement those of the developer that, depending on the role, trump the ability to write code.</p>\r\n<p>Instead of having everyone learn javascript, it might make more sense for us all to take communication classes. Then, at least, we&rsquo;ll be able to effectively talk with each other about the products and services we want to build as well as the way we are going to go about building them.</p>',30,124,NULL,'2017-07-24 18:13:51','2017-07-24 18:13:51',NULL),(27,'ArtificiaI intelligence, APIs and the transformation of computer science','<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/machine-learning-ai-artificial-intelligence-100678121-large_15009208981222.jpg\" alt=\"machine-learning-ai-artificial-intelligence-100678121-large.jpg\" width=\"700\" height=\"451\" /></p>\r\n<p>These are exciting times in the world of artificial intelligence as advanced capabilities escape the labs and become central to mainstream products -- think&nbsp;<a href=\"http://buy.geni.us/Proxy.ashx?TSID=14159&amp;GR_URL=https%3A%2F%2Fwww.amazon.com%2FAmazon-Echo-Bluetooth-Speaker-with-WiFi-Alexa%2Fdp%2FB00X4WHP5E\" data-amzn-asin=\"B00X4WHP5E\">Amazon Echo</a>&nbsp;-- and increasingly accessible to everyday software developers.</p>\r\n<p>Recently, we&rsquo;ve seen a run of large companies open-sourcing their A.I. platform APIs. Google&nbsp;<a href=\"https://www.tensorflow.org/\">has open-sourced TensorFlow</a>, its machine learning library. Not to be outdone, Amazon has released its TensorFlow competitor&nbsp;<a href=\"https://github.com/amznlabs/amazon-dsstne\">DSSTNE (or Deep Scalable Sparse Tensor Network</a>) and Facebook has done the same with&nbsp;<a href=\"https://research.facebook.com/blog/fasttext/\">fastText</a>, its machine learning engine for text classification and language recognition. Those big-company A.I. platforms join -- and in some sense compete with -- notable open source deep learning libraries like&nbsp;<a href=\"https://github.com/BVLC/caffe\">Caffe</a>,&nbsp;<a href=\"https://github.com/Theano/Theano\">Theano</a>&nbsp;and&nbsp;<a href=\"https://github.com/torch/torch7\">Torch</a>.</p>\r\n<p>In many ways, open sourcing access to A.I. represents the culmination of the &ldquo;API economy,&rdquo; where software developers set aside their not-invented-here pride of creation and instead access the work done by others -- and in the process accelerate their own development work -- via API hooks. If software development-by-API made sense in the past, it may be the only practical path forward when it comes to fully leveraging new capabilities in the artificial intelligence field.</p>\r\n<p>Indeed, the impact of these moves can&rsquo;t be overstated. On a business level, they significantly lower the barrier for entrepreneurs looking to start up the next wave of artificial intelligence-driven companies by democratizing access to machine learning resources. That&rsquo;s hugely significant.</p>\r\n<aside class=\"nativo-promo smartphone\"></aside>\r\n<p>But I&rsquo;d argue for an even more fundamental change: Open source A.I. APIs represent the beginning of a transformation of computer science (CS) into a field focused on impact.</p>\r\n<h3><strong>Innovators,</strong>&nbsp;<strong>integrators and designers</strong></h3>\r\n<p>At its most basic, CS can be described as people working on new algorithms or improvements to existing ones, with new &ldquo;inventions&rdquo; being built on the back of what came before it. In CS, there are two standard ways of doing this: creating libraries of functionality that developers can build into their own applications; or providing access to functionality in one fell swoop via API.</p>\r\n<p>Software development via API is hardly new. Microsoft became a behemoth on the strength of its platform APIs and the swarms of developers that consumed them. More recently, SaaS companies like Salesforce and Google have offered developers the ability to extend their platforms via API. And in the latest trend, new programming concepts like microservices and bots are centered on assembling applications via modular, distributed processes accessed via open APIs.</p>\r\n<p>The latest surge in A.I. APIs opens up a whole new world of capabilities to software developers. Need an engine that can understand spoken language, recognize a single face from a crowd of thousands, pull meaning from a mountain of social media sentiment or act as the \"brain\" behind an industrial robot or self-driving car? Yesterday&rsquo;s programmatic algorithms won&rsquo;t cut it; you need machine learning and A.I.</p>\r\n<aside class=\"nativo-promo tablet desktop\"></aside>\r\n<p>Like yesterday&rsquo;s code libraries, you could try to build A.I. platforms yourself -- if you had a few years and a dozen data scientists to throw at the problem. Or you can access A.I. engines like IBM&rsquo;s Watson or Google&rsquo;s TensorFlow &ldquo;as-a-service,&rdquo; taking advantage of the planet&rsquo;s most advanced, fundamental CS work via an API call.</p>\r\n<p>When one looks at the world of software in this way, the choice for most companies today is straightforward: spend years of effort and millions of dollars in expense duplicating extremely important -- but ultimately commodity, especially once it&rsquo;s open-sourced -- computer science work, or instead focus on leveraging that work to develop and improve their own products and intellectual property. For most businesses, the choice is simple.</p>\r\n<p>In this new future, the choice for developers also becomes clear. They can be software innovators, integrators or designers. These roles will slightly overlap in some ways given an individual&rsquo;s skillset and company needs but by simplifying the roles, its easier to communicate how I envision our way forward.</p>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p>Innovators work out on the edge of research and development, creating new algorithms that have not yet been built and extending the capabilities of those that already exist. Fundamental CS work is always required and absolutely crucial but in today&rsquo;s open source-driven software environment also increasingly a commodity.</p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p>Designers focus on crafting new products, solutions and applications, using powerful technologies like machine learning and A.I. -- accessed via open APIs -- to identify new opportunities, create new solutions and solve problems in new ways altogether.</p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p>Integrators take existing capabilities and build them into new products. Their role is to deal with the nuts and bolts of implementing the designs that bring innovations together. They do not design the algorithms or products; rather, they make them work.</p>\r\n</li>\r\n</ul>\r\n<p>It&rsquo;s absolutely crucial for academia, businesses and even developers themselves to understand the distinctions between these three very different types of software developers. They represent three different skill sets and approaches to the business of software development.</p>\r\n<p>For companies, hire wrong and the results can be devastating. Bring on an innovator to do an integration job and nine months later they&rsquo;ll still be tinkering under the hood. Need some exploratory data work done? Hire an innovator, yes, but don&rsquo;t expect them to build your industry&rsquo;s next breakout product. For individual developers, mismatched skills and ambitions can be just as damaging, not only to someone\'s career path but day-to-day enjoyment of the job and task at hand.</p>\r\n<p>More than ever in a computer science landscape defined by systems in artificial intelligence and big data manipulation and analytics, not all data scientists and programmers are created equal. That&rsquo;s not a value judgement, i.e., good vs. bad. It&rsquo;s about a difference in kind. Innovators, designers and integrators.</p>\r\n<p>Tomorrow&rsquo;s worlds of business -- and software development -- needs them all, and more importantly needs to be able to tell them apart and pick the right tool for the right job. The future of computer science depends on it.</p>',30,125,NULL,'2017-07-24 18:30:18','2017-07-24 18:30:18',NULL),(28,'Artificial Intelligence: Transparency isn’t just a trend','<p dir=\"ltr\">&nbsp;Istraddle three very different worlds. First of all, I am a technologist living in an academic environment as a professor of computer science at Northwestern University. But I am also a co-founder of Narrative Science, an advanced natural language generation company that is a thriving business. And my work in both of these worlds has lead to a substantial amount of time spent with different government organizations that are considering how and when A.I. should be utilized.</p>\r\n<p dir=\"ltr\">Each world has its own concerns and it is no secret that they occasionally look at each other with some skepticism. Academics and businesses each see the other as completely missing the point. The government tends to see businesses as too short-sighted while stereotyping academics as having their heads in the clouds. And everyone sees the government as hampered by bureaucracy. All things considered, these three communities rarely tend to agree.</p>\r\n<p dir=\"ltr\">You can imagine how my ears perked up when I recently heard the same topic surfacing over and over again -- transparency. All three worlds -- academia, business and government - weren&rsquo;t just in agreement but actually in aggressive alignment. In particular, there is a growing realization that we cannot start deploying and using intelligent systems, machine learning solutions or cognitive computing platforms if their reasoning is opaque. We need to know what they are thinking.</p>\r\n<p dir=\"ltr\">Without explanation, we are blindly trusting the output of systems that many of us don&rsquo;t understand at an algorithmic level. While somewhat tolerable for systems that are identifying faces on Facebook, it is unacceptable for systems that are integrating highly sensitive and highly valuable business logic, goals and priorities into their reasoning. Nothing much rests on the former while our livelihoods are driven by the latter.</p>\r\n<aside class=\"nativo-promo smartphone\"></aside>\r\n<p dir=\"ltr\">The idea of machines explaining both themselves and the world that surrounds them has always been a personal driver for my work. While readers may perceive this as a comment driven by my substantial stake in a company whose business is machine-driven explanation, the causality is reversed. I don&rsquo;t believe in the need for machines to explain themselves because it serves my business. Rather, Narrative Science exists as a business because we believe that transparency is essential for humans who work with data, analytics and intelligent systems.</p>\r\n<p dir=\"ltr\">For some time, it has felt like the issue of transparency had fallen to the wayside with people (mostly vendors) arguing that performance alone should support acceptance. I am happy to say, however, that over the past few months there has been an upsurge of interest in transparency.</p>\r\n<p dir=\"ltr\">On the academic side, I have seen three vastly different technology talks exploring the science of models used to unpack the results of deep learning systems and the threads of thought associated with evidence-based approaches to reasoning such as&nbsp;<a href=\"http://www.computerworld.com/article/3135852/artificial-intelligence/ibm-in-5-years-watson-ai-will-be-behind-your-every-decision.html\" target=\"_blank\" rel=\"noopener\">IBM Watson</a>. On the business side, I keep finding myself in conversations with CIOs who are asking how they can deploy learning and reasoning systems that are completely opaque. In effect, asking the question, how do we use a piece of software that no one can understand? Finally, the topper was a recent announcement from DARPA requesting proposals for work in \"<a href=\"http://www.darpa.mil/program/explainable-artificial-intelligence\">explanatory A.I</a>.\"</p>\r\n<p dir=\"ltr\">So clearly, the awareness and demand for transparency is growing, but the question is now -- how do we actually make these systems more transparent? Some technologies, such as deep learning models, are opaque to the point of disagreement among practitioners as to what they are actually doing once you step above the specifics of the algorithms. So what can we do?</p>\r\n<aside class=\"nativo-promo tablet desktop\"></aside>\r\n<p dir=\"ltr\">In the long run, we need to focus on the design of systems that don&rsquo;t just think, but can think&nbsp;<em>and</em>&nbsp;explain. As we wait for this to happen, there are a few rules of the road to follow for evaluating current systems.</p>\r\n<p dir=\"ltr\"><strong>Above all else -- do not deploy an intelligent system if you cannot explain its reasoning.</strong>&nbsp;You need to understand what a system is doing even if you don&rsquo;t understand how it&rsquo;s doing it at a detailed algorithmic level. These are table stakes because it&rsquo;ll support your understanding of the data required, the decisions it is going to make and the reasoning it uses to support those decisions.</p>\r\n<p dir=\"ltr\">Beyond this essential issue, there are the three levels of capabilities that are important.</p>\r\n<ul>\r\n<li dir=\"ltr\"><strong>Explain and consider</strong>&nbsp;-- Optimally, systems should be able to explain and consider. We need a system to provide clear and coherent explanations of how it has come to a decision and the alternatives. For example, a system that is designed to identify possible vendor fraud needs to be able to not only list the features that participated in an alert but also explain why each of those features is indicative of fraud. Since some indicators may be outside of the data set or not included in a system&rsquo;s model, the possibility of presenting those features to a system and assessing their impact is also crucial. The ability to ask, &ldquo;What about X?&rdquo; is crucial when working with people and is similarly crucial as we begin to work with intelligent systems.</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><strong>Articulation</strong>&nbsp;-- Even those systems that are not open to end user manipulation of their models need to be able to at least articulate the features participating and the framework of the reasoning itself. This does not mean simply presenting a picture of the ten thousand pieces of evidence that lead to a conclusion. Systems need to be able to at least extract the truly relevant features and how they interact to support the reasoning. If a system alerts a user that it has recognized an instance of fraudulent behavior, it should be able to indicate the set of untoward transactions that set off the alert.</p>\r\n</li>\r\n</ul>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><strong>Audibility</strong>&nbsp;-- If a system is not designed to provide real-time or user-facing explanations or articulation of a piece of reasoning, it needs to at least be auditable after the fact. There has to be a logic trace that is available for inspection so that any problems can be unpacked for inspection. Even if an end user cannot get access to the trace of a system, the analysts who designed the back end need to be able to see it.</p>\r\n</li>\r\n</ul>\r\n<p dir=\"ltr\">Given the early stage of A.I. technologies, many systems simply cannot yet support explanation, consideration, articulation or audits. These systems remain powerful, but should only be used in areas where an&nbsp;explanation of its reasoning is unnecessary -- like facial recognition photo tagging on Facebook. This same system would be inappropriate for evaluating the creditworthiness of someone applying for a mortgage loan, because while it might perform accurately, it will not provide a useful explanation once the applicant questions your &ldquo;yes&rdquo; or &ldquo;no.&rdquo;</p>\r\n<p dir=\"ltr\">As with humans, we want to be able to work with rather than for A.I. systems in our homes and workplaces. To support this, these systems need to possess the ability to explain themselves to us. Otherwise, we will be in a position where all we can do is listen and obey. As always with A.I., we have a choice between developing systems that will act as partners and those that will simply be telling us what to do. &nbsp;</p>\r\n<p dir=\"ltr\">While transparency may seem like a technical issue, it has widespread societal and economical implications. Without transparency, users will be hard-pressed to fully trust and respect A.I. systems. Without trust and respect, the adoption of A.I. systems will stall and potentially wither the vast and positive returns that these technologies could bring to our world.</p>',30,126,NULL,'2017-07-24 18:33:32','2017-07-24 18:33:32',NULL),(29,'How Google\'s Chrome browser does updates','<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/browser-773216-100599823-large_15009216749306.jpg\" alt=\"browser-773216-100599823-large.jpg\" width=\"700\" height=\"495\" /></p>\r\n<p>Chrome has been the world\'s most popular browser for more than a year now, convincing first individuals and then enterprises that it was the best replacement for Microsoft\'s once-leading Internet Explorer.</p>\r\n<p>But Google\'s browser has not been without its critics. Among the sticking points has been Chrome\'s automatic updating mechanism, which some decried for force-feeding unwanted changes, or for delivering those changes at speeds too quickly for customers to absorb.</p>\r\n<p>We\'ve dug into Chrome\'s updates, from their frequency and schedule to how to manage them as an individual or as an IT professional. Here\'s what we found.</p>\r\n<h3>Automatic updates, or manual trigger</h3>\r\n<p>Chrome updates itself in the background -- and has done so since it debuted nearly nine years ago -- so most users need do nothing but relaunch the browser once in a while.</p>\r\n<aside class=\"nativo-promo smartphone\"></aside>\r\n<p>The browser periodically checks for updates; Google\'s own documentation, which is often left long out of date on the firm\'s site, says in one place it does so every 23 hours and 20 minutes. When Chrome does detect an available update, it downloads the new code and preps it for installation, although that latter step doesn\'t begin until the user starts or restarts the browser.</p>\r\n<aside class=\"fakesidebar\"><strong>[ To comment on this story, visit&nbsp;<a href=\"https://www.facebook.com/Computerworld/posts/10155543997604680\" target=\"_blank\" rel=\"noopener\">Computerworld\'s Facebook page</a>. ]</strong></aside>\r\n<p>But not every copy of Chrome receives an update as soon as Google issues one. Instead, the company spreads out the distribution over days, or sometimes even weeks.</p>\r\n<p>To manually trigger an update -- to get the security fixes pronto when news of active attacks circulate, say -- users simply select \"About Google Chrome\" from the Help menu under the vertical ellipsis at the upper right. The ensuing page either reports \"Google Chrome is up to date\" or displays the updating process before presenting a \"Relaunch\" button.</p>\r\n<h3>How to know when Google will next update Chrome</h3>\r\n<p>Google issues a new version of Chrome approximately every six weeks, although so far this year the average time between releases has been 47 days, or closer to seven weeks.</p>\r\n<p>The company does not keep to a set schedule -- as does, for example, Mozilla with its Firefox -- nor does Google do much to trumpet each release. Officially, Google recommends that users&nbsp;<a href=\"https://chromereleases.googleblog.com/\" target=\"_blank\" rel=\"noopener\">frequent this blog</a>&nbsp;to track just-issued updates, including the security-only fixes that pop up at irregular intervals between each polished edition\'s arrival.</p>\r\n<p>Unofficially, users can view Chrome\'s&nbsp;<a href=\"http://www.chromium.org/developers/calendar\" target=\"_blank\" rel=\"noopener\">estimated release schedule here</a>, to get an idea of when the next version will be distributed. \"These dates are subject to change without advance notice and provided here only for rough planning purposes,\" Google\'s Chromium team states there.</p>\r\n<p>Chrome 59 should update to Chrome 60 shortly, during the week of July 23-29. Future updates in 2017 should appear the weeks of Sept. 3-9, Oct. 15-21, and Dec. 3-9.</p>\r\n<h3>For the individual: How to disrupt Chrome updates</h3>\r\n<p>Although there have been numerous Internet-posted instruction sets that purport to show individuals how to shut down Chrome\'s automatic updates, those efforts are mostly misguided, and in some cases, ultimately futile.</p>\r\n<p>They\'re misguided because the goal of all auto-updating is a sterling one: More secure browsing and a lessened chance of malware hijacking the application and planting itself on a PC. Removing the responsibility for updates, particularly security updates, from the user has been a decades-long theme in software for a good reason, as it results in a higher percentage of up-to-date devices. (<a href=\"http://www.computerworld.com/article/3196292/windows-pcs/microsoft-issues-first-windows-xp-patch-in-3-years-to-stymie-wannacrypt.html\">This spring\'s WannaCry attacks</a>&nbsp;starkly illustrated the differences between quickly-patched and unpatched Windows systems.)</p>\r\n<p>They\'re futile because Chrome\'s updating mechanism cannot be permanently switched off without the business infrastructure of Active Directory. \"To prevent abuse of this policy, if a device is not joined to an Active Directory domain, and if this policy has been set to 0 or to a value greater than 77 hours, this setting will not be honored and replaced by 77 hours after August 2014,\" a Chrome&nbsp;<a href=\"https://www.chromium.org/administrators/turning-off-auto-updates\" target=\"_blank\" rel=\"noopener\">support document</a>&nbsp;reads, referring to a group policy that allows enterprise IT staffers to disable the feature.</p>\r\n<p>In plainer English, that means attempts to turn off auto updates, including by setting a Windows Registry key -- a cornerstone of many of the techniques available on the web -- will fail as the time between update checks reverts to 77 hours, or about 3.2 days.</p>\r\n<p>Individuals can disrupt automatic updates on Windows PCs, however, by nixing the executable the browser relies on for its connection to Google\'s servers. The simplest way is to locate the file&nbsp;<em>GoogleUpdate.exe</em>&nbsp;-- it should be in the folder&nbsp;<em>C:\\Program Files (x86)\\Google\\Update</em>&nbsp;-- and rename it. Any new name will do, say,&nbsp;<em>GoogleUpdate_disabled.exe</em>.</p>\r\n<p>After restarting Chrome, any attempt, whether automatic on the part of the browser or manual by the user, will fail.</p>\r\n<p>To later update Chrome, the executable\'s name must be restored to&nbsp;<em>GoogleUpdate.exe</em>.</p>\r\n<h3>For IT: How to manage Chrome updates</h3>\r\n<p>Centrally-managed copies of Chrome do not update from a central location or console controlled by IT (as do, for example, Windows and its Internet Explorer (IE) and Edge browsers in most organizations). Instead, they are, like Chrome run by individuals, updated at Google\'s inclination and timetable.</p>\r\n<p>But Chrome&nbsp;<em>does</em>&nbsp;support a number of Windows group policies that can be managed -- as are, probably, hosts of others -- by IT. They include policies for shutting down the automatic updates and for lengthening the time between update checks.</p>\r\n<p>Google provides the necessary tools as part of an \"enterprise bundle\" it&nbsp;<a href=\"http://www.computerworld.com/article/3198111/web-browsers/google-raises-heat-on-microsoft-with-new-chrome-bundle-for-enterprises.html\" target=\"_blank\" rel=\"noopener\">rolled out in late May</a>. Those tools include templates for applying group policies to Chrome -- in both .adm and the newer .admx formats -- that administrators may use to manage individual system\'s Chrome browsers, or the entire company\'s fleet.</p>\r\n<p>Among the policies are those that let admins disable all updates, barring automatic and manual updates from occurring; and override the default time between update checks, shortening them to as little as an hour or extending them to as much as 30 days.</p>\r\n<p>Because Chrome management relies on group policies, Active Directory is required.</p>\r\n<h3>Where to obtain .msi packages for Chrome</h3>\r\n<p>After turning off Google\'s updates, IT must distribute its own -- on its schedule -- using a .msi installation package. That package can be deployed with the organization\'s standard deployment tools, including Microsoft\'s own SCCM (System Center Configuration Manager).</p>\r\n<p>The&nbsp;<a href=\"https://enterprise.google.com/chrome/chrome-browser/\" target=\"_blank\" rel=\"noopener\">.msi installation packages for the current version of Chrome</a>&nbsp;for Windows are available from Google\'s website.</p>\r\n<p>More information about Chrome updates</p>\r\n<p>The&nbsp;<a href=\"https://docs.google.com/document/d/1iu6I0MhyrvyS5h5re5ai8RSVO2sYx2gWI4Zk4Tp6fgc/edit\" target=\"_blank\" rel=\"noopener\">Chrome Deployment Guide</a>&nbsp;will be of great help to enterprise admins responsible for managing the browser. The guide can be downloaded from Google\'s site.</p>\r\n<p>Other references include the&nbsp;<a href=\"https://support.google.com/chrome#topic=7438008\" target=\"_blank\" rel=\"noopener\">Chrome Help Center</a>, the&nbsp;<a href=\"https://productforums.google.com/forum/#!forum/chrome\" target=\"_blank\" rel=\"noopener\">Chrome Help Forum</a>&nbsp;and the&nbsp;<a href=\"https://support.google.com/chrome/a/?hl=en#topic=4386908\" target=\"_blank\" rel=\"noopener\">Chrome for business and education Help Center</a>.</p>\r\n<h3>Did you know?</h3>\r\n<p>Chrome subtly tells its user when an update is pending but has not yet been applied by coloring the vertical ellipsis icon in the upper right of the browser window.</p>\r\n<p><strong>Green</strong>: An update has been available for two days&nbsp;<strong>Orange</strong>: Update available for four days&nbsp;<strong>Red</strong>: Update has been available for seven or more days</p>',31,128,NULL,'2017-07-24 18:43:46','2017-07-24 18:43:46',NULL),(30,'Windows 10 is making too many PCs obsolete','<p>Microsoft released its latest Windows 10 update earlier this year. The name,&nbsp;<a href=\"http://www.computerworld.com/article/3187586/microsoft-windows/review-windows-10-creators-update-is-here-and-worth-the-download-with-video.html\">Creators Update</a>,makes it sound bigger than it is; it&rsquo;s really a minor step forward. But about 10 million Windows 10 customers have to face up to an unpleasant surprise: Their&nbsp;<a href=\"http://www.pcworld.com/article/3209705/windows/confirmed-windows-10-will-cut-off-devices-with-older-cpus.html\">machines can&rsquo;t update to Creators Update</a>.</p>\r\n<p>That&rsquo;s how many poor sad sacks bought a Windows 8.x laptop in 2013 or 2014 with an Intel Clover Trail processor. Any of them who have tried to update their PC with the March 2017 Creators Update, version 1703, had no success and were presented with this message: &ldquo;Windows 10 is no longer supported on this PC.&rdquo; Boy, that must have been fun!</p>\r\n<p>Not the end of the road for your three-year-old machine, though. I mean, you could always keep running the last version of Windows 10 on your PC. It wasn&rsquo;t as if you went directly to a permanent blue screen of death. And anyway, Microsoft eventually backed off some, announcing that, while you can&rsquo;t update those machines,<a href=\"http://www.zdnet.com/article/microsoft-agrees-to-extend-support-deadline-for-clover-trail-pcs/\">&nbsp;you can still get security patches</a>.</p>\r\n<p>Now, that&rsquo;s one giant corporation with a big heart.</p>\r\n<aside class=\"nativo-promo smartphone\"></aside>\r\n<p>I remember when&nbsp;<a href=\"http://www.computerworld.com/article/3030564/microsoft-windows/microsoft-uses-the-force-you-will-upgrade-to-windows-10.html\">Microsoft was forcing &ldquo;upgrades&rdquo; to Windows 10 down our throats</a>. There you were with a machine on the low end of Windows 10 hardware compatibility. You might have had some doubts about making the move to 10, but Microsoft was just so persistent. You must be pleased as punch you surrendered now.</p>\r\n<p>Some people have told me that it&rsquo;s not fair of me to expect Microsoft to support aging hardware. That&rsquo;s bull, and I&rsquo;ll tell you why.</p>\r\n<p>You may have noticed that&nbsp;<a href=\"http://www.zdnet.com/article/hp-keeps-up-strong-pc-sales-amid-slumping-worldwide-shipments/\">PC sales have been declining for years</a>. Know why? PCs last for years. I&rsquo;m still running computers that are over a decade old.</p>\r\n<p>If it&rsquo;s not broke, don&rsquo;t fix it. Better still, don&rsquo;t spend your IT money replacing it.</p>\r\n<aside class=\"nativo-promo tablet desktop\"></aside>\r\n<p>PCs aren&rsquo;t smartphones, which die in two to four years. I expect my PCs to last for at least ten years &mdash; especially when I&rsquo;m running desktop Linux on them.</p>\r\n<p>But what do I expect? Of course Windows 10 needs more hardware muscle than older systems can supply. But tell me &mdash; I&rsquo;m serious &mdash; what the heck did Windows 10 add to PCs that was worth having? I can&rsquo;t think of a thing. Windows 7, to me, is still the best version of Windows.&nbsp;<a href=\"http://www.computerworld.com/article/3091769/microsoft-windows/would-you-subscribe-to-windows.html\">Microsoft, of course, wants you off Windows 7</a>.</p>\r\n<p>People tell me, &ldquo;No, no, we need new powerful hardware to deal with today&rsquo;s operating systems and applications.&rdquo;</p>\r\n<p>Really? Excuse me if I missed something, but aren&rsquo;t we moving our applications to the cloud? Sure, if you&rsquo;re editing videos on your PC, you need power. But I can do 99% of my work on an ARM-powered Chromebook with 2GBs of RAM. So can you, I would bet.</p>\r\n<p>So tell me, Microsoft, why can&rsquo;t you fully support Windows 10 on these older PCs? They&rsquo;re not that old, and they can already run the earlier versions.</p>\r\n<p>Admittedly, the first generation of Clover Trail was &hellip; odd. The graphic processing units in Clover Trail PCs were underpowered from day one. Still, Microsoft urged Clover Trail owners to move to Windows 10 just as tirelessly as it did those running PCs with a best-of-breed 2015 i7 Skylake processor.</p>\r\n<p>I&rsquo;m ticked off not just that Microsoft is willing to dump customers. It&rsquo;s that it is treating poorly people who upgraded in good faith. To my mind, Microsoft made an implicit promise that it would be there for these customers. It has broken that promise, and those customers deserve better.</p>\r\n<p>But, looking ahead, why should I believe that Microsoft won&rsquo;t dump me, with my powerful, but no longer new, chips, on the next go-round?</p>\r\n<p>Yes, I get it. Microsoft can&rsquo;t support old hardware forever. But maybe Microsoft should adjust its business model in recognition of the fact that today&rsquo;s hardware doesn&rsquo;t become obsolete as fast as yesterday&rsquo;s hardware did. And maybe, when it introduces a new operating system that demands a lot more from hardware, it should throw in a few new features that make it all worthwhile.</p>\r\n<p>Or maybe you would like me to introduce you to a nice&nbsp;<a href=\"http://www.pcworld.com/category/chromebooks/\">Chromebook</a>&nbsp;or suggest you try, say,&nbsp;<a href=\"http://www.zdnet.com/article/happy-holidays-linux-mint-get-a-major-upgrade/\">Mint Linux</a>&nbsp;on your PC. I think you&rsquo;ll be a lot happier, and if you&rsquo;re already running mostly cloud-based applications, you won&rsquo;t be missing anything by bidding Windows adieu.</p>',31,129,NULL,'2017-07-24 18:46:25','2017-07-24 18:46:25',NULL),(31,'Problems with Surface Pro 4/Surface Book firmware update','<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/surface-book-surface-pro-4-resize-100721163-large_15009221701760.jpg\" alt=\"surface-book-surface-pro-4-resize-100721163-large.jpg\" width=\"700\" height=\"470\" /></p>\r\n<p>We have no way of knowing why Microsoft released the driver updates last Friday or what they&rsquo;re supposed to accomplish. What we do know is that the last set of patches came just 10 days earlier, on July 11, when&nbsp;<a href=\"https://www.microsoft.com/surface/en-us/support/install-update-activate/surface-pro-4-update-history\">Microsoft added support for the new Surface Pro Type Cover and Signature Type Cover</a>.</p>\r\n<p>With two dozen major firmware and driver updates pushed onto the Surface Pro 4 since its release in October 2015, and a new Surface Pro 2017 currently on offer, it&rsquo;s noteworthy that Microsoft is still trying to get the SP4 and SB drivers right.</p>\r\n<p>Commenter Bespin on the&nbsp;<a href=\"https://www.onmsft.com/news/surface-pro-4-firmware-update-adds-battery-control-slider\">OnMsft forum</a>&nbsp;has a screenshot of the drivers just installed on his Surface Pro 4:</p>\r\n<ul>\r\n<li>Surface driver update for Surface Management Engine</li>\r\n<li>Surface driver update for Surface Pen Pairing</li>\r\n<li>Marvell Semiconductor Inc. driver update for Marvell AVASTAR Bluetooth Radio Adapter</li>\r\n<li>Surface driver update for Surface UEFI</li>\r\n<li>Microsoft driver update for Surface Accessory Device</li>\r\n<li>Surface driver update for Surface Embedded Controller Firmware</li>\r\n<li>Marvell Semiconductor Inc. driver update for Marvell AVASTAR Wireless-AC Network Controller</li>\r\n<li>Intel(R) Corporation driver update for Intel(R) Display Audio</li>\r\n<li>Surface driver update for Surface Integration</li>\r\n</ul>\r\n<p>I&rsquo;ve also seen reports of a new &ldquo;Intel Corporation driver update for Intel(R) HD Graphics 520.&rdquo; Of course, until Microsoft graces us with a change list, we&rsquo;re reduced to speculating about the changes.</p>\r\n<p>Judging by the decibel level of complaints, the rollout has been anything but problem-free.</p>\r\n<p>On&nbsp;<a href=\"https://www.reddit.com/r/Surface/comments/6olm3p/new_firmware_for_surface_pro_4/\">Reddit</a>, I see reports of a new power slider&mdash;accessible by hovering your mouse/pen over the power icon in the system tray&mdash;that lets you adjust the power mode from &ldquo;Best battery life&rdquo; to &ldquo;Best performance.&rdquo; Apparently, it&rsquo;s the same slider available now in the Surface Laptop and Surface Pro 2017, and it seems identical to the one&nbsp;<a href=\"https://blogs.windows.com/windowsexperience/2017/01/19/announcing-windows-10-insider-preview-build-15014-for-pc-and-mobile-hello-windows-insiders-today-we-are-excited-to-be-releasing-windows-10-insider-preview-build-15014-for-pc-and-mobile/#TyosZkeEhqtet1Ih.97\">Microsoft has announced</a>&nbsp;for the Fall (North America) Creators Update version 1709, due in September or October.</p>\r\n<p>In the same Reddit thread, there are reports of new Wi-Fi problems, and there&rsquo;s a divergence of opinion as to whether the new Pen driver improves matters.</p>',31,130,NULL,'2017-07-24 18:50:57','2017-07-24 18:50:57',NULL),(32,'Windows 10 Redstone: A guide to the builds','<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/uploads/windows-10-updates-1-100670400-primary.idge_15009224061605.jpg\" alt=\"windows-10-updates-1-100670400-primary.idge.jpg\" width=\"620\" height=\"413\" /></p>\r\n<p>Microsoft never sleeps. Even before the&nbsp;<a href=\"http://www.computerworld.com/article/3187586/microsoft-windows/review-windows-10-creators-update-is-here-and-worth-the-download-with-video.html\">Windows 10 Creators Update</a>&nbsp;was rolled out, the company began work on the next major update to Windows 10, code-named Redstone 3. As it did with the Creators Update, Microsoft has been releasing a series of public preview builds to&nbsp;<a href=\"http://www.computerworld.com/article/3107834/microsoft-windows/after-the-anniversary-update-whats-next-for-windows-10.html?page=1#insider\">members of Microsoft\'s Insider Program</a>.</p>\r\n<p>What follows is a list of every preview build of Redstone 3, starting with the most recent. For each build, we\'ve included the date of its release and a link to Microsoft\'s announcement about it. We\'ve also kept the list of all the preview builds that led up to Creators Update, which are below the builds of Redstone 3.</p>\r\n<p><em>Note: This story covers Insider Preview builds for the PC version of Windows 10, not the phone version. If you\'re looking for information about updates being rolled out to all Windows 10 users, not previews for Microsoft Insiders, see \"<a href=\"http://www.computerworld.com/article/3199077/microsoft-windows/windows-10-a-guide-to-the-updates.html\">Windows 10: A guide to the updates</a>.\"</em></p>\r\n<aside class=\"fakesidebar\"><strong>[ Related:&nbsp;<a href=\"http://www.computerworld.com/article/3155816/microsoft-windows/fix-windows-10-problems-with-these-free-microsoft-tools.html#tk.ctw-infsb\">Fix Windows 10 problems with these free Microsoft tools</a>&nbsp;]</strong></aside>\r\n<aside class=\"fakesidebar\">\r\n<h4>Windows 10 Insider Preview Build 16241</h4>\r\n<p><strong>Release date:</strong>&nbsp;July 13, 2017</p>\r\n<p>This build makes a number of small changes to Windows Shell, Task Manager, the way uploads and downloads are handled, and other parts of Windows.</p>\r\n<p>You can now recover your pin and password from the lock screen by clicking &ldquo;Reset password&rdquo; or &ldquo;I forgot my PIN&rdquo; and following the prompts. You can then return to the lock screen and log in with your new credentials.</p>\r\n<aside class=\"fakesidebar\"><strong>[ To comment on this story, visit&nbsp;<a href=\"https://www.facebook.com/Computerworld/posts/10155542441984680\" target=\"_blank\" rel=\"noopener\">Computerworld\'s Facebook page</a>. ]</strong></aside>\r\n<p>Also in this build, Microsoft continues its ongoing tweaking of the newly added GPU section of the Task Manager&rsquo;s Performance tab. For example, the name of each GPU is now shown on the left-hand side of the Performance tab. The Task Manager gets one more change: Microsoft Edge&rsquo;s processes are given more descriptive labels throughout Task Manager.</p>\r\n<p>The Delivery Optimization page in Settings gets two new additions. One allows you to configure a variety of download and upload settings, such as limiting bandwidth for uploads and downloads. Also added to the page is an activity monitor that shows how much data is being received or sent from your PC.</p>\r\n<p>There are also the usual bug fixes, including one where the battery status on certain laptops wasn&rsquo;t updating while the device was unplugged, and other minor changes.</p>\r\n<p><strong>What IT needs to know:&nbsp;</strong>One of Microsoft&rsquo;s regular Bug Bashes is still ongoing, and will end at 11:59 p.m. (Pacific Daylight Time) on Sunday, July 23rd.</p>\r\n<p>(Get more info about&nbsp;<a href=\"https://blogs.windows.com/windowsexperience/2017/07/13/announcing-windows-10-insider-preview-build-16241-pc-build-15230-mobile/#z2bywCO654bq8i4C.97\" target=\"_blank\" rel=\"noopener\">Insider Preview Build 16241</a>.)</p>\r\n</aside>',31,131,NULL,'2017-07-24 18:55:12','2017-07-24 18:55:12',NULL),(33,'CPU architecture after Moore’s Law: What\'s next?','<p>When considering the future of CPU architecture, some industry watchers predict excitement, and some predict boredom. But no one predicts a return to the old days, when speed doubled at least every other year.</p>\r\n<p>The upbeat prognosticators include David Patterson, a professor at the&nbsp;<a href=\"http://www.berkeley.edu/\" target=\"_blank\" rel=\"noopener\">University of California, Berkeley</a>, who literally wrote the&nbsp;<a href=\"http://buy.geni.us/Proxy.ashx?TSID=14159&amp;GR_URL=https%3A%2F%2Fwww.amazon.com%2FComputer-Architecture-Fifth-Quantitative-Approach%2Fdp%2F012383872X%2Fref%3Dsr_1_3%3Fs%3Dbooks%26ie%3DUTF8%26qid%3D1488386146%26sr%3D1-3%26keywords%3Ddavid%2Bpatterson\" target=\"_blank\" rel=\"noopener\" data-amzn-asin=\"012383872X\">textbook</a>&nbsp;(with John Hennessy) on computer architecture. &ldquo;This will be a renaissance era for computer architecture &mdash; these will be exciting times,&rdquo; he says.</p>\r\n<p>Not so much, says microprocessor consultant Jim Turley, founder of&nbsp;<a href=\"http://www.jimturley.com/\" target=\"_blank\" rel=\"noopener\">Silicon Insider</a>. &ldquo;In five years we will be 10% ahead of where we are now,&rdquo; he predicts. &ldquo;Every few years there is a university research project that thinks they are about to overturn the tried-and-true architecture that John von Neumann and Alan Turing would recognize &mdash; and unicorns will dance and butterflies will sing. It never really happens, and we just make the same computers go faster and everyone is satisfied. In terms of commercial value, steady, incremental improvement is the way to go.&rdquo;</p>\r\n<p>They are both reacting to the same thing: the increasing irrelevance of Moore&rsquo;s Law, which observed that the number of transistors that could be put on a chip at the same price doubled every 18 to 24 months. For more to fit they had to get smaller, which let them run faster, albeit hotter, so performance rose over the years &mdash; but so did expectations. Today, those expectations remain, but processor performance has plateaued.</p>\r\n<h3>The plateau and beyond</h3>\r\n<p>&ldquo;Power dissipation is the whole deal,&rdquo; says Tom Conte, a professor at the&nbsp;<a href=\"http://www.gatech.edu/\" target=\"_blank\" rel=\"noopener\">Georgia Institute of Technology</a>&nbsp;and past president of the&nbsp;<a href=\"https://www.computer.org/\" target=\"_blank\" rel=\"noopener\">IEEE Computer Society</a>. &ldquo;Removing 150 watts per square centimeter is the best we can do without resorting to exotic cooling, which costs more. Since power is related to frequency, we can&rsquo;t increase the frequency, as the chip would get hotter. So we put in more cores and clock them at about the same speed. They can accelerate your computer when it has multiple programs running, but no one has more than a few trying to run at the same time.&rdquo;</p>\r\n<p>The approach reaches the point of diminishing returns at about eight cores, says Linley Gwennap, an analyst at&nbsp;<a href=\"http://www.linleygroup.com/\" target=\"_blank\" rel=\"noopener\">The Linley Group</a>. &ldquo;Eight things in parallel is about the limit, and hardly any programs use more than three or four cores. So we have run into a wall on getting speed from cores. The cores themselves are not getting much wider than 64 bits. Intel-style cores can do about five instructions at a time, and ARM cores are up to three, but beyond five is the point of diminishing returns, and we need new architecture to get beyond that. The bottom line is traditional software will not get much faster.&rdquo;</p>\r\n<p>&ldquo;Actually, we hit the wall back in the &rsquo;90s,&rdquo; Conte adds. &ldquo;Even though transistors were getting faster, CPU circuits were getting slower as wire length dominated the computation. We hid that fact using superscalar architecture [i.e., internal parallelism]. That gave us a speedup of 2x or 3x. Then we hit the power wall and had to stop playing that game.&rdquo;</p>\r\n<div class=\"empty-div\">&nbsp;</div>\r\n<div class=\"tease-promo\">&nbsp;</div>',32,133,NULL,'2017-07-24 19:05:41','2017-07-24 19:05:41',NULL),(34,'The Windows Malicious Software Removal Tool has been updated for WannaCry','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid amet asperiores cum cumque dicta eligendi error eveniet itaque laboriosam laudantium maxime nam natus nostrum odio perspiciatis praesentium quae quidem ratione repudiandae similique soluta, unde voluptates. Aperiam autem enim fugit iusto laboriosam quo veritatis. Architecto cumque dolore doloribus eligendi est eum fugit harum illo incidunt nemo optio perferendis possimus praesentium quaerat quas quasi quod recusandae rem, saepe vitae. Accusantium at consectetur cupiditate debitis dolore eligendi ex expedita facilis harum illum, labore laudantium maxime minima natus nemo, nisi non nulla numquam officiis placeat porro provident quibusdam recusandae repudiandae similique tempore voluptates? Adipisci alias aliquid architecto aspernatur assumenda atque, autem consequatur consequuntur dicta dolor eaque earum eius eligendi fugit in ipsum itaque modi officia omnis provident quaerat quas quia quidem quis quo reprehenderit rerum sapiente sed sint totam ullam veniam voluptatem voluptatibus. Ab architecto atque cumque cupiditate dignissimos dolor eligendi et eum, hic illum laboriosam, maxime modi nostrum quas sapiente soluta temporibus totam ut vel voluptates! Ab accusamus aperiam, aut consectetur ducimus ea eos error, eum excepturi ipsam itaque iure iusto molestias nulla numquam, optio sequi sunt unde veniam voluptatem? At eveniet facere id incidunt obcaecati placeat sint tempora voluptas. Aperiam, omnis.</p>',29,122,NULL,'2017-07-25 07:01:26','2017-07-25 07:01:33','2017-07-25 07:01:33');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_category`
--

DROP TABLE IF EXISTS `post_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_category` (
  `post` int(10) unsigned NOT NULL,
  `category` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post`,`category`),
  KEY `foreign_cat_post_idx` (`category`),
  CONSTRAINT `foreign_cat_post` FOREIGN KEY (`category`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `foreign_post_cat` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_category`
--

LOCK TABLES `post_category` WRITE;
/*!40000 ALTER TABLE `post_category` DISABLE KEYS */;
INSERT INTO `post_category` VALUES (24,20),(26,20),(27,20),(29,20),(30,20),(32,20),(34,20),(24,21),(31,21),(33,21),(25,22),(26,23),(27,24),(28,24),(30,25),(32,25);
/*!40000 ALTER TABLE `post_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_rating`
--

DROP TABLE IF EXISTS `post_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_rating` (
  `post` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`post`,`user`),
  KEY `foreing_post_rating_user_idx` (`user`),
  CONSTRAINT `foreign_post_rating_post` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreing_post_rating_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_rating`
--

LOCK TABLES `post_rating` WRITE;
/*!40000 ALTER TABLE `post_rating` DISABLE KEYS */;
INSERT INTO `post_rating` VALUES (26,32,-1),(27,32,-1),(27,33,-1),(28,32,1),(29,32,1),(30,29,-1),(30,32,1),(31,32,1),(31,33,1),(31,34,-1),(32,32,-1),(32,33,-1),(33,29,1),(33,32,-1),(33,33,-1),(33,34,-1);
/*!40000 ALTER TABLE `post_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_tag`
--

DROP TABLE IF EXISTS `post_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_tag` (
  `post` int(10) unsigned NOT NULL,
  `tag` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post`,`tag`),
  KEY `foreign_tag_idx` (`tag`),
  CONSTRAINT `foreign_post` FOREIGN KEY (`post`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreign_tag` FOREIGN KEY (`tag`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_tag`
--

LOCK TABLES `post_tag` WRITE;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
INSERT INTO `post_tag` VALUES (24,66),(32,66),(24,67),(25,67),(31,67),(24,68),(25,69),(25,70),(28,70),(25,71),(26,72),(27,74),(28,74),(27,75),(28,76),(29,77),(29,78),(30,79),(30,80),(30,81),(31,82),(32,83),(32,84),(34,84),(33,85),(33,86),(33,87);
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (3,'admin\r'),(2,'author\r'),(1,'user\r');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `role` int(10) unsigned NOT NULL,
  `permission` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role`,`permission`),
  KEY `foreign_permission_idx` (`permission`),
  CONSTRAINT `foreign_permission` FOREIGN KEY (`permission`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreign_role` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (1,13),(2,13),(3,13),(1,14),(2,14),(3,14),(2,15),(3,15),(3,16),(2,17),(3,17),(3,18),(1,19),(2,19),(3,19),(1,20),(2,20),(3,20),(1,21),(2,21),(3,21),(3,22),(1,23),(2,23),(3,23),(3,24),(1,25),(2,25),(3,25),(1,26),(2,26),(3,26),(1,27),(2,27),(3,27),(3,28),(1,29),(2,29),(3,29),(3,30),(3,31),(3,32),(3,33);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (70,'antivirus products'),(69,'antivirus software'),(77,'browsers'),(87,'computers'),(68,'diversified investment services'),(82,'driver update'),(86,'evolution'),(76,'facial recognition'),(80,'linux'),(74,'machine learning'),(66,'microsoft'),(73,'research'),(71,'security solution'),(85,'silicon insider'),(81,'smartphones'),(84,'task manager'),(67,'technology_internet'),(75,'tensorflow'),(72,'uber'),(78,'updates'),(79,'windows'),(83,'windows insider');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` char(100) DEFAULT NULL,
  `role` int(10) unsigned NOT NULL DEFAULT '1',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` int(10) unsigned DEFAULT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `bio` text,
  `deletedAt` timestamp NULL DEFAULT NULL,
  `lastAttend` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userscol_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `index_deleted` (`deletedAt`),
  KEY `foreign_userrole_idx` (`role`),
  KEY `foreign_user_avatar_idx` (`avatar`),
  CONSTRAINT `foreign_user_avatar` FOREIGN KEY (`avatar`) REFERENCES `media` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `foreign_userrole` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (29,'Admin','katanitsyaanton@gmail.com','$2a$10$1zpfBf/rt46sY29riOJB0OJbNJ1Dij83GHJ1t4k4IrIejz4W/P0kK',3,'2017-07-24 17:45:40',118,'Nameless','One','The being now known only as The Nameless One is one of the closest things the multiverse has to an immortal. Precisely how long he has lived or how many reincarnations he has had is unknown, but judging from some of the contacts he has made (Lum the Mad, among others) he is probably at least several thousand years old, and a former \"incarnation\" notes that he has died several thousand times.\r\nHis place of birth and early history have been lost, but at some point he committed a horrible crime. The precise nature of this crime is never expressed directly, though it was clearly severe enough to damn his soul even if he spent the rest of his life doing nothing but good. ',NULL,'2017-07-25 14:39:55'),(30,'Morte','rictusgrin@sigil.net','$2a$10$I3BJ.cfBEpFZlitVzK6zFe2zmHAGrAvqimxS4XiRRbY2mpzK1/eHC',2,'2017-07-24 18:07:21',123,'Mortimer','Rictusgrin','Morte is a talking skull. His sole weapon seems to be his mouth, whether by taunting or biting. He seems to be along for the ride, whether you want him around or not.\r\nYou are somewhat curious as to how he is able to float around.\r\nWhile he lacks a body, he is a capable warrior in many aspects, biting with his sharp fangs and throwing foes off guard with taunts, while his smaller size, lack of vital organs and pseudo-undead nature protect him from many attacks that would normally inflict serious wounds. Many fans wonder where he keeps his inventory. Morte and the Nameless One have an extended history together. Morte is the source of much of the game\'s humor, not the least being the chaotic conversations that can result between him and the Nameless One, whom he refers to as Chief',NULL,'2017-07-24 18:34:28'),(31,'Virgil','brummond@caladon.net','$2a$10$3qxZ2hoO/3R84Jsz.kVrE.mqBlFtUWFaReGu2HBNzc4Uz0Tu8jVba',2,'2017-07-24 18:38:51',127,'Virgil','Brummond','Virgil is a Panarii priest and one of the first NPCs any player will encounter in the game at the Crash Site. He believes the player to be a reincarnation of an ancient legend and will automatically join the player party from the start of the game, unless the player goes to extensive lengths to convince him not to. \r\n\r\nVirgil is a useful companion to have around unless and until you reach a technical aptitude of around 35 (then his spells will generally fail against you). Virgil is the most interesting follower in the game. He has a history that you\'ll slowly discover, and he adds the most dialogue of any of the followers.',NULL,'2017-07-24 18:55:30'),(32,'Sulik','sulik@greatsaltwater.org','$2a$10$OkVFY9uLqPpVd1dUAqXSmeK8A5ArFDjPkAsDFL.2Zl0LrEns2MER.',2,'2017-07-24 18:58:52',132,'Sulik',NULL,'Not much is known about Sulik, apart from what little he tells to the Chosen One. He says that he is from a primitive village located by the \'Great Salt Water\' which could either refer to the sea or the Great Salt Lake in Utah, and has come in search of Kurisu, his sister who he thought was captured by slavers.\r\n\r\nThe large bone driven through his nose he calls \'Grampy Bone\'. It is connected to the tribal lore of his family and village; through it he is able to speak to his grandfather and other important dead spirits of his tribe, who give him advice, cryptic as it may be.',NULL,'2017-07-24 22:19:16'),(33,'Cassidy','cassidy@vaultcity.org','$2a$10$ErjYSB6M6KkqEb7KTVnua.jjpeKPUmf8BEWmpo38q9eApx3GQZFFi',1,'2017-07-25 13:15:51',134,'John ','Cassidy','His long history of adventuring has left him in a less than perfect state, including a metal plate in his head, many old wounds, a missing eye, and memories of some particularly nasty injuries (including a knifing in 2203 and gunshot wounds from 2195, 2199, and 2201).[2]\r\n\r\nDespite his life of violence, he is a very kind and polite person, at least to people he respects. Anyone who gets on his bad side can expect to be treated no better than dirt, usually accompanied by a number of curses from Cassidy\'s sizable catalog of insults. Myron is the usual victim of Cassidy\'s picking, as the adolescent genius, apart from creating the pharmaceutical scourge of the wastes, is perceived by the old man as a piece of human trash.',NULL,'2017-07-25 13:28:16'),(34,'Myron','myron@redding.com','$2a$10$w6ApaXnk3TO9ijHpdChXYOaSXo5AxDqvt3DXaElBDXgpHSnQuZSc6',1,'2017-07-25 13:32:05',135,'Myron',NULL,'Myron is a child-genius and chemist from the town of Redding , and the inventor of jet, which he invented to aid the Mordino family\'s vile exploits and also to barter for prostitutes, a laboratory, and cash. He can be found wasting time at the Stables in the basement. The Stables are located north of New Reno.\r\n\r\nCuriously, Myron hates Marcus due to him being a super mutant. Myron dies around 2242, less than a year after the defeat of the Enclave, stabbed by a jet addict while drinking in the Den. His discovery of jet was quickly forgotten, and since then no one remembers his name. His invention however continued to grow in popularity and reached the East Coast of America in the Capital Wasteland and the Commonwealth.',NULL,'2017-07-25 13:36:28');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-25 17:53:58
