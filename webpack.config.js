const path = require('path'),
    webpack = require("webpack");

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractLess = new ExtractTextPlugin({
    filename: "../css/style.css"
});

module.exports = {
    entry: './dev/js/index.js',
    output: {
        path: path.join(__dirname, 'web/js'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                use: extractLess.extract({
                    use: [{ loader: 'css-loader', options: { importLoaders: 1 } }, 'postcss-loader', "less-loader"],
                    fallback: "style-loader"
                })
            },
            {
                test: /\.css$/,
                use: extractLess.extract({
                    use: [{ loader: 'css-loader', options: { importLoaders: 1 } }, 'postcss-loader'],
                    fallback: "style-loader"
                })
            },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015'],
                    }
                }
            },
            {
                test: /\.(svg|woff|woff2|ttf|eot|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=10000&name=../fonts/[name].[hash].[ext]?'
            },
            {
                // Images
                test: /\.(png|jpe?g|gif|ico)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader?name=../images/[name].[ext]?'
            },
        ]
    },
    plugins: [
        extractLess
    ]
};

