<?php

namespace Framework;

/**
 * class Message which allow save and read messages
 *
 *  methods:
 *
 *  * registerMessage
 *  * getMessage
 *
 */

class Message {

    /**
     * Registers message
     *
     * @param string           $message      actual message
     * @param string           $type         type of message
     *
     */

    static public function registerMessage ($message, $type='success') {
        if( !isset( $_SESSION['messages'] ) ) $_SESSION['messages'] = [];
        $_SESSION['messages'][] = ['message' => $message, 'type' => $type];
    }

    /**
     * returns saved messages and clear session
     *
     * @return mixed    messages or false
     */

    static public function getMessages() {
        if( !empty($_SESSION['messages']) && is_array($_SESSION['messages']) ) {
            $messages = $_SESSION['messages'];
            $_SESSION['messages'] = [];
            return $messages;
        } else {
            return false;
        }
    }

}