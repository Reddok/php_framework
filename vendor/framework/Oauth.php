<?php

    namespace Framework;

    /**
     * class Oauth handles with all authentication routine
     *
     *  methods:
     *
     *  * checkUserSession
     *  * authenticate
     *  * generatePasswordHash
     *  * setCurrentUser
     *  * getCurrentUser
     *
     */

    class Oauth {

        /**
         * Place where current user has been stored
         *
         * @var array
         */

        private static $currentUser = null;


        /**
         * check if user exists
         *
         */

        public static function checkUserSession () {
            if( !$_SESSION['user'] ) Controller::redirect('/login');
        }

        /**
         * This method performs authenticate
         *
         * @param int           $id         user id
         *
         */

        public static function authenticate($id) {
            $_SESSION['user'] = $id;
        }

        /**
         * Generates hash from password
         *
         * @param string           $password         Query which will be performed
         *
         * @return string
         */

        public static function generatePasswordHash($password) {

            $salt = strtr(base64_encode(random_bytes (16)), '+', '.');
            $cost = 10;

            $salt = sprintf("$2a$%02d$", $cost) . $salt;
            return crypt($password, $salt);
        }

        /**
         * This method execute all database operations
         *
         * @param array           $user         user array
         *
         */

        public static function setCurrentUser($user) {
            static::$currentUser = $user;
        }

        /**
         * This method execute all database operations
         *
         * @return array
         */

        public static function getCurrentUser() {
            return static::$currentUser;
        }

        /**
         * Clear information about current user
         *
         */

        public static function clear() {
            unset($_SESSION['user']);
        }

    }