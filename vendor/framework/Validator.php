<?php

/**
 * use cases:
 *
 * $validator = new Validator();
 *
 * - add fields and constraints
 *
 * $validator->addField('email', $email)
 * ->addConstraint('required')
 * ->addConstraint('max', [16])
 * ->addConstraint('isEmail', [], 'This field must be an email');
 *
 * - add custom rules
 *
 * $validator->addRule('super rule', function() {});
 *
 * $validator->validate();
 *
 * - get error messages
 *
 * $errors = $validator->getErrors();
 *
 * - get trimmed values
 *
 * $values = $validator->getValues();
 */

    namespace Framework;


    /**
     * class Validator. What he does is obvious from his name.
     *
     *  methods:
     *
     *  * validate
     *  * addField
     *  * addRule
     *  * getErrors
     *  * getValues
     *  * clear
     *
     */

    class Validator {

        /**
         * Contains all rules
         *
         * @var array
         */

        static private $rules = [];

        /**
         * Contains current errors
         *
         * @var array
         */

        private $errors = [];

        /**
         * Contains current values
         *
         * @var array
         */


        private $values = [];

        /**
         * Constructor.
         *
         * Initialize set of rules if it not exist.
         */

        function __construct() {

            if(!self::$rules) {

                self::$rules = [

                    'required' => function ($value) {
                        return empty($value)? 'is required' : false;
                    },

                    'isEmail' => function ($value) {
                        return filter_var($value, FILTER_VALIDATE_EMAIL)? false : 'must be in email format';
                    },

                    'maxLength' => function($value, $max=10) {
                        return mb_strlen($value) <= $max? false : 'value must be lower or equal ' . $max . ' characters';
                    },

                    'minLength' => function ($value, $min=3) {
                        return mb_strlen($value) >= $min? false : 'value must be greater or equal ' . $min . ' characters';
                    },

                    'equal' => function ($value, $equal, $name='another value') {
                        return $value === $equal? false : 'non equal with ' . $name;
                    },

                    'hashEqual' => function($value, $hash, $name='password') {
                        return hash_equals( crypt($value, $hash), $hash )? false : 'non equal with ' . $name;
                    },

                    'between' => function ($value, $min, $max) {
                        $low = self::minLength($value, $min);
                        $great = self::maxLength($value, $max);

                        return !($low || $great)? false : 'value must be between ' . $min . ' and ' . $max . ' characters';
                    },

                    'isImage' => function ($value) {

                        $extensions = ['jpeg', 'jpg', 'png', 'gif', 'ico', 'svg'];
                        $file_ext = strtolower( end( explode('.', $value) ) );

                        return in_array($file_ext, $extensions)? false : 'must be an image type';

                    },

                    'isUnique' => function ($value, $fieldName, $db, $table) {

                        $result = $db->execute('SELECT COUNT(*) as count FROM `' . $table . '` WHERE ' . $fieldName . '=?;', [$value], false)['count'];
                        return !$result ? false : 'is already exists';

                    },

                    'isExists' => function($value, $fieldName, $db, $table) {

                        return self::isUnique($value, $fieldName, $db, $table)? false : $fieldName . ' with this ' . $fieldName . ' doesn\'t exist!';

                    },

                    'beforeTime' => function($value) {
                        return $value - time() >= 0? false : 'expiration time passed.';
                    },

                    'isNumber' => function($value) {
                        return is_numeric($value) && (int) $value == $value? false : 'value must be a number.';
                    }

                ];

            }

        }

        /**
         * Validates previously defined values
         *
         * @param bool      $returnErrors       indicates whether to return errors or just bool
         *
         * @return mixed
         */

        public function validate($returnErrors = false) {

            $this->errors = [];

            foreach($this->values as $name => $valRule) {

                foreach($valRule['rules'] as $ruleName => $rule) {

                    $message = null;
                    $args = $rule['arguments'];
                    array_unshift($args, $valRule['value']);

                    if(isset(self::$rules[$ruleName])) {
                        $message = call_user_func_array(self::$rules[$ruleName], $args);
                    }

                    if($message) {
                        $message = empty( $rule['message'] )? ucfirst($name) . ': ' . $message : $rule['message'];
                        $this->errors[] = $message;
                    }

                }

            }

            if($returnErrors) return $this->errors;

            return !( (bool) $this->errors );

        }

        /**
         * add field to validator for further validation
         *
         * @param string      $name      name of field
         * @param bool        $value     value to be validated
         *
         * @return object       returns object, where will be the necessary rules
         */

        public function addField($name, $value) {

            $this->values[$name] = [
                'value' => is_string($value)? trim($value) : $value,
                'rules' => []
            ];

            return new ConstrainsContainer($this->values[$name]['rules']);

        }

        /**
         * add custom rule for validation
         *
         * @param string      $name     name of rule
         * @param bool        $func     a function that will be a rule
         *
         */

        public function addRule($name, $func) {
            self::$rules[$name] =  $func;
        }

        /**
         * get all errors
         *
         * @return array
         */

        public function getErrors() {
            return $this->errors;
        }

        /**
         * get values
         *
         * @return array
         */

        public function getValues() {

            return array_map(function($val) {
                return $val['value'];
            }, $this->values);

        }

        /**
         * clear errors and values
         *
         */

        public function clear() {
            $this->values = [];
            $this->errors = [];
        }

        public static function __callStatic($name, $arguments){
            return call_user_func_array(self::$rules[$name], $arguments);
        }


    }

    /**
     * A simple class to be used to add new rules.
     *
     *  methods:
     *
     *  * addConstraint
     *
     */

    class ConstrainsContainer {

        /**
         * Contains all rules
         *
         * @var array
         */

        public $rules = null;

        function __construct(&$rules) {
            $this->rules = &$rules;
        }

        /**
         * Adds new constraint rule for value
         *
         * @param string        $ruleName   just rulename... I don't know what else can be said here
         * @param array         $ruleArgs   some arguments if present
         * @param string        $message    custom message if value not passed this rule. Completely optional
         *
         * @return object       reference on current instance
         */

        public function addConstraint($ruleName, $ruleArgs=[], $message=null) {
            $this->rules[$ruleName] = [
                'arguments' => $ruleArgs,
                'message' => $message
            ];

            return $this;
        }

    }