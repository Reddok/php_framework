<?php

namespace Framework\Interfaces;

/**
 * Interface methods from which the Middleware container  has to implement
 *
 * abstract methods:
 *
 *  * registerMiddleware
 *  * run
 *
 *
 */

    interface MiddlewareContainerInterface {

        public function registerMiddleware(MiddlewareInterface $middleware);

        public function run(&$data);


    }
