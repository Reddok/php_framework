<?php

namespace Framework\Interfaces;

/**
 * Interface methods from which the Model has to implement
 *
 * abstract methods:
 *
 *  * getAll
 *  * getById
 *  * create
 *  * update
 *  * delete
 *
 *
 */

interface ModelInterface
{

    public function getAll();

    public function getById($id);

    public function create($data);

    public function update($id, $data);

    public function delete($id);

}