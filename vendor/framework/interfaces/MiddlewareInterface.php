<?php

namespace Framework\Interfaces;

/**
 * Interface methods from which the Middleware  has to implement
 *
 * abstract methods:
 *
 *  * process
 *
 *
 *
 */

    interface MiddlewareInterface {

        public function &process(&$data);

    }