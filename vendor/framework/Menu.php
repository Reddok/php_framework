<?php

    namespace Framework;

    /**
     * abstract class Menu which represent site menu
     *
     *  methods:
     *
     *  * print
     *  * getItems
     *  * selectActive
     *  * show
     *  * getMenu
     *
     */

    abstract class Menu {

        /**
         * Menu items
         *
         * @var array
         */

        protected $items;

        abstract protected function print();

        abstract protected function getItems();

        /**
         * Selects active element comparing slug with current url
         *
         */

        protected function selectActive() {

            foreach($this->items as &$item) {
                if($item['slug'] === trim(strtok($_SERVER["REQUEST_URI"],'?'), '/')) {
                    $item['selected'] = true;
                    return $item;
                }

            }

        }

        /**
         * Shows menu
         *
         */

        public function show() {

            $this->getItems();
            $this->selectActive();
            $this->print();

        }

        /**
         * function-fabric get specified menu if it exist;
         *
         */

        public static function getMenu($type, $args=[]) {
            $className = ucfirst($type) . 'Menu';

            if(class_exists($className)) ( new $className(...$args) )->show();
        }

    }