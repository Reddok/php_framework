<?php

namespace Framework;

/**
 * class Mail which represent system, which can send messages to other emails.
 *
 *  methods:
 *
 *  * sendMail
 *  * clear
 *  * getInstance
 *  * setCredentials
 *
 */

class Mail {

    /**
     * Single instance of Mail class
     *
     * @var object
     */

    private static $_instance;

    /**
     * email from which system will be connected to gmail
     *
     * @var string
     */

    private static $_appEmail;

    /**
     * password for gmail account
     *
     * @var string
     */

    private static $_appPassword;

    /**
     * instance of PHPMailer class
     *
     * @var object
     */

    private $mail;

    /**
     * Constructor.
     *
     * Configures PHPMailer instance
     */

    private function __construct() {

        if(!self::$_appEmail || !self::$_appEmail) throw new \Exception('Mail: empty app email or password!');

        $this->mail = new \PHPMailer();
        $this->mail->IsSMTP();
        $this->mail->SMTPDebug = 0;
        $this->mail->SMTPAuth = true;
        $this->mail->SMTPKeepAlive = true;
        $this->mail->SMTPSecure = 'ssl';
        $this->mail->Host = 'smtp.gmail.com';
        $this->mail->Port = 465;
        $this->mail->Username = APP_EMAIL;
        $this->mail->Password = APP_EMAIL_PASSWORD;

    }

    /**
     * Sends message
     *
     * @param string           $to          recepient
     * @param string           $from_name   Sender name
     * @param string           $subject     Subject of message
     * @param string           $body        message body
     * @param string           $from        sender
     *
     * @throws \Exception       throws error if the mail was not sent
     *
     * @return mixed
     */

    public function sendMail($to, $from_name, $subject, $body, $from=null) {

        $from = $from? $from : $this->mail->Username;

        $this->mail->SetFrom($from, $from_name);
        $this->mail->Subject = $subject;
        $this->mail->Body = $body;
        $this->mail->AddAddress($to);
        if(!$this->mail->Send()) {
            throw new \Exception('Mail error: '.$this->mail->ErrorInfo);
        } else {
            $this->clear();
            return true;
        }

    }

    /**
     * Clears all predefined parameters for new messages
     *
     */

    public function clear() {

        $this->mail->clearAllRecipients();
        $this->mail->clearAddresses();
        $this->mail->clearAttachments();
        $this->mail->clearCustomHeaders();

    }

    private function __clone() {}

    /**
     * Return(create if not exist) instance of class Mail
     *
     * @return object
     */

    static public function getInstance() {

        if(!self::$_instance) self::$_instance = new self();
        return self::$_instance;
    }

    /**
     * Set credentials for class Mail
     *
     * @param string           $email         gmail email
     * @param string           $password      gmail password
     */

    static public function setCredentials($email, $password) {
        self::$_appEmail = $email;
        self::$_appPassword = $password;
    }


}

