<?php

    namespace Framework;

    /**
     * Middleware Container which operates with all middleware
     *
     *  methods:
     *
     *  * registerMiddleware
     *  * getMiddleware
     *  * run
     *
     */

    class MiddlewareContainer implements Interfaces\MiddlewareContainerInterface {

        /**
         * Array that contains all middleware
         *
         * @var array
         */

        private $middlewares = [];

        /**
         * Registers middleware
         *
         * @param Interfaces\MiddlewareInterface    $middleware     middleware that will be registered
         */

        public function registerMiddleware(Interfaces\MiddlewareInterface $middleware) {
            $this->middlewares[] = $middleware;
        }

        /**
         * Return count of all middlewares
         *
         * @return int
         */

        public function getMiddlewareList() {
            return count($this->middlewares);
        }

        /**
         * Run all middlewares in turn
         *
         * @param array     $data       request array that will be used for all middlewares
         *
         */

        public function run(&$data) {

            foreach($this->middlewares as $middleware) {
                $middleware->process($data);
            }

        }


    }