<?php

namespace Framework;

/**
 * class Route which acts as simple router
 *
 *  methods:
 *
 *  * start
 *  * runController
 *  * checkAccess
 *  * findPath
 *  * select
 */

class Route
{
    /**
     * array of routes
     *
     * @var object
     */

     private $routes;

    /**
     * request array
     *
     * @var object
     */

     private $req;

    /**
     * array of replacements
     *
     * @var object
     */

     private $transformations = [
         ['sign' => '\*', 'replace' => '\S*', 'priority' => 1] ,
         ['sign' => ':([a-zA-Z1-9]+)', 'replace' => '(?P<$1>\w+)', 'priority' => 2] ,
     ];

    /**
     * Constructor. Receive application routes and initialize transformations
     *
     * @param array             $routes     routes from application
     */

     function __construct(array $routes)
     {
         $this->routes = $routes;
     }

    /**
     * starts router
     *
     * @param array             $req     request array
     *
     * @throws HttpException             throws exception if path don't exist
     */

    public function start(&$req)
    {

        $this->req = &$req;

        $path = $this->findPath();

        if(!$path) throw new HttpException(404);

        $this->runController($path);

    }

    /**
     * runs controller
     *
     * @param string             $path     string which indicates controller and action
     *
     * @throws HttpException             throws exception controller or method doesn't exist
     */

    public function runController($path) {

        list($controller, $action) = explode('/', $path);
        $controller = ucfirst( $controller . 'Controller' );
        $action .= 'Action';

        if( class_exists($controller) ) {
            $controller = new $controller();
        } else {
            throw new HttpException(404);
        }

        if( method_exists($controller, $action) ) {
            call_user_func([$controller, $action], $this->req);
        } else {
            throw new HttpException(404);
        }

    }

    /**
     * checks if user have access to specified path
     *
     * @param callable             $permissions     function which return bool value, that indicates access to path
     *
     * @return bool
     */

    public function checkAccess($permissions)
    {
        return $permissions($this->req);
    }

    /**
     * finds paths in routes. Collects it in one array and select one with highest priority
     *
     * @return string
     */

    private function findPath()
    {

        $fitUrls = array_reduce($this->routes, function($fitUrls, $group) {

            foreach ($group['routes'] as $urlPattern => $path) {

                $priority = 3;

                foreach($this->transformations as $trans) {
                    $changedPattern = preg_replace("/{$trans['sign']}/", $trans['replace'], $urlPattern);
                    if($changedPattern !== $urlPattern && $priority > $trans['priority']) $priority = $trans['priority'];
                    $urlPattern = $changedPattern;
                }

                if (preg_match("~^$urlPattern$~", $this->req['url'], $matches) ) {

                    $this->req['params'] = array_filter($matches, 'is_string', ARRAY_FILTER_USE_KEY);

                    if( $this->checkAccess($group['permissions']) ) {
                        $fitUrls[] = [
                            'path' => $path,
                            'params' => $this->req['params'],
                            'priority' => $priority
                        ];
                    }
                }

            }

            return $fitUrls;

        }, []);

        return $this->selectUrl($fitUrls);

    }

    /**
     * select url with highest priority
     *
     * @param array             $fitUrls    array of urls
     *
     * @return mixed
     */

    private function selectUrl($fitUrls) {

        usort($fitUrls, function($a, $b) {
            return $a['priority'] < $b['priority'];
        });

        $url = array_shift($fitUrls);

        if($url) {
            $this->req['params'] = $url['params'];
            return $url['path'];
        }

        return false;

    }

}