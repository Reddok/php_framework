<?php

namespace Framework;

/**
 * abstract class Controller which implement general actions
 *
 *  methods:
 *
 *  * redirect
 *  * returnBack
 *
 */

abstract class Controller implements Interfaces\ControllerInterface
{
    /**
     * There will be instance of class View
     *
     * @var object
     */

    protected $view;

    /**
     * There will be instance of class Validator
     *
     * @var object
     */

    protected $validator;

    /**
     * There will be instance of class Model
     *
     * @var object
     */

    protected $model;

    /**
     * Constructor.
     *
     * Sets general parameters for all controllers
     */

    public function __construct() {
        $this->view = new View();
        $this->validator = new Validator();
    }

    /**
     * Redirects to specified path.
     *
     * @param string           $path      The path, which provided to redirect
     */

    static public function redirect($path) {
        $path = ltrim($path, '/');
        header('Location: /' . strtolower($path));
        die();
    }

    /**
     * Return to the page from where the form was sent
     *
     */

    static public function returnBack() {
        static::redirect( parse_url($_SERVER['HTTP_REFERER'])['path'] );
    }
}