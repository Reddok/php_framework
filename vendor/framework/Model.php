<?php

namespace Framework;

/**
 * abstract class Model
 *
 *  methods:
 *
 *  * countRows
 *
 */

abstract class Model implements Interfaces\ModelInterface
{

    /**
     * Instance of database
     *
     * @var object
     */

    public $connect;

    /**
     * Name of table associated with this model
     *
     * @var string
     */

    protected $tableName = 'test';

    /**
     * Constructor. Receive instance of database
     *
     */

    public function __construct()
    {
        $this->connect = Database::getInstance();
    }

    /**
     * Count all entries for table of this model
     *
     * @return int
     *
     */

    public function countRows()
    {

        $q = 'SELECT ( SELECT COUNT(*) FROM ' . $this->tableName . ' as t where deletedAt IS NULL ) as count from ( select NULL as deletedAt) as dummy;';

        $result = $this->connect->execute($q, [], false);

        return $result['count'];

    }

    /**
     * Deletes entity
     *
     * @param int           $id     The id of the entity
     *
     * @return bool
     *
     */

    public function delete($id) {

        $q = 'UPDATE ' . $this->tableName . ' SET deletedAt=NOW() WHERE id=?;';
        return $this->connect->execute($q, [$id]);

    }

}