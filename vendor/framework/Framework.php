<?php

namespace Framework;

/**
 * main class of framework
 *
 *  methods:
 *
 *  * registerMiddleware
 *  * runController
 *  * start
 *  * aetDebugMode
 *  * setErrorHandler
 *  * handleError
 *  * defaultErrorHandler
 *
 */

class Framework {

    /**
     * There will be instance of class Route
     *
     * @var object
     */

    private $router;

    /**
     * Request array which contains all data from request
     *
     * @var array
     */

    private $req;

    /**
     * There will be instance of class MiddlewareContainer
     *
     * @var object
     */

    private $middlewareContainer;

    /**
     * Custom error handler
     *
     * @var callable
     */

    private $errorHandler;

    /**
     * Variable which indicates, it's debug mode or not
     *
     * @var bool
     */

    private $debugMode = true;

    /**
     * Constructor. Creates initial utility object and set debug mode
     *
     * @param array         $routes         array of routes from application
     *
     */

    public function __construct(array $routes) {

        $this->router = new Route($routes);
        $this->middlewareContainer = new MiddlewareContainer();

        if( defined(DEBUG) ) $this->setDebugMode(DEBUG);

    }

    /**
     * Registers middleware.
     *
     * @param Interfaces\MiddlewareInterface         $middleware         middleware object
     *
     */

    public function registerMiddleware(Interfaces\MiddlewareInterface $middleware) {
        $this->middlewareContainer->registerMiddleware($middleware);
    }

    /**
     * runs router's controller.
     *
     * @param string         $path         controller path
     *
     */

    public function runController($path) {
        $this->router->runController($path);
    }

    /**
     * Starts framework. Collects all request data and send it through all middleware to appropriate controller
     *
     */

    public function start() {

        $url = strtok($_SERVER["REQUEST_URI"],'?');
        $url = trim($url, '/');

        $this->req = [
            'url' => &$url,
            'content' => array_merge($_POST, $_FILES),
            'query' => &$_GET,
            'cookie' => &$_COOKIE,
            'session' => &$_SESSION
        ];

        try {

            $this->middlewareContainer->run($this->req);
            $this->router->start($this->req);

        } catch(\Exception $e) {
            $this->handleError($e);
        }

    }

    /**
     * Sets debug mode;
     *
     * @param bool          $mode   debug mode
     *
     */

    public function setDebugMode(bool $mode) {
        $this->debugMode = $mode;
    }

    /**
     * Sets error handler;
     *
     * @param callable          $handler  custom function which will handle error
     *
     */

    public function setErrorHandler(callable  $handler) {
        $this->errorHandler = $handler;
    }

    /**
     * Catch exception an chooses appropriate handler;
     *
     * @param \Exception          $e  exception
     *
     */

    public function handleError(\Exception $e) {

        if(! ($e instanceof HttpException) ) {
            $e = $this->debugMode? new HttpException(500, $e->getMessage()) : new HttpException(500);
        }

        if(!$this->errorHandler) {
            $this->defaultErrorHandler($e);
        } else {
            ($this->errorHandler)($e);
        }
    }

    /**
     * Default error handler;
     *
     * @param \Exception          $e  exception
     *
     * @throws $e   throws error
     *
     */

    private function defaultErrorHandler($e) {
        throw $e;
    }
}